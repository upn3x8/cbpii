# CBPII API


## Overview

This readme details project output artifacts only.

### Build & Package

`mvn package`

outputs..

`consent-service/target/R010584C0010_CoopDigitalCBPII-Database.zip`

`consent-service-ear/target/R010584C0010_CoopDigitalCBPII.ear`

See `consent-service/README.md` for application specific implementation.