BEGIN
DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"CBPIIDIG&&env"."funds_consent_cleanup"',
            job_type => 'PLSQL_BLOCK',
            job_action => 'BEGIN
    DELETE FROM funds_confirmation_consent where status = ''AWAITINGAUTHORISATION''
                      AND created < CURRENT_TIMESTAMP-interval ''1'' HOUR;
END;
',
            number_of_arguments => 0,
            start_date => NULL,
            repeat_interval => 'FREQ=HOURLY;INTERVAL=1;',
            end_date => NULL,
            enabled => FALSE,
            auto_drop => FALSE,
            comments => 'Test Schedule');



DBMS_SCHEDULER.SET_ATTRIBUTE(
             name => '"CBPIIDIG&&env"."funds_consent_cleanup"',
             attribute => 'store_output', value => TRUE);
DBMS_SCHEDULER.SET_ATTRIBUTE(
             name => '"CBPIIDIG&&env"."funds_consent_cleanup"',
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_OFF);



DBMS_SCHEDULER.enable(
             name => '"CBPIIDIG&&env"."funds_consent_cleanup"');
END;
