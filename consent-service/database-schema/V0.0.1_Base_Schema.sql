create user cbpiidig&&env identified  by "&&cpbii_pass"
default tablespace &&table_space
temporary tablespace temp
  quota unlimited on &&table_space;

create user cbpiidig&&env._app identified  by "&&cpbii_pass_app"
default tablespace &&table_space
temporary tablespace temp
  quota unlimited on &&table_space;

create user cbpiidig&&env._mts identified  by "&&cpbii_pass_mts"
default tablespace &&table_space
temporary tablespace temp
  quota unlimited on &&table_space;

-- Schema Owner
grant connect to cbpiidig&&env;
-- The CBPII Application user
grant connect to cbpiidig&&env._app;
-- The CBPII user used ot access the REF_DATA_LIVE TABLE
grant connect to cbpiidig&&env._mts;
--grant create session to  cbpiidig&&env._mts;

ALTER SESSION SET CURRENT_SCHEMA = cbpiidig&&env;

-- Setup
CREATE TABLE  cbpiidig&&env..FUNDS_CONFIRMATION_CONSENT
(
  CONSENT_ID NUMBER(12,0) NOT NULL,
  TPP_ID VARCHAR2(128) NOT NULL,
  C_ID VARCHAR2(12) NOT NULL,
  DC_ID VARCHAR2(12) NOT NULL,
  IDENTIFICATION VARCHAR2(16) NOT NULL,
  EXPIRY_DATE TIMESTAMP WITH TIME ZONE,
  STATUS VARCHAR2(22) NOT NULL,
  STATUS_UPDATED TIMESTAMP(6) WITH TIME ZONE,
  CREATED TIMESTAMP(6) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT FUNDS_CONFIRMATION_CONSENT_PK PRIMARY KEY (CONSENT_ID)
);

prompt
CREATE SEQUENCE  cbpiidig&&env..FUNDS_CONFIRMATION_CONSENT_SEQ MINVALUE 1000 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1000;

prompt
CREATE TABLE  cbpiidig&&env..FUNDS_CONFIRMATION
(
  CONFIRMATION_ID NUMBER(12,0) NOT NULL,
  CONSENT_ID NUMBER(12,0) NOT NULL REFERENCES cbpiidig&&env..FUNDS_CONFIRMATION_CONSENT(CONSENT_ID), -- parent fk
  REFERENCE VARCHAR2(50),
  AMOUNT NUMBER(12,2) NOT NULL,
  CURRENCY VARCHAR2(3) NOT NULL,
  APPROVED SMALLINT NOT NULL,
  CREATED TIMESTAMP(6) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT FUNDS_CONFIRMATION_PK PRIMARY KEY (CONFIRMATION_ID)
);

CREATE  SEQUENCE cbpiidig&&env..FUNDS_CONFIRMATION_SEQ MINVALUE 1000 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1000;

-- Grants for new tables and sequences
GRANT SELECT, INSERT, UPDATE ON  cbpiidig&&env..FUNDS_CONFIRMATION_CONSENT TO cbpiidig&&env._app;
GRANT SELECT, INSERT, UPDATE ON  cbpiidig&&env..FUNDS_CONFIRMATION TO cbpiidig&&env._app;
GRANT SELECT ON  cbpiidig&&env..FUNDS_CONFIRMATION_CONSENT_SEQ TO cbpiidig&&env._app;
GRANT SELECT ON  cbpiidig&&env..FUNDS_CONFIRMATION_SEQ TO cbpiidig&&env._app;
GRANT SELECT ON  mtspibs&&env..REF_DATA_LIVE TO cbpiidig&&env._mts;

-- set current schema
CREATE OR REPLACE TRIGGER CPBII_LOGON_APP
AFTER LOGON ON DATABASE
BEGIN
if USER='cbpiidig&&env._app' THEN
EXECUTE IMMEDIATE 'ALTER SESSION SET CURRENT_SCHEMA =  cbpiidig&&env';
end if;
EXCEPTION
when others
then null; -- prevent a login failure due to an exception
END;
/
-- set current schema
CREATE OR REPLACE TRIGGER CPBII_LOGON_MTS
AFTER LOGON ON DATABASE
BEGIN
if USER='cbpiidig&&env._mts' THEN
EXECUTE IMMEDIATE 'ALTER SESSION SET CURRENT_SCHEMA =  mtspibs&&env';
end if;
EXCEPTION
when others
then null; -- prevent a login failure due to an exception
END;
/
exit;