create or replace view cbpiidig&&env..account_index_subset as
(
	select sort_code, account_number , account_type from
        stmtpibs&&env..account_index
	union  
	select sort_code, account_number , account_type from
        stmtsmile&&env..account_index
);

exit;
