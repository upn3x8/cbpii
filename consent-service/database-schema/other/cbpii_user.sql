create user cbpiidig&&env identified  by &&cpbii_pass
 default tablespace &&table_space
 temporary tablespace temp
 quota unlimited on &&table_space;

create user cbpiidig&&env._app identified  by &&cpbii_pass
 default tablespace &&table_space
 temporary tablespace temp
 quota unlimited on &&table_space;

grant connect to cbpiidig&&env;
grant connect to  cbpiidig&&env._app;
