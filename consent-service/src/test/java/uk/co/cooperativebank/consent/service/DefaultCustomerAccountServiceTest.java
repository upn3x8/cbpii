package uk.co.cooperativebank.consent.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import uk.co.cooperativebank.account.config.BrandConfiguration;
import uk.co.cooperativebank.account.dtos.Customer;
import uk.co.cooperativebank.account.gateway.BalanceAndTransactionsServiceGateway;
import uk.co.cooperativebank.account.gateway.CustomerServiceGateway;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.dao.account.AccountStatus;
import uk.co.cooperativebank.account.mts.dao.account.CurrentAccount;
import uk.co.cooperativebank.account.mts.dao.account.DomesticAccount;
import uk.co.cooperativebank.account.mts.framework.DomainObject;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceCall;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceManager;
import uk.co.cooperativebank.consent.controller.exception.BadRequestException;
import uk.co.cooperativebank.consent.controller.exception.NotFoundException;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class DefaultCustomerAccountServiceTest {

    private static final String PRODUCT_CODES = "2000,0000,0003,1600,2900,2700,2701,1500,9901";

    private static final String VALID_PRODUCT = "2900";
    private static final String INVALID_PRODUCT = "1234";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private CustomerAccountService mockCustomerAccountService;

    @Mock
    private EnterpriseServiceManager mockCallUtil;

    @Mock
    private BalanceAndTransactionsServiceGateway mockBalanceAndTransactionsServiceGateway;

    @Mock
    private CustomerServiceGateway mockCustomerServiceGateway;

    @Mock
    private BrandConfiguration mockBrandConfiguration;

    private static final String IDENTIFICATION = "08030822556722";
    private static final String IDENTIFICATION_ACCOUNT_TYPE = IDENTIFICATION + "01";

    @Before
    public void setup() {
        mockCustomerAccountService = new DefaultCustomerAccountService(mockCallUtil,
            mockBalanceAndTransactionsServiceGateway,
            mockCustomerServiceGateway,
            mockBrandConfiguration,
            PRODUCT_CODES);
    }

    @Test
    public void findCustomerByAccountShouldThrowAPIExceptionIfIdentificationLessThan14Digits() {
        //Given
        expectedException.expect(BadRequestException.class);
        expectedException.expectMessage("Identification not Valid");

        //When
        mockCustomerAccountService.findCustomerByAccount("08030822556", createClientContext());
    }

    @Test
    public void findCustomerByAccountShouldReturnExpectedCustomer() {
        //Given
        final Customer customer = new Customer();
        customer.setForename("Tester");
        customer.setSurname("John");
        customer.setDigitalCustomerId("CB0987654321");
        List<DomainObject> customerList = Collections.singletonList(customer);
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        final Customer result = mockCustomerAccountService.findCustomerByAccount(IDENTIFICATION, createClientContext());

        //Then
        assertThat(result.getForename(), is("Tester"));
        assertThat(result.getSurname(), is("John"));
    }

    @Test
    public void findCustomerByAccountShouldThrowNotFoundExceptionIfNoCustomerIsReturned() {
        //Expect
        expectedException.expect(NotFoundException.class);

        //Given
        List<DomainObject> customerList = Collections.emptyList();
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        mockCustomerAccountService.findCustomerByAccount(IDENTIFICATION, createClientContext());
    }

    @Test
    public void customerAccountHasEnoughFundsShouldThrowAPIExceptionIfIdentificationLessThan16Digits() {
        //Given
        expectedException.expect(BadRequestException.class);
        expectedException.expectMessage("Identification not Valid");

        //When
        mockCustomerAccountService.customerAccountHasEnoughFunds("01808692", IDENTIFICATION, BigDecimal.valueOf(20), createClientContext());
    }

    @Test
    public void customerAccountHasEnoughFundsShouldThrowNotFoundExceptionIfNoAccountIsReturned() {
        //Expect
        expectedException.expect(NotFoundException.class);

        //Given
        List<DomainObject> customerList = Collections.emptyList();
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        mockCustomerAccountService.customerAccountHasEnoughFunds("01808692", IDENTIFICATION_ACCOUNT_TYPE, BigDecimal.valueOf(20), createClientContext());
    }

    @Test
    public void customerAccountHasEnoughFundsShouldReturnTrueIfAccountBalanceIsMoreThanAmount() {
        //Given
        final CurrentAccount currentAccount =
            createCurrentAccount(IDENTIFICATION_ACCOUNT_TYPE,
            BigDecimal.valueOf(30),
            BigDecimal.valueOf(5),
            VALID_PRODUCT);

        final List<DomainObject> customerList = Collections.singletonList(currentAccount);
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        Optional<Boolean> result = mockCustomerAccountService.customerAccountHasEnoughFunds("01808692", IDENTIFICATION_ACCOUNT_TYPE, BigDecimal.valueOf(20), createClientContext());

        //Then
      assertThat(result.isPresent(), is(false)); // TODO: Mainframe simulator should return status but doesn't

    }

    @Test
    public void customerAccountHasEnoughFundsShouldReturnFalseIfAccountBalanceIsLessThanAmount() {
        //Given
        final CurrentAccount currentAccount =
            createCurrentAccount(IDENTIFICATION_ACCOUNT_TYPE,
                BigDecimal.valueOf(10),
                BigDecimal.valueOf(5),
                VALID_PRODUCT);

        final List<DomainObject> customerList = Collections.singletonList(currentAccount);
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        Optional<Boolean> result = mockCustomerAccountService.customerAccountHasEnoughFunds("01808692", IDENTIFICATION_ACCOUNT_TYPE, BigDecimal.valueOf(20), createClientContext());

        //Then
      assertThat(result.isPresent(), is(false)); // TODO: Mainframe simulator should return status but doesn't
    }


    @Test
    public void customerAccountIsBlocked() {
        //Given
        final CurrentAccount currentAccount =
            createCurrentAccount(IDENTIFICATION_ACCOUNT_TYPE,
                BigDecimal.valueOf(10),
                BigDecimal.valueOf(5),
                VALID_PRODUCT);
        currentAccount.setAccountStatus(AccountStatus.ACCOUNT_UNAVAILABLE);

        final List<DomainObject> customerList = Collections.singletonList(currentAccount);
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        Optional<Boolean> result = mockCustomerAccountService.customerAccountHasEnoughFunds("01808692", IDENTIFICATION_ACCOUNT_TYPE, BigDecimal.valueOf(20), createClientContext());

        //Then
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void customerAccountHasInvalidProduct() {

        //Expect
        expectedException.expect(NotFoundException.class);

        //Given
        final CurrentAccount currentAccount =
            createCurrentAccount(IDENTIFICATION_ACCOUNT_TYPE,
                BigDecimal.valueOf(30),
                BigDecimal.valueOf(5),
                INVALID_PRODUCT);

        final List<DomainObject> customerList = Collections.singletonList(currentAccount);
        given(mockCallUtil.call(any(EnterpriseServiceCall.class), any(ClientContext.class), anyString())).willReturn(customerList);

        //When
        Optional<Boolean> result = mockCustomerAccountService.customerAccountHasEnoughFunds("01808692", IDENTIFICATION_ACCOUNT_TYPE, BigDecimal.valueOf(20), createClientContext());

        //Then
        assertThat(result.isPresent(), is(false)); // TODO: Mainframe simulator should return status but doesn't

    }

    private ClientContext createClientContext() {
        final ClientContext clientContext = ClientContext.builder().build();
        clientContext.setIpAddress("192.134.2.1");
        clientContext.setBrowser("internet");
        clientContext.setDomain("coop");
        clientContext.setAuthenticationToken("dummy");
        clientContext.setReferrerSite("www.smile.com");
        clientContext.setRestartKey("");
        return clientContext;
    }

    private CurrentAccount createCurrentAccount(String identification, BigDecimal clearedBal, BigDecimal odLimit, String productCode) {
        CurrentAccount currentAccount = new CurrentAccount();
        currentAccount.setIdentifier(new DomesticAccount.ID(identification));
        currentAccount.setForecastClearedBalance(clearedBal);
        currentAccount.setOverdraftLimit(odLimit);
        currentAccount.setProductCode(productCode);
        return currentAccount;
    }
}