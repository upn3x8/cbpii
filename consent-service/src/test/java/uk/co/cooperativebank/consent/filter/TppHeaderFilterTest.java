package uk.co.cooperativebank.consent.filter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TppHeaderFilterTest {

  @Mock
  HttpServletRequest httpServletRequest;

  @Mock
  HttpServletResponse mockResponse;

  @Mock
  FilterChain mockChain;

  @Before
  public void setUp() {
    initMocks(this);

//    when(httpServletRequest.getHeader(anyString())).thenReturn(null);

  }

  @Test
  public void testJoiningCertHeaders() throws IOException, ServletException {

    when(httpServletRequest.getHeader(TppHeaderFilter.X_SSL_CLIENT_CERT_SUBJECT_HEADER)).thenReturn("subject");
    when(httpServletRequest.getHeader(TppHeaderFilter.X_SSL_CLIENT_CERT_ISSUER_HEADER)).thenReturn("issuer");

    TppHeaderFilter filter = new TppHeaderFilter();

    ArgumentCaptor<MutableHttpServletRequest> argCaptor = ArgumentCaptor.forClass(MutableHttpServletRequest.class);
    filter.doFilter(httpServletRequest, mockResponse, mockChain);

    verify(mockChain).doFilter(argCaptor.capture(), any(ServletResponse.class));

    MutableHttpServletRequest argCaptorValue = argCaptor.getValue();

    assertThat(argCaptorValue.getHeader("x-tpp-id"), is("subjectissuer"));

  }

  @Test
  public void testInjectingTppHeaderBeforeJoiningCertHeaders() throws IOException, ServletException {

    when(httpServletRequest.getHeader("x-tpp-id")).thenReturn("injected_header");
    when(httpServletRequest.getHeader(TppHeaderFilter.X_SSL_CLIENT_CERT_SUBJECT_HEADER)).thenReturn("subject");
    when(httpServletRequest.getHeader(TppHeaderFilter.X_SSL_CLIENT_CERT_ISSUER_HEADER)).thenReturn("issuer");

    TppHeaderFilter filter = new TppHeaderFilter();

    ArgumentCaptor<MutableHttpServletRequest> argCaptor = ArgumentCaptor.forClass(MutableHttpServletRequest.class);
    filter.doFilter(httpServletRequest, mockResponse, mockChain);

    verify(mockChain).doFilter(argCaptor.capture(), any(ServletResponse.class));

    MutableHttpServletRequest argCaptorValue = argCaptor.getValue();

    assertThat(argCaptorValue.getHeader("x-tpp-id"), is("subjectissuer"));

  }

}