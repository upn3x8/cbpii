package uk.co.cooperativebank.consent.filter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestOverrideFilterTest {

  @Mock
  HttpServletRequest httpServletRequest;

  @Mock
  HttpServletResponse mockResponse;

  @Mock
  FilterChain mockChain;

  @Before
  public void setUp() {
    initMocks(this);

    when(httpServletRequest.getHeader(anyString())).thenReturn(null);

  }

  @Test
  public void testSimpleHeader() throws IOException, ServletException {

    when(httpServletRequest.getQueryString()).thenReturn("header-x-tpp-id=123456789&header-hello-world=hello(%22world%22)&header-no-second-part=&header-no-equals");

    TestOverrideFilter filter= new TestOverrideFilter();

    ArgumentCaptor<MutableHttpServletRequest> argCaptor = ArgumentCaptor.forClass(MutableHttpServletRequest.class);
    filter.doFilter(httpServletRequest, mockResponse, mockChain);

    verify(mockChain).doFilter(argCaptor.capture(), any(ServletResponse.class));

    MutableHttpServletRequest argCaptorValue = argCaptor.getValue();

    assertThat(argCaptorValue.getHeader("x-tpp-id"), is("123456789"));
    assertThat(argCaptorValue.getHeader("hello-world"), is("hello(\"world\")"));
    assertNull(argCaptorValue.getHeader("no-second-part"));
    assertNull(argCaptorValue.getHeader("no-equals"));
  }

}