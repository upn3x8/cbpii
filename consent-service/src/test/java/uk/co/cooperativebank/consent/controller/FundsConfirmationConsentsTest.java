package uk.co.cooperativebank.consent.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import uk.co.cooperativebank.ApplicationConfiguration;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmation;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsent;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApproval;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApprove;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentCreate;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentCreateData;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentCreateDataDebtorAccount;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentListResponse;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentResponse;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationData;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationDataInstructedAmount;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationResponse;
import uk.co.cooperativebank.consent.filter.ClientContextFilter;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.TimeZone;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmation.UK_OBIE_SORT_CODE_ACCOUNT_NUMBER;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsent.StatusEnum.AUTHORISED;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsent.StatusEnum.REJECTED;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsent.StatusEnum.REVOKED;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApproval.ActionEnum.APPROVE;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApproval.ActionEnum.REJECT;
import static uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApproval.ActionEnum.REVOKE;

@ActiveProfiles("local")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@WebAppConfiguration
@Transactional
public class FundsConfirmationConsentsTest {
  private static final int SHORT_CONSENT_LIFETIME = 5;  //SECONDS
  private static final int SHORT_CONSENT_LIFETIME_MILLIS = SHORT_CONSENT_LIFETIME * 1000;  //SECONDS
  private static final int TYPICAL_CONSENT_LIFETIME = 90;
  private static final String VALID_TPP_ID = "46";
  private static final String VALID_DCID = "123456789012";
  private static final String TWENTY_POUNDS = "20.00";
  private static final String BASE64_CONTEXT = "eyJpcEFkZHJlc3MiOiIxMC4xMDIuMTA5LjY4IiwiYnJvd3NlciI6Ik5ldHNjYXBlLTQuMCIsImRvbWFpbiI6IjEwLjEwMi4xMDkuNjgiLCJvc1R5cGUiOiIkb3NUeXBlIiwicmVmZXJyZXJTaXRlIjoiJHJlZmVycmVyU2l0ZSIsInNlc3Npb24iOiIwMDAwVmJEVkphTTBuTkN1cXFsVGlZRkRzTTY6LTEiLCJhdXRoZW50aWNhdGlvblRva2VuIjoiVmJEVkphTTBuTkN1cXFsVGlZRkRzTTY7OC9MakdXaDlaMXcqVmMoeG1wN3g3UFYhUytENypSU05GSzQ0UGw4NFQlUCIsImNoYW5uZWwiOiJpbnRlcm5ldCIsImJyYW5kIjoiY29vcCIsInJlc3RhcnRLZXkiOm51bGwsImpTZXNzaW9uSWQiOiJWYkRWSmFNMG5OQ3VxcWxUaVlGRHNNNiIsInNlcnZpY2VJZGVudGlmaWVyIjpudWxsLCJ1c2VyQWdlbnQiOiJNb3ppbGxhLzQuMCIsImJyb3dzZXJVcmwiOiJodHRwOi8vbG9jYWxob3N0Ojg4ODgvQ0Iva2R3I19mcm1Mb2dpblN0YXRlbWVudHMiLCJ4Rm9yd2FyZGVkRm9yIjpudWxsLCJ3aWRnZXREYXRhIjpudWxsLCJ0UFBJRCI6bnVsbH0=";
  private static final String BASE64_CONTEXT_EMPTY = "e30="; // valid but no properties
  private static final String X_TPP_ID = "x-tpp-id";
  private static final long INVALID_CONSENT_EXPIRY = 14;
  private static final String SORTCODE_ACCOUNT_NO = "08030822556722";
  private static final String GBP_CURRENCY = "GBP";
  private static final String INVALID_TPP_ID = "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
  private static final String BLOCKED_ACCOUNT = "08030822556722";

  @Autowired
  private WebApplicationContext wac;

  @Autowired
  private ObjectMapper mapper;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }

  @Test
  public void fundsConfirmationHappyPath() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised by the customer
    authoriseConsent(consentId, APPROVE, status().isOk());

    // then I expect the consent to be authorised
    checkConsentHasStatus(consentId, AUTHORISED);

    // and funds confirmation checks to work
    OBFundsConfirmationResponse fundsConfirmationResponse = performFundsCheck(consentId, TWENTY_POUNDS, GBP_CURRENCY, status().isCreated());

    assertTrue("should have funds", fundsConfirmationResponse.getData().isFundsAvailable());

  }

  @Ignore
  @Test
  public void fundsConfirmationBlockedAccount() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = createObFundsConfirmationConsentWithID(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME), BLOCKED_ACCOUNT);

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate, VALID_TPP_ID, status().isBadRequest());

  }

  @Test
  public void fundsConfirmationInvalidTppLength() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    createConsentRequest(fundsConfirmationConsentCreate, INVALID_TPP_ID, status().isBadRequest());

  }

  @Test
  public void fundsConfirmationBlankIdentification() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));
    fundsConfirmationConsentCreate.getData().getDebtorAccount().setIdentification("");

    // when they submit the funds confirmation consent request
    createConsentRequest(fundsConfirmationConsentCreate, VALID_TPP_ID, status().isBadRequest());

  }

  @Test
  public void fundsConfirmationBlankExpiryDate() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));
    fundsConfirmationConsentCreate.getData().setExpirationDateTime(null);

    // when they submit the funds confirmation consent request
    createConsentRequest(fundsConfirmationConsentCreate, VALID_TPP_ID, status().isBadRequest());

  }

  @Test
  public void insufficientFundsShouldReturnFalseForIsFundsUnAvailable() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised by the customer
    authoriseConsent(consentId, APPROVE, status().isOk());

    // then I expect the consent to be authorised
    checkConsentHasStatus(consentId, AUTHORISED);

    // and funds confirmation checks to work
    OBFundsConfirmationResponse fundsConfirmationResponse = performFundsCheck(consentId, "9999999999.99", GBP_CURRENCY, status().isCreated());

    assertFalse("should not have funds", fundsConfirmationResponse.getData().isFundsAvailable());

  }

  @Test
  public void expiredConsentCannotPerformFundsCheck() throws Exception {

    // given a registered CBPII TPP has built a consent request payload with a short lifetime
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusSeconds(SHORT_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised by the customer
    authoriseConsent(consentId, APPROVE, status().isOk());

    // then I expect the consent to be authorised
    checkConsentHasStatus(consentId, AUTHORISED);

    // and funds confirmation checks to work briefly
    performFundsCheck(consentId, TWENTY_POUNDS, GBP_CURRENCY, status().isCreated());


    // when the consent period has expired
    Thread.sleep(SHORT_CONSENT_LIFETIME_MILLIS);

    // then I expect further funds confirmation checks to fail
    performFundsCheck(consentId, TWENTY_POUNDS, GBP_CURRENCY, status().isNotFound());

  }

  @Test
  public void fundsConfirmationNonGBPCurrencyShouldFail() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
        OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised by the customer
    authoriseConsent(consentId, APPROVE, status().isOk());

    // then I expect the consent to be authorised
    checkConsentHasStatus(consentId, AUTHORISED);

    // and funds confirmation checks to work
    OBFundsConfirmationResponse fundsConfirmationResponse = performFundsCheck(consentId, TWENTY_POUNDS, "USD", status().isBadRequest());

  }

  @Test
  public void expiredConsentCannotBeAuthorised() throws Exception {

    // given a registered CBPII TPP has built a consent request payload with a short lifetime
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusSeconds(SHORT_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // and funds confirmation checks to work

    // when the consent has expired before the Customer has authorised it

    Thread.sleep(SHORT_CONSENT_LIFETIME_MILLIS);

    authoriseConsent(consentId, APPROVE ,status().isNotFound());

  }


  @Test
  public void shouldThrowAnApiExceptionIfNoClientContextIsProvided() throws Exception {
    // given a registered CBPII TPP has built a consent request payload with a short lifetime
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusSeconds(SHORT_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request without a client context
    this.mockMvc.perform(post("/funds-confirmation-consents")
      .content(asJsonString(mapper, fundsConfirmationConsentCreate))
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON)
      .header(X_TPP_ID, VALID_TPP_ID))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andReturn();
  }

  @Test
  public void should_400BadRequest_Given_ExpiryAfter13Months() throws Exception {

    // given a registered CBPII TPP has built a consent request payload with a short lifetime
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusMonths(INVALID_CONSENT_EXPIRY));

    // when they submit the funds confirmation consent request
    createConsentRequest(fundsConfirmationConsentCreate, VALID_TPP_ID, status().isBadRequest());

   }

  @Test
  public void should_400BadRequest_Given_ExpiryBeforeNow() throws Exception {

    // given a registered CBPII TPP has built a consent request payload with a short lifetime
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusMonths(-1));

    // when they submit the funds confirmation consent request
    createConsentRequest(fundsConfirmationConsentCreate, VALID_TPP_ID, status().isBadRequest());

  }

  @Test
  public void rejectedConsentCannotDoFundsCheck() throws Exception {
    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate

    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised by the customer
    authoriseConsent(consentId, REJECT, status().isOk());

    // then I expect the consent to be rejected
    checkConsentHasStatus(consentId, REJECTED);

    // and funds confirmation checks to work
    performFundsCheck(consentId, TWENTY_POUNDS, GBP_CURRENCY, status().isNotFound());

  }

  @Test
  public void revokedConsentCannotDoFundsCheck() throws Exception {
    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate

    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
        OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised then revoked by the customer
    authoriseConsent(consentId, APPROVE, status().isOk());
    authoriseConsent(consentId, REVOKE, status().isOk());


    // then I expect the consent to be rejected
    checkConsentHasStatus(consentId, REVOKED);

    // and funds confirmation checks to work
    performFundsCheck(consentId, TWENTY_POUNDS, GBP_CURRENCY, status().isNotFound());

  }

  @Test
  public void cannotRevokeConsentNotAlreadyAuthorised() throws Exception {
    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate

    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = createConsentRequest(fundsConfirmationConsentCreate);

    // then i expect a consent to have been created with awaiting authorisation status
    OBFundsConfirmationConsentResponse consentResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
        OBFundsConfirmationConsentResponse.class);

    String consentId = consentResponse.getData().getConsentId();
    assertEquals("creation status incorrect", OBFundsConfirmationConsent.StatusEnum.AWAITINGAUTHORISATION, consentResponse.getData().getStatus());

    // when the consent has been authorised then revoked by the customer
    authoriseConsent(consentId, REVOKE, status().isNotFound());

  }


  @Test
  public void shouldReturnBadRequestResponseIfContextHeaderNotProvided() throws Exception {
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = this.mockMvc.perform(post("/funds-confirmation-consents")
            .content(asJsonString(mapper, fundsConfirmationConsentCreate))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .header(X_TPP_ID, VALID_TPP_ID))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andReturn();

    assertThat(mvcResult.getResponse().getStatus(), is(400));
  }

  @Test
  public void shouldReturnBadRequestResponseIfContextHeaderProvidedIsEmpty() throws Exception {
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    MvcResult mvcResult = this.mockMvc.perform(post("/funds-confirmation-consents")
            .content(asJsonString(mapper, fundsConfirmationConsentCreate))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .header(X_TPP_ID, VALID_TPP_ID)
            .header(ClientContextFilter.CLIENT_CONTEXT_HEADER, BASE64_CONTEXT_EMPTY))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andReturn();

    assertThat(mvcResult.getResponse().getStatus(), is(400));
  }

  @Test
  public void shouldReturnExceptionIfAccountIDIsInvalid() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    final OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = createObFundsConfirmationConsentWithID(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME), "08030822556720");

    // when they submit the funds confirmation consent request
    this.mockMvc.perform(post("/funds-confirmation-consents")
            .content(asJsonString(mapper, fundsConfirmationConsentCreate))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .header(X_TPP_ID, VALID_TPP_ID)
            .header(ClientContextFilter.CLIENT_CONTEXT_HEADER, BASE64_CONTEXT))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andReturn();
  }

  @Test
  public void fundsConfirmationList() throws Exception {

    // given a registered CBPII TPP has built a typical consent request payload
    // Create a OBFundsConfirmationConsentCreate
    OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate = getObFundsConfirmationConsentCreate(OffsetDateTime.now().plusDays(TYPICAL_CONSENT_LIFETIME));

    // when they submit the funds confirmation consent request
    createConsentRequest(fundsConfirmationConsentCreate);
    createConsentRequest(fundsConfirmationConsentCreate);

    List<OBFundsConfirmationConsent> stuff = getListOfConsents(VALID_TPP_ID, VALID_DCID);

    assertThat(stuff.size(), is(2));

  }

  @Ignore
  @Test
  public void notACustomer() {

  }

  @Ignore
  @Test
  public void jointPrimaryCustomer() {

  }

  @Ignore
  @Test
  public void jointNonPrimaryCustomer() {

  }

  private List<OBFundsConfirmationConsent> getListOfConsents(String tppId, String dcId) throws Exception {
    MvcResult mvcResult = this.mockMvc.perform(get("/funds-confirmation-consents")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .header(X_TPP_ID, VALID_TPP_ID)
        .header(ClientContextFilter.CLIENT_CONTEXT_HEADER, BASE64_CONTEXT)
        .param("dcid", dcId))
        .andDo(print())
        .andExpect(status().isOk())
        .andReturn();

    OBFundsConfirmationConsentListResponse listResponse = mapper.readValue(mvcResult.getResponse().getContentAsString(),
        OBFundsConfirmationConsentListResponse.class);
    return listResponse.getData();
  }


  private OBFundsConfirmationConsentCreate getObFundsConfirmationConsentCreate(OffsetDateTime expiryDateTime) {
    return createObFundsConfirmationConsentWithID(expiryDateTime, SORTCODE_ACCOUNT_NO);
  }

  private OBFundsConfirmationConsentCreate createObFundsConfirmationConsentWithID(OffsetDateTime expiryDateTime, String identification) {
    return OBFundsConfirmationConsentCreate.builder().data(
      OBFundsConfirmationConsentCreateData.builder()
        .debtorAccount(buildTestAccount(identification))
        .expirationDateTime(expiryDateTime)
        .build())
      .build();
  }

  private OBFundsConfirmationConsentCreateDataDebtorAccount buildTestAccount(String identification) {
    return OBFundsConfirmationConsentCreateDataDebtorAccount.builder().identification(identification).schemeName(UK_OBIE_SORT_CODE_ACCOUNT_NUMBER).name("fred").secondaryIdentification("flintstone").build();
  }

  private OBFundsConfirmationResponse performFundsCheck(String consentId, String amount, String currency, ResultMatcher resultMatcher) throws Exception {
    MvcResult mvcResult;
    OBFundsConfirmation fundsConfirmation = OBFundsConfirmation.builder()
      .data(OBFundsConfirmationData.builder()
        .consentId(consentId)
        .reference("tpp supplied value£")
        .instructedAmount(OBFundsConfirmationDataInstructedAmount.builder()
          .amount(amount)
          .currency(currency)
          .build())
        .build())
      .build();


    mvcResult = this.mockMvc.perform(post("/funds-confirmations")
      .content(asJsonString(mapper, fundsConfirmation))
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON)
      .header(X_TPP_ID, VALID_TPP_ID)
      .header(ClientContextFilter.CLIENT_CONTEXT_HEADER, BASE64_CONTEXT))
      .andDo(print())
      .andExpect(resultMatcher)
      .andReturn();

    return mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationResponse.class);
  }

  private MvcResult createConsentRequest(OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate) throws Exception {
    return createConsentRequest(fundsConfirmationConsentCreate, VALID_TPP_ID, status().isCreated());
  }

  private MvcResult createConsentRequest(OBFundsConfirmationConsentCreate fundsConfirmationConsentCreate, String tppId, ResultMatcher matcher) throws Exception {
    return this.mockMvc.perform(post("/funds-confirmation-consents")
      .content(asJsonString(mapper, fundsConfirmationConsentCreate))
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON)
      .header(X_TPP_ID, tppId)
      .header(ClientContextFilter.CLIENT_CONTEXT_HEADER, BASE64_CONTEXT))
      .andDo(print())
      .andExpect(matcher)
      .andReturn();
  }

  private void authoriseConsent(String consentId, OBFundsConfirmationConsentApproval.ActionEnum action, ResultMatcher resultMatcher) throws Exception {
    OBFundsConfirmationConsentApprove fundsConfirmationConsentApproval = OBFundsConfirmationConsentApprove.builder()
      .data(OBFundsConfirmationConsentApproval.builder()
        .consentId(consentId)
        .action(action).build())
      .build();


    this.mockMvc.perform(post("/funds-confirmation-consent-approvals?dcId=DCID-1")
      .content(asJsonString(mapper, fundsConfirmationConsentApproval))
      .contentType(MediaType.APPLICATION_JSON)
      .accept(MediaType.APPLICATION_JSON)
      .header(X_TPP_ID, VALID_TPP_ID))
      .andDo(print())
      .andExpect(resultMatcher)
      .andReturn();
  }

  private void checkConsentHasStatus(String consentId, OBFundsConfirmationConsent.StatusEnum statusEnum) throws Exception {
    MvcResult mvcResult;
    mvcResult = this.mockMvc.perform(get("/funds-confirmation-consents/" + consentId + "/?dcId=DCID-1")
      .accept(MediaType.APPLICATION_JSON)
      .header(X_TPP_ID, VALID_TPP_ID))
      .andDo(print())
      .andExpect(status().isOk())
      .andReturn();

    OBFundsConfirmationConsentResponse fundsConfirmationConsent = mapper.readValue(mvcResult.getResponse().getContentAsString(),
      OBFundsConfirmationConsentResponse.class);

    assertEquals(fundsConfirmationConsent.getData().getStatus(), statusEnum);
  }

  private static String asJsonString(ObjectMapper mapper, final Object obj) {
    try {
      return mapper.writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}