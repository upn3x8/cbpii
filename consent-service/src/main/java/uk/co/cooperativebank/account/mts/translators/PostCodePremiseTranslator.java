package uk.co.cooperativebank.account.mts.translators;

import uk.co.cooperativebank.account.mts.dao.address.Premises;
import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Translates postcode premises
 */
public class PostCodePremiseTranslator extends AbstractHostTranslator {

    @Override
    public String encode(Object obj, int inFieldLength, int outFieldLength) throws HostFieldFormatException {

        //Not implemented as no encoding required for postcode premises
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object decode(String str) throws HostFieldFormatException {

        Premises premises = new Premises();
        List<String> premisesList = new ArrayList<>();

        if (str != null && !str.isEmpty()) {
            String premiseStr = str.trim();
            Matcher matcher = Pattern.compile(".{1,56}").matcher(premiseStr);

            while (matcher.find()) {
                premisesList.add(premiseStr.substring(matcher.start(), matcher.end()).trim());
            }

        }
        premises.setPremisesList(premisesList);
        return premises;

    }

}
