package uk.co.cooperativebank.account.config;

import com.rabbitmq.jms.admin.RMQDestination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiTemplate;

import javax.jms.Queue;
import javax.naming.NamingException;

@Slf4j
@Configuration
public class MQConfigSmile {

  private final Environment env;

  public MQConfigSmile(Environment env) {
    this.env = env;
  }

  // local reply queue Smile
  @Profile("local")
  @Bean("replyQueueSM")
  public Queue replyQueueSM() {
    return new RMQDestination("replyQueue", "", "replyQ", "replyQ");
  }

  // WAS reply queue Smile
  @Profile("!local")
  @Bean("replyQueueSM")
  public Queue getJndireplyQueueSM() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.reply-sm.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (Queue) new JndiTemplate().lookup(jndiLookupName);
  }

  // local account enq. queue Smile
  @Profile("local")
  @Bean("accountEnquiryQueueSM")
  public Queue getLocalAccountEnquiryQueueSM() {
    return new RMQDestination("accountEnquiry", "", "accountEnquiry", "accountEnquiry");
  }

  // WAS account enq. queue Smile
  @Profile("!local")
  @Bean("accountEnquiryQueueSM")
  public Queue accountEnquiryQueueSM() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.account-enq-sm.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (Queue) new JndiTemplate().lookup(jndiLookupName);
  }

  // local customer enq. queue - Smile
  @Profile("local")
  @Bean("customerEnquiryQueueSM")
  Queue getLocalCustomerEnquiryQueueSM() {
    return new RMQDestination("customerEnquiryQ", "", "customerEnquiryQ", "customerEnquiryQ");
  }

  // WAS customer enq. queue - Smile
  @Profile("!local")
  @Bean("customerEnquiryQueueSM")
  public Queue getCustomerEnquiryQueueSM() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.customer-enq-sm.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (Queue) new JndiTemplate().lookup(jndiLookupName);
  }

}