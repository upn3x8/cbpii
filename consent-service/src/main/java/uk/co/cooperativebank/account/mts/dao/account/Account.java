package uk.co.cooperativebank.account.mts.dao.account;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import uk.co.cooperativebank.account.mts.dao.product.Product;
import uk.co.cooperativebank.account.mts.framework.DomainObject;
import uk.co.cooperativebank.account.mts.framework.Identifier;

import javax.validation.Valid;
import java.util.Date;

/**
 * An account opened by a
 * Customer and held with the
 * Cooperative bank.
 *
 * This is a base-implementation of common account features.
 *
 * @param <T>
 * @see CreditCardAccount
 * @see DomesticAccount
 * @see MortgageAccount
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "_accountType")
public abstract class Account<T extends Account> implements DomainObject {

    /**
     * Serialisation UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The unique {@link Identifier} of the {@link Account}.
     */
    @Valid
    private Account.ID<T> identifier;

    /**
     * The Product that this account is an instance of
     *
     * @see Product
     */
    private Product product;

    /**
     * The boolean to determine whether the account has card
     */
    private boolean hasCard;

    /**
     * The value to indicate a customers account statement preference X -paper
     * only N -Paper enabled Y -Paperless enabled P -Paperless only
     */
    private String onlineStatementPreferenceFlag;

    private Date accountOpeningDate;

    private String productCode;

    private String companyCode;

    private String personalCorporateIndicator;

    private boolean isSelected;

    private String status;

    //Derived from status
    private AccountStatus accountStatus;

    //Initialize to true as default. If detail is unavailable then explicitly
    //set to false using setter method.
    private boolean isDetailAvailable = true;

    private String title;

    private String revenueType;

    private String revenueType2Char;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getPersonalCorporateIndicator() {
        return personalCorporateIndicator;
    }

    public void setPersonalCorporateIndicator(String personalCorporateIndicator) {
        this.personalCorporateIndicator = personalCorporateIndicator;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        //Decode the status code to the applicable enumeraton
        this.accountStatus = AccountStatus.getAccountStatusForCode(status);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isHasCard() {
        return hasCard;
    }

    public void setHasCard(boolean hasCard) {
        this.hasCard = hasCard;
    }

    public final Account.ID<T> getIdentifier() {
        return identifier;
    }

    public final void setIdentifier(final Account.ID<T> identifier) {
        this.identifier = identifier;
    }

    public final Product getProduct() {
        return product;
    }

    public final void setProduct(Product product) {
        this.product = product;
    }

    public String getOnlineStatementPreferenceFlag() {
        return onlineStatementPreferenceFlag;
    }

    public void setOnlineStatementPreferenceFlag(String onlineStatementPreferenceFlag) {
        this.onlineStatementPreferenceFlag = onlineStatementPreferenceFlag;
    }

    public Date getAccountOpeningDate() {
        return accountOpeningDate;
    }

    public final void setAccountOpeningDate(Date accountOpeningDate) {
        this.accountOpeningDate = accountOpeningDate;
    }

    /**
     * The helper method to get the volatile Account details
     *
     * @return data
     */
    public abstract VolatileAccountData getVolatileAccountData();

    /**
     * The helper method to clear all volatile data
     */
    public abstract void clearVolatileAccountData();

    /**
     * The helper method to clear all volatile data
     *
     * @param volatileAccountData data
     */
    public abstract void addVolatileAccountData(VolatileAccountData volatileAccountData);

    public abstract void prepareForPresentation();

    /**
     * Method used to determine whether the account is safe to make subsequent
     * calls to the mainframe based on {@link Account#status}
     *
     * @return boolean
     */
    public abstract boolean isAccountVolatileDataAvailable();

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public boolean isIsDetailAvailable() {
        return isDetailAvailable;
    }

    public void setIsDetailAvailable(boolean isDetailAvailable) {
        this.isDetailAvailable = isDetailAvailable;
    }

    public String getRevenueType() {
        return revenueType;
    }

    public void setRevenueType(String revenueType) {
        this.revenueType = revenueType;
    }

    public String getRevenueType2Char() {
        return revenueType2Char;
    }

    public void setRevenueType2Char(String revenueType2Char) {
        this.revenueType2Char = revenueType2Char;
    }

    /**
     * The unique {@link Identifier} of an {@link Account}.
     *
     * The exact format of the {@link Account.ID} is determined by each
     * sub-type.
     *
     * @param <T> the type of {@link Account} this {@link Identifier} is used
     * for
     * @see CreditCardAccount.ID
     * @see DomesticAccount.ID
     * @see MortgageAccount.ID
     */
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "_type")
    @JsonSubTypes({
            @JsonSubTypes.Type(value = CreditCardAccount.ID.class, name = "CreditCard"),
            @JsonSubTypes.Type(value = DomesticAccount.ID.class, name = "Domestic"),
            @JsonSubTypes.Type(value = MortgageAccount.ID.class, name = "Mortgage")
    })
    public interface ID<T> extends Identifier {

        public static final String ZEROS_ACCOUNT_TYPE = "00";

        String fullIdentifierValue();

        String fullIdentifierValueWithTypeZeros();
    }
}
