package uk.co.cooperativebank.account.mts.contexts;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static uk.co.cooperativebank.consent.service.Constants.REGEX_NOT_BLANK;

@Getter
@Setter
@Builder
public class ClientContext implements java.io.Serializable {

  @NotNull
  private String ipAddress;

  private String browser;

  private String domain;

  private String osType;

  private String referrerSite;

  private String session;

  @Pattern(regexp = REGEX_NOT_BLANK)
  private String authenticationToken;

  @Pattern(regexp = REGEX_NOT_BLANK)
  private String channel;

  @Pattern(regexp = REGEX_NOT_BLANK)
  private String brand;

  //added for mainframe services
  private String restartKey;

  //added for user identification
  private String jSessionId;

  private String userAgent;

  private String browserUrl;

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}