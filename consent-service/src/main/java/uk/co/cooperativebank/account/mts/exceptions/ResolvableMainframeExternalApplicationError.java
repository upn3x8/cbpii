package uk.co.cooperativebank.account.mts.exceptions;

import static uk.co.cooperativebank.account.mts.exceptions.MainframeApplicationErrorCode.resolveMainframeApplicationErrorCodeToToken;

public class ResolvableMainframeExternalApplicationError extends MainframeExternalApplicationError {

    public ResolvableMainframeExternalApplicationError(String errorCode) {
        super(resolveMainframeApplicationErrorCodeToToken(errorCode));
    }
}