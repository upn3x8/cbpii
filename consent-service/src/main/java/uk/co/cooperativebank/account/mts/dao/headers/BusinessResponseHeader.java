package uk.co.cooperativebank.account.mts.dao.headers;

public class BusinessResponseHeader extends BasicResponseHeader {

    private String token;

    /**
     * Gets the token
     * @return Returns a String
     */
    public String getToken() {
        return token;
    }
    /**
     * Sets the token
     * @param token The token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        String inh = super.toString();
        StringBuilder output = new StringBuilder();
        if (!inh.endsWith("(unassigned)")) {
            output.append(inh);
        }
        appendProp(output, "token", token);
        if (output.length() == 0) {
            output.append("ResponseHeader: (unassigned)");
        }
        return output.toString();

    }
}
