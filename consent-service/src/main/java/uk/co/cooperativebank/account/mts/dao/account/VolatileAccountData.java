package uk.co.cooperativebank.account.mts.dao.account;


import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Digital-Dev
 */
public class VolatileAccountData implements Serializable {

    // volatile
    private BigDecimal forecastClearedBalanceWithAuthorisation;
    private BigDecimal overdraftLimit;
    private BigDecimal lastStatementBalance;
    private BigDecimal currentClearedBalance;
    private BigDecimal currentUnclearedBalance;
    private BigDecimal forecastClearedBalance;
    private BigDecimal forecastUnclearedBalance;
    private BigDecimal availableBalance;
    private BigDecimal originalMortgageAmount;
    private BigDecimal outstandingBalance;
    private BigDecimal originalLoanAmount;
    private BigDecimal availableToSpend;
    private BigDecimal availableCredit;
    private BigDecimal currentBalance;
    private BigDecimal creditLimit;
    private String informationMessage;

    public BigDecimal getForecastClearedBalanceWithAuthorisation() {
        return forecastClearedBalanceWithAuthorisation;
    }

    public void setForecastClearedBalanceWithAuthorisation(BigDecimal forecastClearedBalanceWithAuthorisation) {
        this.forecastClearedBalanceWithAuthorisation = forecastClearedBalanceWithAuthorisation;
    }

    public BigDecimal getOverdraftLimit() {
        return overdraftLimit;
    }

    public void setOverdraftLimit(BigDecimal overdraftLimit) {
        this.overdraftLimit = overdraftLimit;
    }

    public BigDecimal getLastStatementBalance() {
        return lastStatementBalance;
    }

    public void setLastStatementBalance(BigDecimal lastStatementBalance) {
        this.lastStatementBalance = lastStatementBalance;
    }

    public BigDecimal getCurrentClearedBalance() {
        return currentClearedBalance;
    }

    public void setCurrentClearedBalance(BigDecimal currentClearedBalance) {
        this.currentClearedBalance = currentClearedBalance;
    }

    public BigDecimal getCurrentUnclearedBalance() {
        return currentUnclearedBalance;
    }

    public void setCurrentUnclearedBalance(BigDecimal currentUnclearedBalance) {
        this.currentUnclearedBalance = currentUnclearedBalance;
    }

    public BigDecimal getForecastClearedBalance() {
        return forecastClearedBalance;
    }

    public void setForecastClearedBalance(BigDecimal forecastClearedBalance) {
        this.forecastClearedBalance = forecastClearedBalance;
    }

    public BigDecimal getForecastUnclearedBalance() {
        return forecastUnclearedBalance;
    }

    public void setForecastUnclearedBalance(BigDecimal forecastUnclearedBalance) {
        this.forecastUnclearedBalance = forecastUnclearedBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getOriginalMortgageAmount() {
        return originalMortgageAmount;
    }

    public void setOriginalMortgageAmount(BigDecimal originalMortgageAmount) {
        this.originalMortgageAmount = originalMortgageAmount;
    }

    public BigDecimal getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(BigDecimal outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public BigDecimal getOriginalLoanAmount() {
        return originalLoanAmount;
    }

    public void setOriginalLoanAmount(BigDecimal originalLoanAmount) {
        this.originalLoanAmount = originalLoanAmount;
    }

    public BigDecimal getAvailableToSpend() {
        return availableToSpend;
    }

    public void setAvailableToSpend(BigDecimal availableToSpend) {
        this.availableToSpend = availableToSpend;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(BigDecimal availableCredit) {
        this.availableCredit = availableCredit;
    }

    public String getInformationMessage() {
        return informationMessage;
    }

    public void setInformationMessage(String Status) {
        this.informationMessage = Status;
    }
}
