package uk.co.cooperativebank.account.mts.dao.account;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import uk.co.cooperativebank.account.mts.framework.Identifier;
import uk.co.cooperativebank.account.mts.templates.utilities.ToStringUtils;
import uk.co.cooperativebank.consent.service.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;


/**
 * An {@link Account} held by customers who have taken out a mortgage with
 * the Cooperative bank.
 */
@JsonTypeName("MortgageAccount")
public class MortgageAccount extends Account<MortgageAccount> {

  private BigDecimal originalMortgageAmount;
  private BigDecimal outstandingBalance;

  public final BigDecimal getOriginalMortgageAmount() {
    return originalMortgageAmount;
  }

  public final void setOriginalMortgageAmount(BigDecimal originalMortgageAmount) {
    this.originalMortgageAmount = originalMortgageAmount;
  }

  public final BigDecimal getOutstandingBalance() {
    return outstandingBalance;
  }

  public final void setOutstandingBalance(BigDecimal outstandingBalance) {
    this.outstandingBalance = outstandingBalance;
  }

  @Override
  public VolatileAccountData getVolatileAccountData() {
    return new VolatileAccountData();
  }

  /**
   * Volatile MortgageAccount data cannot be refreshed from the mainframe so
   * it must not be cleared
   */
  @Override
  public void clearVolatileAccountData() {
    /* Do nothing*/
  }

  @Override
  public void addVolatileAccountData(VolatileAccountData volatileAccountData) {
    setOriginalMortgageAmount(volatileAccountData.getOriginalMortgageAmount());
    setOutstandingBalance(volatileAccountData.getOutstandingBalance());
  }

  @Override
  public void prepareForPresentation() {
    /* Do nothing*/
  }

  /**
   * MortgageAccount balance data cannot be refreshed via host request 0073
   *
   * @return boolean indicating whether volatile account data can be retrieved from mainframe
   */
  @Override
  public boolean isAccountVolatileDataAvailable() {
    return false;
  }

  /**
   * The unique {@link Identifier} of a {@link MortgageAccount}.
   * <p>
   * The identifier is a 16-digit numeric code which breaks down in to the
   * following components:
   * <ul>
   * <li>6-digit sort code</li>>
   * <li>8-digit account number</li>
   * <li>2-digit account type</li>
   * </ul>
   * <p>
   * TODO: verify if this format is correct for mortgage accounts. Discussions
   * within WS2 seem to suggest it's not like this at all.
   * <p>
   * This type is immutable. If a {@link MortgageAccount} is assigned a new
   * identifier for any reason then a new {@link MortgageAccount.ID} instance
   * should be created.
   */
  public static class ID implements Account.ID<MortgageAccount> {

    private static final long serialVersionUID = 1L;

    /**
     * The sort code component of the identifier.
     */
    @NotNull
    @Pattern(regexp = "\\d{6}")
    private final String sortCode;

    /**
     * The account number component of the identifier.
     */
    @NotNull
    @Pattern(regexp = "\\d{8}")
    private final String accountNumber;

    /**
     * The account type component of the identifier.
     */
    //TODO: need to investigate if an enum is more suitable here.
    //What are the possible values?
    @NotNull
    @Pattern(regexp = "\\d{2}")
    private final String accountType;

    /**
     * Default constructor required by Jackson for (de)serialization.
     */
    public ID() {
      sortCode = null;
      accountNumber = null;
      accountType = null;
    }

    public ID(String identifier) {

      this.sortCode = identifier.substring(0, Constants.SORT_CODE_INDEX);
      this.accountNumber = identifier.substring(Constants.SORT_CODE_INDEX, Constants.ACCOUNT_NUMBER_INDEX);
      this.accountType = identifier.substring(Constants.ACCOUNT_NUMBER_INDEX);
    }

    @JsonCreator
    public ID(@JsonProperty("sortCode") final String sortCode,
              @JsonProperty("accountNumber") final String accountNumber,
              @JsonProperty("accountType") final String accountType) {
      this.sortCode = sortCode;
      this.accountNumber = accountNumber;
      this.accountType = accountType;
    }

    public String getSortCode() {
      return sortCode;
    }

    public String getAccountNumber() {
      return accountNumber;
    }

    public String getAccountType() {
      return accountType;
    }

    @Override
    public String fullIdentifierValue() {
      return getSortCode() + getAccountNumber() + getAccountType();
    }

    @Override
    public boolean equals(Object obj) {
      return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
      return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
      return ToStringUtils.toString(this);
    }

    @Override
    public String fullIdentifierValueWithTypeZeros() {
      return getSortCode() + getAccountNumber() + ID.ZEROS_ACCOUNT_TYPE;
    }
  }
}