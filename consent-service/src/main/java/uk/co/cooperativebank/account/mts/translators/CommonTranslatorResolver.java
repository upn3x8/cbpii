package uk.co.cooperativebank.account.mts.translators;

import org.springframework.stereotype.Component;
import uk.co.cooperativebank.account.mts.templates.TranslatorResolver;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides common Translators
 * @author Digital-Dev
 */
@Component
public class CommonTranslatorResolver implements TranslatorResolver {


    private static final Map<String, HostTranslatorInterface> TRANSLATORS = new HashMap<>();

    static {
        TRANSLATORS.put("BigDecimalToMoney", new BigDecimalToMoneyTranslator());
        TRANSLATORS.put("java.util.Date", new DateTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.digital.data.EncryptedString", new StringEncryptionTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.digital.data.Premises", new PostCodePremiseTranslator());
    }


    @Override
    public HostTranslatorInterface getTranslator(String type) {
        return TRANSLATORS.get(type);
    }

    @Override
    public boolean canResolve(String type) {
        return TRANSLATORS.containsKey(type);
    }

}
