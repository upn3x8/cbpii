package uk.co.cooperativebank.account.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import uk.co.cooperativebank.account.dtos.LiveReferenceData;
import uk.co.cooperativebank.account.dtos.ReferenceDataMaps;
import uk.co.cooperativebank.account.mts.dao.product.DigitalProductCategory;
import uk.co.cooperativebank.account.mts.dao.product.Product;
import uk.co.cooperativebank.account.mts.framework.DomainObject;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class ReferenceDataConfig {

  private static final String SELECT_REF_DATA = "SELECT JSON_DATA, DATA_TYPE, DATA_KEY, SORT_BY FROM"
      + " REF_DATA_LIVE refData WHERE DATA_TYPE=? ORDER BY refData.DATA_TYPE, refData.SORT_BY";
  private static final String JSON_DATA_COLUMN = "JSON_DATA";
  private static final String DATA_TYPE_COLUMN = "DATA_TYPE";
  private static final String DATA_KEY_COLUMN = "DATA_KEY";
  private static final String SORT_BY_COLUMN = "SORT_BY";

  private JdbcTemplate jdbcTemplate;
  private ObjectMapper jsonDeserializer;

  @Autowired
  public void setDataSource(@Qualifier("mtsDataSource") DataSource datasource) {
    this.jdbcTemplate = new JdbcTemplate(datasource);
    initialiseJSONDeserializer();
  }

  @Bean
  public ReferenceDataMaps getReferenceData() {
    ReferenceDataMaps maps = new ReferenceDataMaps();

    jdbcTemplate.query(SELECT_REF_DATA,
        new Object[] {"PRODUCT_DESCRIPTION"}, (rs, RowNum) -> new LiveReferenceData(rs.getString(JSON_DATA_COLUMN),
            rs.getString(DATA_TYPE_COLUMN), rs.getString(DATA_KEY_COLUMN), rs.getString(SORT_BY_COLUMN))
    ).forEach(liveRef -> maps.getProductMap().put(liveRef.getDataKey(), deserializeTo(liveRef.getJsonData(), Product.class)));


    jdbcTemplate.query(SELECT_REF_DATA,
        new Object[] {"DIGITAL_PRODUCT_CAT"}, (rs, RowNum) -> new LiveReferenceData(rs.getString(JSON_DATA_COLUMN),
            rs.getString(DATA_TYPE_COLUMN), rs.getString(DATA_KEY_COLUMN), rs.getString(SORT_BY_COLUMN))
    ).forEach(categoryRef -> maps.getCategoryMap().put(categoryRef.getDataKey(), deserializeTo(categoryRef.getJsonData(), DigitalProductCategory.class)));

    return maps;
  }

  private void initialiseJSONDeserializer() {
    jsonDeserializer = new ObjectMapper();
    jsonDeserializer.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
    jsonDeserializer.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
  }

  private <T extends DomainObject> T deserializeTo(String json, Class targetType) {
    T result;

    try {
      result = (T) jsonDeserializer.readValue(json, targetType);

    } catch (IOException | SecurityException
        | IllegalArgumentException e) {
      throw new RuntimeException(e);
    }

    return result;
  }
}