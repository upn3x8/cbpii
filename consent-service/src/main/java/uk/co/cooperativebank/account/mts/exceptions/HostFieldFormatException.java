package uk.co.cooperativebank.account.mts.exceptions;

/**
 * This exception is thrown when formatting errors occurs
 *
 * @author Craig Parkinson
 * @see AlertingException
 */
public class HostFieldFormatException extends RuntimeException {

    private String alertId;

    private String errorText;


    /**
     * Constructor for the class
     * @param errorText the error code for the exception
     */
    public HostFieldFormatException(String errorText) {
        this.alertId = "HOST_FIELD";
        this.errorText = errorText;
    }

    /**
     * Constructor for the class
     * @param  alertId the alert id
     * @param errorText the error code for the exception
     *
     */
    public HostFieldFormatException(String alertId, String errorText) {
        this.alertId = alertId;
        this.errorText = errorText;
    }


    public String getAlertID() {
        return alertId;
    }


    public String getReasonCode() {
        return errorText;
    }

}