package uk.co.cooperativebank.account.mts.dao.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import uk.co.cooperativebank.account.mts.dao.product.Product;

import java.math.BigDecimal;
import java.util.Date;

/**
 * An {@link Account} that has been opened based on a loan agreement
 * {@link Product} that
 * is offered by the Cooperative bank to its
 *
 */
@JsonTypeName("LoanAccount")
public class LoanAccount extends DomesticAccount {

    private Date endDate;
    /**
     * The total sum of money originally borrowed by the customer as part of
     * the loan.
     */
    private BigDecimal originalLoanAmount;

    private BigDecimal availableBalance;

    public final BigDecimal getOriginalLoanAmount() {
        return originalLoanAmount;
    }

    public final void setOriginalLoanAmount(BigDecimal originalLoanAmount) {
        this.originalLoanAmount = originalLoanAmount;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    @Override
    protected void initialiseDomesticAccountSpecificDerivedFields() {
        this.availableBalance = calculateAvailableBalance();
    }

    public static class ID extends DomesticAccount.ID {

        public ID() {
            super();
        }

        public ID(String identifier) {
            super(identifier);
        }

        @JsonCreator
        public ID(@JsonProperty("sortCode") final String sortCode,
                  @JsonProperty("accountNumber") final String accountNumber,
                  @JsonProperty("accountType") final String accountType) {
            super(sortCode, accountNumber, accountType);
        }
    }
}
