package uk.co.cooperativebank.account.mts.exceptions;


/**
 * Represents any mainframe external application error code treated as a fatal
 * system error by the mid-tier.
 *
 * @author Digital-Dev
 */
public class ResolvableMainframeFatalExternalSystemError extends ResolvableMainframeExternalSystemError implements FatalFailure {

    public ResolvableMainframeFatalExternalSystemError(String errorCode) {
        super(errorCode);
    }
}
