package uk.co.cooperativebank.account.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.jms.DefaultJmsHeaderMapper;
import org.springframework.messaging.MessageHeaders;

@Slf4j
public class CustomJmsHeaderMapper extends DefaultJmsHeaderMapper {
  @Override
  public void fromHeaders(MessageHeaders headers, javax.jms.Message jmsMessage) {
    super.fromHeaders(headers, jmsMessage);
    try {
      jmsMessage.setJMSType("TextMessage");
    } catch (Exception e) {
      log.error("Error setting message type", e);
    }
  }
}
