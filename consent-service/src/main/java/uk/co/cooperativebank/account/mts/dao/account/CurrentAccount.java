package uk.co.cooperativebank.account.mts.dao.account;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.math.BigDecimal;

import static uk.co.cooperativebank.account.mts.templates.utilities.SensitivePropertyUtil.reflectObjectToStringExcludingSensitiveProperties;


/**
 * An {@link Account} that has been opened based on a 'current account' that
 * is offered by the Cooperative bank to its customer
 */
@JsonTypeName("CurrentAccount")
public class CurrentAccount extends DomesticAccount {


    public final BigDecimal getAvailableBalance() {
        return calculateAvailableBalance();
    }

    public final void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public static class ID extends DomesticAccount.ID {

        public ID() {
            super();
        }

        public ID(String identifier) {
            super(identifier);
        }

        @JsonCreator
        public ID(@JsonProperty("sortCode") final String sortCode,
                  @JsonProperty("accountNumber") final String accountNumber,
                  @JsonProperty("accountType") final String accountType) {
            super(sortCode, accountNumber, accountType);
        }
    }

    @Override
    protected void initialiseDomesticAccountSpecificDerivedFields() {
        initialiseAvailableBalance();
    }

    private void initialiseAvailableBalance() {
        availableBalance = calculateAvailableBalance();
    }

    @Override
    public String toString() {
        return reflectObjectToStringExcludingSensitiveProperties(this);
    }
}
