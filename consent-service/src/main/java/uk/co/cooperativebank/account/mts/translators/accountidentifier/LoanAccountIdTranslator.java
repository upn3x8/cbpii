package uk.co.cooperativebank.account.mts.translators.accountidentifier;

import uk.co.cooperativebank.account.mts.dao.account.LoanAccount;
import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;
import uk.co.cooperativebank.account.mts.translators.AbstractHostTranslator;

/**
 *
 * @author Digital-Dev
 */
public class LoanAccountIdTranslator extends AbstractHostTranslator {

    /**
     *
     */
    public LoanAccountIdTranslator() {
        super();

    }

    /**
     * Encodes the key
     * @param obj the data object
     * @param inlength the length of the field.
     * @param outlength the output length
     * @return a string for Percent
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public String encode(Object obj, int inlength, int outlength) throws HostFieldFormatException {
        if (obj == null) {
            return Utilities.getHostNull(outlength);
        }
        assertObjectType(obj, LoanAccount.ID.class);

        LoanAccount.ID val = (LoanAccount.ID) obj;

        return val.getFullIdentifierValue();

    }

    /**
     * Decodes the key
     *
     * @param str - the string to create the data object
     * @return Object - the decoded object
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public Object decode(String str) throws HostFieldFormatException {

        return new LoanAccount.ID(str);

    }

}
