package uk.co.cooperativebank.account.mts.exceptions;

public class ResolvedMainframeExternalApplicationError extends MainframeExternalApplicationError {

    public ResolvedMainframeExternalApplicationError(String errorToken) {
        super(errorToken);
    }
}