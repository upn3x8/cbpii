package uk.co.cooperativebank.account.mts.exceptions;

public class ExternalError extends RuntimeException {

    public ExternalError(String message) {
        super(message);
    }
}