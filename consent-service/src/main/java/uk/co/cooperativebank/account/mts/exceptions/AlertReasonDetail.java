package uk.co.cooperativebank.account.mts.exceptions;

import java.io.Serializable;
import java.util.Objects;

public class AlertReasonDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String code;
    private final String description;

    public AlertReasonDetail(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public AlertReasonDetail(AlertReasonCode alertReasonCode) {
        this.code = alertReasonCode.getCode();
        this.description = alertReasonCode.getDescription();
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public String toString() {
        return code + ", description:" + description;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AlertReasonDetail)) {
            return false;
        }
        AlertReasonDetail other = (AlertReasonDetail) obj;
        return this.code == null ? other.code == null : this.code.equals(other.code)
                && this.description == null ? other.description == null : this.description.equals(other.description);

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.code);
        hash = 17 * hash + Objects.hashCode(this.description);
        return hash;
    }

}
