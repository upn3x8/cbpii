package uk.co.cooperativebank.account.mts.framework;

import java.io.Serializable;

/**
 * A marker interface used to identify domain objects.
 *
 * All domain objects should define a serialization UID of:
 * <code>
 * private static final long serialVersionUID = 1L;
 * </code>
 */
public interface DomainObject extends Serializable {

}
