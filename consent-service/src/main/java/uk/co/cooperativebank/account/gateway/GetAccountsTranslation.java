package uk.co.cooperativebank.account.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.dtos.ReferenceDataMaps;
import uk.co.cooperativebank.account.mts.dao.product.DigitalAccountType;
import uk.co.cooperativebank.account.mts.dao.product.DigitalProductCategory;
import uk.co.cooperativebank.account.mts.templates.MessageField;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslationConfig;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslationImpl;
import uk.co.cooperativebank.account.mts.translators.HostTranslatorFactory;

import java.util.Map;

/**
 * @author Digital-Dev
 * <p>
 * This class provides specific implementation to provide subtype of Account for
 * given product code.
 */
public class GetAccountsTranslation extends EnterpriseTranslationImpl {

  private static final Logger LOG = LoggerFactory.getLogger(GetAccountsTranslation.class);

  private final ReferenceDataMaps maps;

  public GetAccountsTranslation(ReferenceDataMaps maps, HostTranslatorFactory hostTranslatorFactory, EnterpriseTranslationConfig enterpriseTranslationConfig) {
    super(hostTranslatorFactory, enterpriseTranslationConfig);
    this.maps = maps;
  }

  @Override
  protected String decodeRequestSpecificCode(MessageField switchField, String switchValue, Map<String, Object> headersMap) {
    LOG.info("in GetAccountsTranslation decodeRequestSpecificCode for switchValue:" + switchValue);

    String optionValue = switchValue;
    if ("OptionCode".equals(switchField.getType()) && "prodCode".equals(switchField.getName())) {

      String cat = maps.getProductMap().get(switchValue).getProductCat();
      for (DigitalProductCategory prod : maps.getCategoryMap().values()) {
        if (prod.getKey().equals(cat)) {
          optionValue = DigitalAccountType.getDigitalAccountType(prod.getValue()).name();
          break;
        }
      }
    }
    return optionValue;
  }

}