package uk.co.cooperativebank.account.mts.framework;

/**
 * Marker interface to distinguish objects used as unique identifiers.
 */
public interface Identifier extends DomainObject {

}