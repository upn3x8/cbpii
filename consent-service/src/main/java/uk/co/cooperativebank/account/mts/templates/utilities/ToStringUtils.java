package uk.co.cooperativebank.account.mts.templates.utilities;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.LoggerFactory;

/**
 * Utility methods for implementing <code>toString()</code> methods.
 *
 * @author dmartin
 */
public class ToStringUtils {

    private ToStringUtils() {
    }

    private static final String AT = "@";

    /**
     * Helper method for use in <code>toString()</code> implementations.
     *
     * A standard <code>toString()</code> implementation should look like this:
     * <code>
     * &at;Override
     * public String toString() {
     *   return ToStringUtils.toString(this);
     * }
     * </code>
     *
     * @param inst Instance of any class
     * @return If debug logging is enabled on the <code>org.​slf4j.​Logger</code>
     * whose name matches the fully qualified class name of <code>inst</code>,
     * then return a full breakdown of the object's properties and their current
     * values. Otherwise return the object's class name and <code>hashCode</code>
     */
    public static String toString(Object inst) {
        if (LoggerFactory.getLogger(inst.getClass()).isDebugEnabled()) {
            return ToStringBuilder.reflectionToString(inst);
        }

        return inst.getClass().getName() + AT
                + Integer.toHexString(inst.hashCode());
    }
}