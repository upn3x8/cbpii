package uk.co.cooperativebank.account.mts.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.dao.headers.BasicResponseHeader;
import uk.co.cooperativebank.account.mts.dao.headers.BusinessServiceEnquiryResponseHeader;
import uk.co.cooperativebank.account.mts.dao.headers.TechnicalServiceEnquiryResponse;
import uk.co.cooperativebank.account.mts.exceptions.MainframeExternalSystemError;
import uk.co.cooperativebank.account.mts.exceptions.ResolvableMainframeExternalApplicationError;
import uk.co.cooperativebank.account.mts.exceptions.ResolvedMainframeExternalApplicationError;
import uk.co.cooperativebank.account.mts.framework.DomainObject;
import uk.co.cooperativebank.account.mts.templates.AbstractHostTemplate;
import uk.co.cooperativebank.account.mts.templates.EnterpriseServiceTemplates;
import uk.co.cooperativebank.account.mts.templates.MessageTemplate;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslationConfig;

import java.util.List;

@Component
public class EnterpriseServiceManager {

    private static final Logger LOG = LoggerFactory.getLogger(EnterpriseServiceManager.class);


    private static final int MAINFRAME_SYSTEM_ERROR_START = 5000;
    private static final String ZERO_RECORDS_RETURNED = "0000";
    private static final int MAINFRAME_SYSTEM_ERROR = 1;
    private static final String DEFAULT_EXCEPTION_FOR_HOST_REQUEST = "default";
    private static final String BANK_PACKAGE_ROOT = "uk.co.cooperativebank";
    private static final int MAX_RESTART_ITERATIONS = 50;


    @Autowired
    private EnterpriseTranslationConfig brandEnterpriseTranslationConfig;

    public <T extends DomainObject>List<T> call(EnterpriseServiceCall call, final ClientContext clientContext, final String customerNumber) {
        LOG.debug("Entering into call() from Common-Integration Epic");
        List responseObjects = null;
        EnterpriseGateway.EnterprisePayload payload = new EnterpriseGateway.EnterprisePayload(
                clientContext, customerNumber);

        LOG.debug("Payload : " + payload.toString());
        payload.setEnterpriseTranslationConfig(brandEnterpriseTranslationConfig);


        int restartIterationCount = 0;

        while (true) {
            List<? extends DomainObject> objects = call.service(payload);
            BasicResponseHeader responseHeader;
            // Following a restart, response objecst will already be populated
            // with previous response, so add the new objects to the existing
            // list
            if (responseObjects != null) {
                // Read the header but don't re-add it to list
                responseHeader = (BasicResponseHeader) objects.remove(0);
                responseObjects.addAll(objects);
            } else {
                responseObjects = objects;
                responseHeader = (BasicResponseHeader) objects.get(0);
            }

            if (!isRestartRequired(responseHeader, payload, restartIterationCount)) {
                break;
            } else {
                restartIterationCount = restartIterationCount + 1;
            }
        }
        // Test for errors
        BasicResponseHeader responseHeader = (BasicResponseHeader) responseObjects.remove(0);
        if (!AbstractHostTemplate.SUCCESS_RESPONSE_NUMBER.equals(responseHeader.getErrorCode())) {

            int errorCode = Integer.parseInt(responseHeader.getErrorCode());
            if (errorCode < MAINFRAME_SYSTEM_ERROR_START && errorCode != MAINFRAME_SYSTEM_ERROR) {
                throwConfiguredMessageTemplateException(responseHeader);
            } else {
                throw new MainframeExternalSystemError();
            }
        }
        LOG.debug("Exiting from call() from Common-Integration Epic");
        return responseObjects;
    }

    private boolean isRestartRequired(BasicResponseHeader responseHeader, EnterpriseGateway.EnterprisePayload payload,
                                      int restartIterationCount) {
        String restartKey = getRestartKey(responseHeader);
        if (restartKey == null) {
            return false;
        } // If a restart key has been specified but there are no records
        // returned, something has gone wrong in mainframe.
        // In this case we need to raise an exception otherwise MTS may loop
        // - continually resending same request with restart key and getting
        // restart response
        // Added as a fix to prevent the posisbility of lockup as seen in
        // defect 8341
        else if (ZERO_RECORDS_RETURNED.equals(getNumberReturned(responseHeader))) {
            throw new RuntimeException("Mainframe sent invalid response - restart key with zero records not allowed");
        } else {
            // If restart is required but the number of iterations exceeds the
            // maximum specified, throw an exception
            // Again, this it to prevent possiblility of an infinite loop as was
            // seen in defect 8341
            if (restartIterationCount > MAX_RESTART_ITERATIONS) {
                throw new RuntimeException("Mainframe sent too may restart responses");
            }
            payload.getClientContext().setRestartKey(restartKey);
            return true;
        }
    }

    private void throwConfiguredMessageTemplateException(BasicResponseHeader responseHeader) {

        String defaultErrorClass = null;

        MessageTemplate messageTemplate = EnterpriseServiceTemplates
                .getOutboundMessageTemplate(responseHeader.getRequestNumber());
        if (messageTemplate != null && messageTemplate.getErrors() != null) {
            for (String errorCode : messageTemplate.getErrors().keySet()) {

                if (errorCode.equals(responseHeader.getErrorCode())) {
                    String errorInformation = messageTemplate.getErrors().get(errorCode);
                    if (errorInformation != null && !errorInformation.isEmpty()) {
                        if (!errorInformation.startsWith(BANK_PACKAGE_ROOT)) {
                            throw new ResolvedMainframeExternalApplicationError(errorInformation);
                        }

                        throwExceptionForExceptionClassName(errorInformation, responseHeader);
                    }
                } // If a default error handler has been specified - store it.
                else if (DEFAULT_EXCEPTION_FOR_HOST_REQUEST.equals(errorCode)) {
                    defaultErrorClass = messageTemplate.getErrors().get(errorCode);
                }
            }
        }
        // If the error code does not have an explicit Exception class, use the
        // defautl if it exists
        if ((defaultErrorClass != null) && !defaultErrorClass.isEmpty()) {
            throwExceptionForExceptionClassName(defaultErrorClass, responseHeader);
        }

        throw new ResolvableMainframeExternalApplicationError(responseHeader.getErrorCode());
    }

    private void throwExceptionForExceptionClassName(String exceptionClassName, BasicResponseHeader responseHeader) {
        try {
            Class exceptionClass = Class.forName(exceptionClassName);
            Exception exception = (Exception) exceptionClass.getDeclaredConstructor(String.class).newInstance(responseHeader.getErrorCode());
            throw exception;
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            throw (RuntimeException) ex;
        }
    }

    private static String getRestartKey(BasicResponseHeader responseHeader) {
        if (responseHeader instanceof TechnicalServiceEnquiryResponse) {
            return ((TechnicalServiceEnquiryResponse) responseHeader).getRestartKey();
        } else if (responseHeader instanceof BusinessServiceEnquiryResponseHeader) {
            return ((BusinessServiceEnquiryResponseHeader) responseHeader).getRestartKey();
        }
        return null;
    }

    private static String getNumberReturned(BasicResponseHeader responseHeader) {
        if (responseHeader instanceof TechnicalServiceEnquiryResponse) {
            return ((TechnicalServiceEnquiryResponse) responseHeader).getNumberReturned();
        } else if (responseHeader instanceof BusinessServiceEnquiryResponseHeader) {
            return ((BusinessServiceEnquiryResponseHeader) responseHeader).getNumberReturned();
        }
        return null;
    }
}
