package uk.co.cooperativebank.account.mts.exceptions;

public enum AlertReasonCode {

    //Persistence alerts
    DEFAULT("Default", "please refer to the exception documentation"),
    DEFAULT_SQL_EXCEPTION("DefaultSQLException", "SQLException raised with message: \"%1s\""),
    CONNECTION_FAILURE("ConnectionFailure", "Failed to make Database Connection"),
    DUPLICATE_KEY("DuplicateKey", "Primary Key already exists"),
    SQL_EXCEPTION_WITH_VENDOR_CODE("SQLProblemWithVendorCode", "SQL exception raised with vendor code: \"%1d\" and SQL state: \"%2s\", and message: \"%3s\""),
    SQL_EXCEPTION_WITH_SQL_STATE("SQLProblemWithSQLState", "SQL exception raised with SQL state: \"%1s\", and message: \"%2s\""),
    //configuration alerts
    FTRESS_CONFIGURATION_ERROR("FortressConfigurationError", "Exception with FTress service"),
    INVALID_FORMAT("InvalidFormat", "Wrong formatted field received"),
    LOA_UNDEFINED("LoaUndefined", "LOA undefined for function"),
    RESOURCE_NOT_FOUND("ResourceNotFound", "Unable to find required resource"),
    TWO_FA_REF_DATA_MISSING("TwoFaRefDataMissing", "Missing 2fa ref dao properties file"),
    //Security alerts
    CUSTOMERID_ACCOUNTID_MISMATCH("CustomerIdAccountIdMismatch", "Requested and Mainframe returned customer ID are not matching"),
    AUTHENTICATION_TOKEN_MISSING("AuthTokenMissng", "Authentication Token is missing in the request"),
    BAD_AUTHENTICATION_TOKEN("BadAuthToken", "Authentication Token is not valid"),
    LOA_INSUFFICIENT("LoaInsufficient", "Current LOA is not sufficint to process the request"),
    LOA_NOT_FOUND("LoaNotFound", "No LOA available"),
    AUTHENTICATION_TOKEN_CHANGED_BUT_NOT_RETURNED_IN_RESPONSE("AuthenticationTokenChangedButNotReturnedInResponse", "Authentication token changed (as service raised LOA above 10) but not returned in response"),
    INVALID_SECURITY_CREDENTIALS("InvalidSecurityCredentials", "Security credentials submitted do not match those cached for this session"),
    MISSING_SECURITY_CREDENTIAL("MissingSecurityCredential", "Security credential missing in client request"),
    //INVALID_REQUEST("InvalidRequest","The request is not available"),
    INVALID_BRAND("InvalidBrand", "The Brand is not supported"),
    INVALID_CHANNEL("InvalidChannel", "The Channel is not supported"),
    //Encripton alerts
    ENCRYPTION_VALIDATION_FAILED("EncryptionValidationFailed", "Failed validation to enctipt"),
    DESCRIPTION_VALIDATION_FAILED("DecryptionValidationFailed", "Failed validation to dectipt"),
    //EnterpriseTranslator Alert codes
    ENCODING_FAILED("EncodingFailed", "Message translation failed while encoding"),
    UNIDENTIFIED_SERVICE("UnIdentifiedService", "UnIdentifiedService received while encodeing"),
    DECODING_FAILED("DecodingFailed", "Message translation failed while decoding"),
    TRANSLATOR_NOT_FOUND("TranslatorNotFound", "Translator for the object type not found"),
    ENTERPRISE_TRANSALATOR_INIT_FAILED("EnterpriseTransalatorInitFailed", "Failed to initialise Enterprise Message translator"),
    //JMS alerts
    QUEUE_MANAGER_NOT_AVAILABLE("QUEUE_MANAGER_NOT_AVAILABLE", "Failed to connect to queue manager"),
    MISSING_JSESSION_ID("MISSING_JSESSION_ID", "missing JSesssionID in cleint context"),
    //External System alerts
    FORTRESS_V3_ERROR("FortressV3Error", "Exception  occurred during Ftress v3 service call (2FA services)"),
    FORTRESS_V7_ERROR("FortressV7Error", "Exception  occurred during FTress v7 service call (Active ID)"),
    GENERAL_MAINFRAME_ERROR("MainFrameError", "Exception occurred during KBA (Experian) service call"),
    FATAL_MAINFRAME_ERROR("FatalMainFrameError", "Fatal Exception occurred during KBA (Experian) service call"),
    KBA_ERROR("KBAError", "Exception occurred during KBA (Experian) service call"),
    EXTERNAL_SYSTEM_ERROR_DEFAULT("ExternalSystemError", "ExternalSystemError"),
    EXTERNAL_APPLICATION_SYSTENM_DEFAULT("ExternalApplicationError", "ExternalApplicationError"),

    INVALID_SECURE_TRANSACTION_DETAILS("InvalidSecureTransactionDetails", "The secure transaction details within the client context are either null or empty"),
    INVALID_CLIENT_TRANSACTION_ID("InvalidClientTransactionId", "The client transaction Id provided \"{0}\" does not match with the required secure transaction in the database"),
    INVALID_2FA_AUTHENTICATION_STATUS("Invalid2FAAuthenticationStatus", "The current 2FA authenciation status \"{0}\" is not sufficient to complete this transaction"),
    INVALID_LOA_FOR_2FA_AUTHENTICATION("InvalidLOAFor2FAAuthentication", "The current LOA \"{0}\" is not sufficient to complete this transaction"),
    INVALID_TRANSACTION_PROCESS_STATUS("InvalidTransactionProcessStatus", "The current transaction process status \"{0}\" is not valid to complete this transaction"),
    INVALID_SECURE_TXN_INPUT_DATA("InvalidSecureTxnInputData", "The input dao provided in the Submit request \"{0}\" does not match with that of database Initial request \"{1}\""),

    //MoveMoney
    INVALID_DIGITAL_BENEFICIARY_ID("InvalidDigitalBeneficiaryId", "The specified digitalBeneficiaryId does not exist"),

    //Criminal Detection
    UNEXPECTED_CD_MESSAGE("UnexpectedCriminalDetectionMessage", "A message was received on the crimimal detection queue when the service was disabled"),

    //ALSI Expired
    ALSI_EXPIRED("ALSIExpiredException", "ALSIExpired exception Please Login again"),

    //OLB resetPasswordAndPin
    PASSWORD_OR_PIN_MANDATORY("PasswordOrPinMandatory", "Either Password or Pin Should present in request"),

    //EBS calls
    ACCOUNT_NUMBER_NOT_FOUND("AccountNumberNotFound", "19 digit account number not found in cache");

    private final String code;
    private final String description;

    AlertReasonCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public String toString() {
        return code + ", description:" + description;
    }

}
