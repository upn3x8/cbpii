package uk.co.cooperativebank.account.mts.gateway;

import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslationConfig;

public interface EnterpriseGateway {
    class EnterprisePayload {
        private final ClientContext clientContext;
        private final String customerNumber;

        private EnterpriseServiceRequest request;
        private EnterpriseService service;

        private EnterpriseTranslationConfig enterpriseTranslationConfig;

        public EnterprisePayload(final ClientContext clientContext, String customerNumber) {
            this.clientContext = clientContext;
            this.customerNumber = customerNumber;
        }
        public final EnterpriseServiceRequest getRequest() {
            return request;
        }

        public final EnterpriseService getService() {
            return service;
        }

        public ClientContext getClientContext() {
            return clientContext;
        }

        public String getCustomerNumber() {
            return customerNumber;
        }

        public final void setService(EnterpriseService service) {
            this.service = service;
        }

        public void setRequest(EnterpriseServiceRequest request) {
            this.request = request;
        }

        public EnterpriseTranslationConfig getEnterpriseTranslationConfig() {
            return enterpriseTranslationConfig;
        }

        public void setEnterpriseTranslationConfig(EnterpriseTranslationConfig enterpriseTranslationConfig) {
            this.enterpriseTranslationConfig = enterpriseTranslationConfig;
        }

    }
}