package uk.co.cooperativebank.account.mts.encryption;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.exceptions.AlertReasonDetail;
import uk.co.cooperativebank.account.mts.exceptions.EncryptionAlert;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.DESCRIPTION_VALIDATION_FAILED;
import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.ENCRYPTION_VALIDATION_FAILED;


/**
 * StringEncryptionException is used to provide a simple encryption and
 * decryption routines. THis is not very clever and is lifted directly from the
 * SHOX enrolment code.
 *
 * @author Craig Parkinson
 * @version $Revision$
 */
public class StringEncryptionService extends AbstractEncryptionService {

    /**
     * ENCRYPTION KEY - HARDCODED DUE TO AS IS POLICY - LIFTED FROM SHOX AS IS
     */
    private static final String KEY = "Unix for Dummies";

    /**
     * THE SINGLETON INSTANCE
     */
    private static EncryptionService mInstance = null;

    /**
     * LOGGING
     */
    private static final Logger LOG = LoggerFactory.getLogger(StringEncryptionService.class);

    private static final int MIN_ASCCI_VAL = 32;
    private static final int MAX_ASCCI_VAL = 127;

    /**
     * constructor
     */
    private StringEncryptionService() {
    }

    /**
     * validates an encryption routine - simply check that the output is twice
     * as long as the input
     *
     * @param source the input string
     * @param output the encrypted string
     * @return true if successfully validated
     * @throws EncryptionAlert an encryption exception
     */
    @Override
    protected boolean validateEncrypt(String source, String output) throws EncryptionAlert {

        if (source == null || output == null) {
            LOG.error("source or ouput string is null");
        }

        // check that the output is twice the length of the input
        if ((source.length() * 2) != (output.length())) {
            LOG.error("ouput string length is not twice the input stringlength");
            throw new EncryptionAlert(new AlertReasonDetail(ENCRYPTION_VALIDATION_FAILED.getCode(), ENCRYPTION_VALIDATION_FAILED.getDescription()));
        }

        return true;
    }

    /**
     * validates a decryption routine
     *
     * @param source the input string
     * @param output the encrypted string
     * @return true if successfully validated
     * @throws EncryptionAlert an encryption exception
     */
    @Override
    protected boolean validateDecrypt(String source, String output) throws EncryptionAlert {

        if (source == null || output == null) {
            LOG.error("source or ouput string is null");
            throw new EncryptionAlert(new AlertReasonDetail(DESCRIPTION_VALIDATION_FAILED.getCode(), DESCRIPTION_VALIDATION_FAILED.getDescription()));
        }

        // check that the output is twice the length of the input
        if ((source.length() / 2) != (output.length())) {
            LOG.error("ouput string length is not twice the input stringlength");
            throw new EncryptionAlert(new AlertReasonDetail(DESCRIPTION_VALIDATION_FAILED.getCode(), DESCRIPTION_VALIDATION_FAILED.getDescription()));
        }
        return true;
    }

    /**
     * encrypt a string routine valid chars are only ASCII in range
     * MIN_ASCCI_VAL to MAX_ASCCI_VAL
     *
     * @param sourceStr the input string
     * @throws EncryptionAlert an encryption exception
     * @return the encrypted string
     */
    @Override
    public String encrypt(String sourceStr) throws EncryptionAlert {

        String leftStr = "";
        String leftKey;

        if (sourceStr == null) {
            LOG.error("source string is null");
            throw new EncryptionAlert(new AlertReasonDetail(ENCRYPTION_VALIDATION_FAILED.getCode(), ENCRYPTION_VALIDATION_FAILED.getDescription()));
        }

        // for mainframe reasons we need to ensure that all chars are in upper case
        // trim off spaces as we don't want to encrypt the spaces
        String tempStr = sourceStr.toUpperCase().trim();

        leftKey = KEY;
        while (leftKey.length() < tempStr.length()) {
            leftKey = leftKey + KEY;
        }

        for (int c = 0; c < tempStr.length(); c++) {

            // check char is in ascii RANGE MIN_ASCCI_VAL TO MAX_ASCCI_VAL
            if (((int) tempStr.charAt(c) < MIN_ASCCI_VAL) || ((int) tempStr.charAt(c) > MAX_ASCCI_VAL)) {
                LOG.error("Char '"
                        + tempStr.charAt(c)
                        + "' is outside chracter encryption range of "
                        + MIN_ASCCI_VAL
                        + " to "
                        + MAX_ASCCI_VAL);
                throw new EncryptionAlert(new AlertReasonDetail(ENCRYPTION_VALIDATION_FAILED.getCode(), ENCRYPTION_VALIDATION_FAILED.getDescription()));
            }

            leftStr = leftStr + (char) ((((int) tempStr.charAt(c) ^ (int) leftKey.charAt(c)) / 16) + 97);
            leftStr = leftStr + (char) ((((int) tempStr.charAt(c) ^ (int) leftKey.charAt(c)) % 16) + 97);
        }

        // validate routine
        validateEncrypt(tempStr, leftStr);

        return leftStr;
    }

    /**
     * decrypt a string
     *
     * @param sourceStr the input string
     * @return the decrypted string
     * @throws EncryptionAlert an encryption exception
     */
    @Override
    public String decrypt(String sourceStr) throws EncryptionAlert {

        String leftStr = "";
        String leftKey;
        int leftChar;

        if (sourceStr == null) {

            LOG.error("source string is null");
            throw new EncryptionAlert(new AlertReasonDetail(DESCRIPTION_VALIDATION_FAILED.getCode(), DESCRIPTION_VALIDATION_FAILED.getDescription()));
        }

        leftKey = KEY;
        while (leftKey.length() < sourceStr.length()) {
            leftKey = leftKey + KEY;
        }

        for (int c = 0; c < sourceStr.length() / 2; c++) {
            leftChar
                    = (((int) sourceStr.charAt((c + 1) * 2 - 2) - 97) * 16)
                    + ((int) sourceStr.charAt((c + 1) * 2 - 1) - 97) ^ ((int) leftKey.charAt(c));
            if ((leftChar >= 32) && (leftChar <= 127)) {
                leftStr = leftStr + (char) leftChar;
            }
        }

        // validate routine
        validateDecrypt(sourceStr, leftStr);

        return leftStr;
    }

    /**
     * Returns an instance of this class, synchronised for thread safe access
     *
     * @return ComponentLocation
     */
    public static synchronized EncryptionService getInstance() {
        LOG.debug("Getting instance of the encryption service singelton");
        if (mInstance == null) {
            mInstance = new StringEncryptionService();
        }
        return mInstance;
    }
}
