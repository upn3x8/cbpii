package uk.co.cooperativebank.account.mts.exceptions;

public class MainframeExternalSystemError extends ExternalSystemError {

    private static final String DEFAULT_SYSTEM_ERROR_MESSAGE = "External system error reported by mainframe";

    public MainframeExternalSystemError() {
        super(DEFAULT_SYSTEM_ERROR_MESSAGE);
    }

    public MainframeExternalSystemError(String errorMessage) {
        super(errorMessage);
    }
}