package uk.co.cooperativebank.account.mts.translators;

public interface EnterpriseTranslationConfig {

    String getUserId();

    String getBrand();

    String getDigitalCustomerIdPrefix();

}
