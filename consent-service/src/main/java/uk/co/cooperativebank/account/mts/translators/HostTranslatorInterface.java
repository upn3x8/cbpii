package uk.co.cooperativebank.account.mts.translators;


import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.MessageTemplate;
import uk.co.cooperativebank.account.mts.templates.MessageTemplateHelper;

/**
 * This interface must be implemented by any custom objects that require to be translated by the
 * HostTransalationTemplate. This implementation of AbstractHostTemplate calls the relevant encode/decode funations
 * for objects. Encode is called on inbound host requests and decode is called on outbound host requests
 *
 * @see MessageTemplateHelper
 * @see MessageTemplate
 * @author Craig Parkinson
 */
public interface HostTranslatorInterface {

    /**
     * Encodes the key
     * @param obj the object to encode
     * @param inFieldLength the length of the field to encode
     * @param outFieldLength the output length
     * @return a string a flattened string
     * @throws HostFieldFormatException a field format exception
     */
    String encode(Object obj, int inFieldLength, int outFieldLength) throws HostFieldFormatException;

    /**
     * Encodes the key
     * @param str - the string to create the dao object
     * @return - an object
     * @throws HostFieldFormatException a field format exception
     */
    Object decode(String str) throws HostFieldFormatException;
}