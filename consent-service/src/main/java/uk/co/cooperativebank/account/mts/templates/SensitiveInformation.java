package uk.co.cooperativebank.account.mts.templates;

import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Identifies a class property as containing sensitive information that should not be written to logs e.g. password, pin, credit card number
 */
@Documented
@Target({ElementType.METHOD,
        ElementType.FIELD,
        ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR,
        ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface SensitiveInformation {

    String message() default "{invalid.format}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.METHOD,
            ElementType.FIELD,
            ElementType.ANNOTATION_TYPE,
            ElementType.CONSTRUCTOR,
            ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {

        SensitiveInformation[] value();
    }
}
