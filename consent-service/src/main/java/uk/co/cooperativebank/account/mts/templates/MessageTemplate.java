package uk.co.cooperativebank.account.mts.templates;

import java.util.List;
import java.util.Map;

public class MessageTemplate {

    private String requestNumber = null;
    private String argumentsIndicator = null;
    private String service = null;
    private String type = null;
    private int length = 0;
    private String direction = null;
    private String messageType = null;
    private String messageClass = null;
    private String translatorClass = null;

    private Map<String, String> options = null;
    private List <MessageField> messageFields = null;
    private TemplateVersion versionNumber = null;

    private Map <String, String> errors = null;

    /**
     * Gets the message Request Number
     *
     * @return Returns a String
     */
    public String getRequestNumber() {
        return requestNumber;
    }


    /**
     * Sets the message Request Number
     *
     * @param requestNumber The requestNumber to set
     */
    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }


    /**
     * Gets the message Arguments Indicator
     *
     * @return Returns a String
     */
    public String getArgumentsIndicator() {
        return argumentsIndicator;
    }


    /**
     * Sets the message Arguments Indicator
     *
     * @param argumentsIndicator The argumentsIndicator to set
     */
    public void setArgumentsIndicator(String argumentsIndicator) {
        this.argumentsIndicator = argumentsIndicator;
    }


    /**
     * Gets the message type
     *
     * @return Returns a String
     */
    public String getMessageType() {
        return messageType;
    }


    /**
     * Sets the message type
     *
     * @param messageType The message type to set
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }


    /**
     * Gets the business object class of the message
     *
     * @return Returns a String
     */
    public String getMessageClass() {
        return messageClass;
    }


    /**
     * Sets the business object class of the message
     *
     * @param messageClass The class to set
     */
    public void setMessageClass(String messageClass) {
        this.messageClass = messageClass;
    }


    /**
     * Gets the message direction
     *
     * @return Returns a String
     */
    public String getDirection() {
        return direction;
    }


    /**
     * Sets the message direction
     *
     * @param direction The direction to set
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }


    /**
     * Gets the message length
     *
     * @return Returns an int
     */
    public int getLength() {
        return length;
    }


    /**
     * Sets the message length
     *
     * @param length The length to set
     */
    public void setLength(int length) {
        this.length = length;
    }


    /**
     * Gets the message options
     *
     * @return Returns a Hashmap
     */
    public Map<String, String> getOptions() {
        return options;
    }


    /**
     * Sets the message options
     *
     * @param options The message options to set
     */
    public void setOptions(Map<String,String> options) {
        this.options = options;
    }


    /**
     * Gets the list of message fields
     *
     * @return Returns an ArrayList
     */
    public List<MessageField> getMessageFields() {
        return messageFields;
    }

    /**
     * Sets the list of messageFields
     *
     * @param messageFields The list of message fields to set
     */
    public void setMessageFields(List<MessageField> messageFields) {
        this.messageFields = messageFields;
    }

    /**
     * Gets the translator class, for delimited messages only
     *
     * @return Returns a String
     */
    public String getTranslatorClass() {
        return translatorClass;
    }


    /**
     * Sets the translator class, for delimited messages only
     *
     * @param translatorClass The translator class to set
     */
    public void setTranslatorClass(String translatorClass) {
        this.translatorClass = translatorClass;
    }

    /**
     * @return - message template version
     */
    public TemplateVersion getVersionNumber() {
        return versionNumber;
    }

    /**
     * @param version - message template version
     */
    public void setVersionNumber(TemplateVersion version) {
        versionNumber = version;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map <String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map <String, String> errors) {
        this.errors = errors;
    }

}