package uk.co.cooperativebank.account.mts.translators;

import uk.co.cooperativebank.account.mts.exceptions.EnterpriseTranslationAlert;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseGateway;

import java.util.List;
import java.util.Map;

/**
 * A template represents a defined structure for the transport of information
 * to an external system. The template must be able to take a <code>EnterpriseServiceRequest</code>
 * an convert this into a string that the target host can under stand. E.g. for a
 * host written in COBOL, it would expect a string that fits into a copybook structure.
 * The template must take any response string and decode it into proper objects. In this
 * system, the return must be a business object or collection of.
 * @author Ron Tomlinson
 * @author Kev Rudland
 *
 */
public interface EnterpriseTranslation {

    /**
     * Encodes dao from an object into a flattened string.
     * @param payload the base dao to encode.
     * @return the string of this dao.
     * @throws EnterpriseTranslationAlert an integration exception
     */
    String encode(EnterpriseGateway.EnterprisePayload payload) throws EnterpriseTranslationAlert;

    /**
     * Decodes a String back into a business object or collection of.
     * @param returnedData the dao returned from the external system to decode.
     * @param headersMap headers
     * @return the business object or collection of.
     * @throws EnterpriseTranslationAlert an integration exception
     */
    List decode(String returnedData, Map<String, Object> headersMap) throws EnterpriseTranslationAlert;

}