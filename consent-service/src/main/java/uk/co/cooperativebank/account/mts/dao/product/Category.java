package uk.co.cooperativebank.account.mts.dao.product;

import uk.co.cooperativebank.account.mts.framework.DomainObject;

/**
 * The category that a {@link Product} belongs to.
 */
public class Category implements DomainObject {

    private static final long serialVersionUID = 1L;

    /**
     * The accounts transaction permissions
     */
    private CategoryPermissions categoryPermissions;

    public CategoryPermissions getCategoryPermissions() {
        return categoryPermissions;
    }

    public void setCategoryPermissions(CategoryPermissions categoryPermissions) {
        this.categoryPermissions = categoryPermissions;
    }
}
