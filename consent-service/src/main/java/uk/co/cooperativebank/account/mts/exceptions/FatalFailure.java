package uk.co.cooperativebank.account.mts.exceptions;

/**
 * Marker interface used to indicate any exception to be treated as fatal.
 *
 * @author Digital-Dev
 */
public interface FatalFailure {

}