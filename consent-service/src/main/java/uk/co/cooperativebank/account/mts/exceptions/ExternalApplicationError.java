package uk.co.cooperativebank.account.mts.exceptions;

public abstract class ExternalApplicationError  extends ExternalError {

    private static final String EXCEPTION_MSG = "ExternalApplicationError mapped to error token: %1s";

    private String errorToken;

    public ExternalApplicationError(String errorToken) {
        super(String.format(EXCEPTION_MSG, errorToken));
        this.errorToken = errorToken;
    }

    public String getErrorToken() {
        return errorToken;
    }

    protected void setErrorToken(String errorToken) {
        this.errorToken = errorToken;
    }

    @Override
    public String toString() {
        return String.format(EXCEPTION_MSG, errorToken);
    }
}