package uk.co.cooperativebank.account.mts.translators;


import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;

import java.util.Date;


/**
 * Class is used as a translator for the class
 * java.util.Date into a format of yyyy-MM-ddHH.mm.ss.SS
 *
 * @author Ron Tomlinson
 */

public class DateTranslator extends AbstractHostTranslator {

    /**
     * Default constructor
     */
    public DateTranslator() {
    }

    /**
     * Encodes the key. If Obj is null then padded with spaces to length
     * @return a string in the yyyy-MM-ddHH.mm.ss.SS format
     * @param inlength the length of the field.
     * @param outlength the output length
     * @param obj - the object to translate
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public String encode(Object obj, int inlength, int outlength) throws HostFieldFormatException {
        if (obj == null) {
            return Utilities.padStringAfter("", outlength, ' ');
        }
        assertObjectType(obj, Date.class);
        final String result = Utilities.convertDateToHostTimeStamp((Date) obj);
        // If the length is 10, that means they don't want the time element
        if (outlength == 10) {
            return result.substring(0, 10);
        }
        return result;
    }

    /**
     * Decodes the String to a Date object
     * @param str - the string to create the dao object
     * @return a date
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public Object decode(String str) throws HostFieldFormatException { // string should be 10 chars
        if (str == null || str.length() != 21) {
            throw new HostFieldFormatException("Invalid date string");
        }
        try {
            return Utilities.parseDateTime(str);
        } catch (java.text.ParseException pe) {
            throw new HostFieldFormatException("Error parsing date:" + str);
        }
    }
}