package uk.co.cooperativebank.account.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import uk.co.cooperativebank.consent.service.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Determines which brand the sortcode belongs to.
 */
@Component
public class BrandConfiguration {

  private java.util.regex.Pattern smileSortCodeRegex;
  private String smileBrandCode;
  private String coopBrandCode;

  public BrandConfiguration(@Value("${mainframe.smile.sortcode.regex}") final String sortCode,
                            @Value("${mainframe.smile.brand.code}") final String smileBrandCode,
                            @Value("${mainframe.coop.brand.code}") final String coopBrandCode) {
    this.smileSortCodeRegex = Pattern.compile(sortCode);
    this.smileBrandCode = smileBrandCode;
    this.coopBrandCode = coopBrandCode;
  }

  public String getBrandByIdentification(String identification) {
    if (StringUtils.isBlank(identification)
        || (identification.length() != Constants.ACCOUNT_NUMBER_INDEX
            && identification.length() != Constants.INDENTIFIER_LENGTH)) {
      throw new IllegalArgumentException("Identification must not be null and the correct length");
    }

    String sortCode = identification.substring(0, Constants.SORT_CODE_INDEX);

    Matcher matcher = smileSortCodeRegex.matcher(sortCode);
    if (matcher.matches()) {
      return smileBrandCode;
    }
    return coopBrandCode;
  }

  public boolean isCoopCustomer(String brand) {
    return coopBrandCode.equals(brand);
  }

  public boolean isSmileCustomer(String brand) {
    return smileBrandCode.equals(brand);
  }


}
