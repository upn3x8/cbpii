package uk.co.cooperativebank.account.mts.dao.product;

import com.fasterxml.jackson.annotation.JsonTypeName;
import uk.co.cooperativebank.account.mts.framework.DomainObject;


@JsonTypeName("CategoryPermissions")
public class CategoryPermissions implements DomainObject {
  /**
   *
   */
  private boolean canDoFundTransfer = false;
  /**
   *
   */
  private boolean canDoBillPayment = false;
  /**
   *
   */
  private boolean canDoForeignPayment = false;
  /**
   *
   */
  private boolean canViewStandingOrders = false;
  /**
   *
   */
  private boolean canCreateStandingOrder = false;
  /**
   *
   */
  private boolean canCancelStandingOrder = false;

  /**
   *
   */
  private boolean canViewDirectDebits = false;
  /**
   *
   */
  private boolean canCancelDirectDebit = false;

  /**
   *
   */
  private boolean canViewPlannedPayments = false;

  public boolean isCanDoFundTransfer() {
    return canDoFundTransfer;
  }

  public void setCanDoFundTransfer(boolean canDoFundTransfer) {
    this.canDoFundTransfer = canDoFundTransfer;
  }

  public boolean isCanDoBillPayment() {
    return canDoBillPayment;
  }

  public void setCanDoBillPayment(boolean canDoBillPayment) {
    this.canDoBillPayment = canDoBillPayment;
  }

  public boolean isCanDoForeignPayment() {
    return canDoForeignPayment;
  }

  public void setCanDoForeignPayment(boolean canDoForeignPayment) {
    this.canDoForeignPayment = canDoForeignPayment;
  }

  public boolean isCanViewStandingOrders() {
    return canViewStandingOrders;
  }

  public void setCanViewStandingOrders(boolean canViewStandingOrders) {
    this.canViewStandingOrders = canViewStandingOrders;
  }

  public boolean isCanCreateStandingOrder() {
    return canCreateStandingOrder;
  }

  public void setCanCreateStandingOrder(boolean canCreateStandingOrder) {
    this.canCreateStandingOrder = canCreateStandingOrder;
  }

  public boolean isCanCancelStandingOrder() {
    return canCancelStandingOrder;
  }

  public void setCanCancelStandingOrder(boolean canCancelStandingOrder) {
    this.canCancelStandingOrder = canCancelStandingOrder;
  }

  public boolean isCanViewDirectDebits() {
    return canViewDirectDebits;
  }

  public void setCanViewDirectDebits(boolean canViewDirectDebits) {
    this.canViewDirectDebits = canViewDirectDebits;
  }

  public boolean isCanCancelDirectDebit() {
    return canCancelDirectDebit;
  }

  public void setCanCancelDirectDebit(boolean canCancelDirectDebit) {
    this.canCancelDirectDebit = canCancelDirectDebit;
  }

  public boolean isCanViewPlannedPayments() {
    return canViewPlannedPayments;
  }

  public void setCanViewPlannedPayments(boolean canViewPlannedPayments) {
    this.canViewPlannedPayments = canViewPlannedPayments;
  }

}
