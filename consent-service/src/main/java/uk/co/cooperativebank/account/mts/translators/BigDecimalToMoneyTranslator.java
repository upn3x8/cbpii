package uk.co.cooperativebank.account.mts.translators;

import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;

import java.math.BigDecimal;


/**
 * Class is used as a translator for the money values
 *
 * @author Craig Parkinson
 * @author Andrew Tennant
 */

public class BigDecimalToMoneyTranslator extends AbstractHostTranslator {

    /**
     * Encodes the key
     * @param obj the dao object
     * @param inlength the length of the field.
     * @param outlength the output length
     * @return a string Y for true N for false
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public String encode(Object obj, int inlength, int outlength) throws HostFieldFormatException {

        if (obj == null) {
            return Utilities.getHostNull(outlength);
        }

        assertObjectType(obj, BigDecimal.class);
        BigDecimal moneyVal = (BigDecimal) obj;

        StringBuilder sb = new StringBuilder(outlength);

        String moneyString = moneyVal.toString();

        if (moneyString.indexOf(".") > 0 && moneyString.length() > (moneyString.indexOf(".") + 1)) {
            String fraction = moneyString.substring(moneyString.indexOf(".") + 1);
            if (fraction.length() != 1 && fraction.trim().length() != 2) {
                throw new HostFieldFormatException("Invalid fractional part '" + fraction + "'");
            }
            if (fraction.length() == 1) {
                fraction = fraction + "0";
            }
            sb.append(moneyString.substring(0,moneyString.indexOf(".") + 1)).append(fraction);
        } else {
            sb.append(moneyVal).append(".00");
        }

        // pad with spaces
        return Utilities.padStringBefore(sb.toString(), outlength, ' ');

    } /**
     * Encodes the key
     * @param str - the string to create the dao object
     * @return Object - the decoded object
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public Object decode(String str) throws HostFieldFormatException { // string should be 10 chars
        boolean credit = true;
        String pounds = str.substring(0, 9).trim();
        String realPounds = pounds.substring(1);
        String pence = str.substring(10, 12);
        if (str.indexOf('+') == -1) {
            credit = false;
        }

        return new BigDecimal((credit ? "+" : "-") + realPounds + "." + pence);
    }
}
