package uk.co.cooperativebank.account.mts.translators.accountidentifier;

import org.springframework.stereotype.Component;
import uk.co.cooperativebank.account.mts.templates.TranslatorResolver;
import uk.co.cooperativebank.account.mts.translators.HostTranslatorInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides common Translators
 * @author Digital-Dev
 */
@Component
public class AccountTranslatorResolver implements TranslatorResolver {

    private static final Map <String, HostTranslatorInterface> TRANSLATORS = new HashMap<>();
    //TODO Fix issue with ID -
    static {
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.CreditCardAccount$ID", new CreditCardAccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.CurrentAccount$ID", new CurrentAccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.LoanAccount$ID", new LoanAccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.SavingsAccount$ID", new SavingsAccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.FixedTermDeposit$ID", new FixedTermDepositIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.IndividualSavingsAccount$ID", new IndividualSavingsAccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.MortgageAccount$ID", new MortgageAccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.Account$ID", new AccountIdTranslator());
        TRANSLATORS.put("uk.co.cooperativebank.account.mts.dao.account.DomesticAccount$ID", new DomesticAccountIdTranslator());
    }


    @Override
    public HostTranslatorInterface getTranslator(String type) {
        return TRANSLATORS.get(type);
    }

    @Override
    public boolean canResolve(String type) {
        return TRANSLATORS.containsKey(type);
    }

}
