package uk.co.cooperativebank.account.dtos;

import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceRequest;
import uk.co.cooperativebank.account.mts.gateway.ProcessRequest;

public class CustomerRequest extends ProcessRequest implements EnterpriseServiceRequest {
  private String sortCode;
  private String accountNumber;
  private String creditCardNumber;

  public String getSortCode() {
    return sortCode;
  }

  public void setSortCode(String sortCode) {
    this.sortCode = sortCode;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getCreditCardNumber() {
    return creditCardNumber;
  }

  public void setCreditCardNumber(String creditCardNumber) {
    this.creditCardNumber = creditCardNumber;
  }
}
