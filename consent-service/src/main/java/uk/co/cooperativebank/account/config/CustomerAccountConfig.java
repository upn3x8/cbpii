package uk.co.cooperativebank.account.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.jms.JmsHeaderMapper;
import org.springframework.integration.jms.JmsOutboundGateway;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Payload;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseGateway;
import uk.co.cooperativebank.account.mts.templates.MessageTemplateHelper;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslation;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import java.util.Arrays;

@Configuration
public class CustomerAccountConfig {

  private static final int RECEIVE_TIMEOUT_MILLIS = 60000;
  private static final String ACCOUNT_REQUEST_TEMPLATE_FILE_LOCATION = "/accounts-host-templates.xml";
  private static final String CUSTOMER_REQUEST_TEMPLATE_FILE_LOCATION = "/customer-host-templates.xml";
  private final ConnectionFactory connectionFactory;

  private final JmsHeaderMapper headerMapper;

  private final MessageChannel reply;
  private final MessageChannel replySM;

  // Coop queues
  private final Queue replyQueue;
  private final Queue accountEnquiryQueue;
  private final Queue customerEnquiryQueue;

  // Smile queues
  private final Queue replyQueueSM;
  private final Queue accountEnquiryQueueSM;
  private final Queue customerEnquiryQueueSM;

  private final EnterpriseTranslation hostEncode;

  @Autowired
  public CustomerAccountConfig(EnterpriseTranslation hostEncode,
                               ConnectionFactory connectionFactory,
                               JmsHeaderMapper headerMapper,
                               @Qualifier("reply") MessageChannel reply,
                               @Qualifier("replySM") MessageChannel replySM,
                               @Qualifier("replyQueue") Queue replyQueue,
                               @Qualifier("accountEnquiryQueue") Queue accountEnquiryQueue,
                               @Qualifier("customerEnquiryQueue") Queue customerEnquiryQueue,
                               @Qualifier("replyQueueSM") Queue replyQueueSM,
                               @Qualifier("accountEnquiryQueueSM") Queue accountEnquiryQueueSM,
                               @Qualifier("customerEnquiryQueueSM") Queue customerEnquiryQueueSM) {
    this.hostEncode = hostEncode;
    this.connectionFactory = connectionFactory;
    this.headerMapper = headerMapper;
    this.reply = reply;
    this.replySM = replySM;
    this.replyQueue = replyQueue;
    this.accountEnquiryQueue = accountEnquiryQueue;
    this.customerEnquiryQueue = customerEnquiryQueue;
    this.replyQueueSM = replyQueueSM;
    this.accountEnquiryQueueSM = accountEnquiryQueueSM;
    this.customerEnquiryQueueSM = customerEnquiryQueueSM;
  }

  @Bean
  public MessageChannel accountEnquiry() {
    return new DirectChannel();
  }

  @Bean
  public MessageChannel accountEnquirySM() {
    return new DirectChannel();
  }

  @Bean
  public MessageChannel customerEnquiry() {
    return new DirectChannel();
  }

  @Bean
  public MessageChannel customerEnquirySM() {
    return new DirectChannel();
  }

  @Bean
  public MessageChannel customerSecurityTransformed() {
    return new DirectChannel();
  }

  @Bean
  public MessageChannel customerSecurityTransformedSM() {
    return new DirectChannel();
  }

  @Transformer(inputChannel = "accountEnquiry", outputChannel = "accountEnquiryTransformed")
  public String accountEnquiryHostTemplateTransformerEncode(@Payload EnterpriseGateway.EnterprisePayload payload) {
    return hostEncode.encode(payload);
  }

  @Transformer(inputChannel = "accountEnquirySM", outputChannel = "accountEnquiryTransformedSM")
  public String accountEnquiryHostTemplateTransformerEncodeSM(@Payload EnterpriseGateway.EnterprisePayload payload) {
    return hostEncode.encode(payload);
  }

  @Transformer(inputChannel = "customerEnquiry", outputChannel = "customerEnquiryTransformed")
  public String customerEnquiryHostTemplateTransformerEncode(@Payload EnterpriseGateway.EnterprisePayload payload) {
    return hostEncode.encode(payload);
  }

  @Transformer(inputChannel = "customerEnquirySM", outputChannel = "customerEnquiryTransformedSM")
  public String customerEnquiryHostTemplateTransformerEncodeSM(@Payload EnterpriseGateway.EnterprisePayload payload) {
    return hostEncode.encode(payload);
  }

  @Bean
  @ServiceActivator(inputChannel = "accountEnquiryTransformed")
  public JmsOutboundGateway accountEnquiryJmsGateway() {
    return getJmsOutboundGateway(accountEnquiryQueue, replyQueue, reply);
  }

  @Bean
  @ServiceActivator(inputChannel = "accountEnquiryTransformedSM")
  public JmsOutboundGateway accountEnquiryJmsGatewaySM() {
    return getJmsOutboundGateway(accountEnquiryQueueSM, replyQueueSM, replySM);
  }

  @Bean
  @ServiceActivator(inputChannel = "customerEnquiryTransformed")
  public JmsOutboundGateway customerEnquiryJmsGateway() {
    return getJmsOutboundGateway(customerEnquiryQueue, replyQueue, reply);
  }

  @Bean
  @ServiceActivator(inputChannel = "customerEnquiryTransformedSM")
  public JmsOutboundGateway customerEnquiryJmsGatewaySM() {
    return getJmsOutboundGateway(customerEnquiryQueueSM, replyQueueSM, replySM);
  }

  @PostConstruct
  public void initialiseServicingTemplates() {
    new MessageTemplateHelper().loadResources(Arrays.asList(CUSTOMER_REQUEST_TEMPLATE_FILE_LOCATION, ACCOUNT_REQUEST_TEMPLATE_FILE_LOCATION));
  }

  private JmsOutboundGateway getJmsOutboundGateway(Queue requestDetinationQueue, Queue replyQueue, MessageChannel replyChannel) {
    JmsOutboundGateway gw = new JmsOutboundGateway();
    gw.setConnectionFactory(connectionFactory);
    gw.setRequestDestination(requestDetinationQueue);
    gw.setReplyDestination(replyQueue);
    gw.setReplyChannel(replyChannel);
    gw.setReceiveTimeout(RECEIVE_TIMEOUT_MILLIS);
    gw.setHeaderMapper(headerMapper);
    // TODO: What needs to be used for the correlation-id
    //   gw.setCorrelationKey("JMSCorrelationID");
    return gw;
  }
}
