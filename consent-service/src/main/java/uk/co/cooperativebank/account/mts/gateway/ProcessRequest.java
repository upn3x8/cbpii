package uk.co.cooperativebank.account.mts.gateway;


import uk.co.cooperativebank.account.mts.contexts.ClientContext;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static uk.co.cooperativebank.account.mts.templates.utilities.SensitivePropertyUtil.reflectObjectToStringExcludingSensitiveProperties;

/**
 * Base interface for all business process request objects.
 * <p>
 * Provides client context which can be anything that suits the application
 * e.g. a HashMap or strong type. This will contain items such as channel
 * identifier, user environment/location details, security tokens etc.
 *
 * @author dmartin
 */
public abstract class ProcessRequest implements IntegrationMethodRequest {

  private static final long serialVersionUID = 1L;

  @NotNull
  @Valid
  private ClientContext clientContext;

  public ProcessRequest() {
  }

  @Override
  public final ClientContext getClientContext() {
    return clientContext;
  }

  public final void setClientContext(ClientContext clientContext) {
    this.clientContext = clientContext;
  }

  @Override
  public String toString() {
    return reflectObjectToStringExcludingSensitiveProperties(this);
  }
}
