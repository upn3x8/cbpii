package uk.co.cooperativebank.account.mts.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.contexts.PropertyUtils;

import java.util.Properties;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.RESOURCE_NOT_FOUND;

public class MainframeApplicationErrorCode {

    private MainframeApplicationErrorCode() {
    }

    private static final String MAINFRAME_APPLICATION_ERROR_CODES = "mainframeApplicationErrorCodes.properties";

    private static final Logger LOG = LoggerFactory.getLogger(MainframeApplicationErrorCode.class);

    private static final Properties mainframeApplicationErrorCodes;

    static {
        mainframeApplicationErrorCodes = PropertyUtils.loadProperties(MainframeApplicationErrorCode.class, MAINFRAME_APPLICATION_ERROR_CODES);
    }

    public static String resolveMainframeApplicationErrorCodeToToken(String errorCode) {

        String errorToken = mainframeApplicationErrorCodes.getProperty(errorCode);
        if(errorToken == null) {
            LOG.error(String.format("Unrecognised mainframe application error code: %1s. This error code does not exist in %2s", errorCode, MAINFRAME_APPLICATION_ERROR_CODES));
            throw new ConfigurationAlert(new AlertReasonDetail(RESOURCE_NOT_FOUND.getCode(), RESOURCE_NOT_FOUND.getDescription()));
        }

        return errorToken;
    }
}