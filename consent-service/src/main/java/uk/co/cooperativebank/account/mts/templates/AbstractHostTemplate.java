package uk.co.cooperativebank.account.mts.templates;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.dao.headers.BasicResponseHeader;
import uk.co.cooperativebank.account.mts.dao.headers.BusinessServiceEnquiryResponseHeader;
import uk.co.cooperativebank.account.mts.dao.headers.BusinessServiceTransactionResponseHeader;
import uk.co.cooperativebank.account.mts.dao.headers.TechnicalServiceEnquiryResponse;
import uk.co.cooperativebank.account.mts.exceptions.AlertReasonDetail;
import uk.co.cooperativebank.account.mts.exceptions.EnterpriseTranslationAlert;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseGateway;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceRequest;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.DECODING_FAILED;

public abstract class AbstractHostTemplate {

  public static final String SUCCESS_RESPONSE_NUMBER = "0000";
  private static final DateTimeFormatter TIMESTAMP_FORMAT =
      DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss");
  private static final String IP_ADDRESS_COUNTRY = null;
  private static final String IP_FULL_LOCATION = null;
  private static final String IP_ORGANISATION_NAME = null;
  private static final String IP_ISP = null;
  private static final String BUSINESS_ENQUIRY = "BE";
  private static final String BUSINESS_TRANSACTION = "BT";
  private static final String TECHNICAL_ENQUIRY = "TE";
  private static final String TECHNICAL_TRANSACTION = "TT";
  private static final char SPACE_CHAR = ' ';
  private static final String ZERO_DATE = "0000-00-00";

  private static final String CHARGE = "00";

  private static final String TIME_PART_STRING = "00:00:00";

  private static final Logger LOG = LoggerFactory.getLogger(AbstractHostTemplate.class);

  private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("ddMMyyyyHHmmssss");

  /**
   * Parse the String received from the host to extract the
   * StandardHostReponseInformation object
   *
   * @param data the host record
   * @return a HashMap
   */
  public HashMap getResponseHeader(String data) throws EnterpriseTranslationAlert {
    LOG.debug("getResponseHeader: Entering()...<<" + data + ">>");
    HashMap returnMap = new HashMap();

    try {
      Integer recordsReturned = 0;
      String businessData = null;

      String responseKey = "responseInfo";
      String numberKey = "numberReturned";
      String dataKey = "dataRecords";
      String reqNumKey = "requestNumber";

      String restartInd;

      int startPointer = 0;
      int endPointer = 2;

      String type = data.substring(startPointer, endPointer);
      LOG.debug("response is:" + type);
      if (type.equals(BUSINESS_ENQUIRY)) {
        BusinessServiceEnquiryResponseHeader header = new BusinessServiceEnquiryResponseHeader();

        startPointer = populateBasicResponse(header, data);

        // see if the response from the enterprise system is in error
        endPointer = startPointer + 5;
        header.setToken(data.substring(startPointer, endPointer).trim());
        startPointer += 5;

        if (header.getErrorCode().equals(SUCCESS_RESPONSE_NUMBER)) {
          LOG.debug("Success returned");

          endPointer += 1;
          restartInd = data.substring(startPointer, endPointer);
          startPointer += 1;

          if ("Y".equals(restartInd)) {
            endPointer += 100;
            header.setRestartKey(data.substring(startPointer, endPointer));
            startPointer += 100;
          }

          endPointer += 4;
          recordsReturned = getRecordsReturned(recordsReturned, data, startPointer, endPointer);
          header.setNumberReturned(data.substring(startPointer, endPointer));
          startPointer += 4;

          businessData = data.substring(startPointer);
        }
        returnMap.put(responseKey, header);

      } else if (type.equals(BUSINESS_TRANSACTION)) {
        BusinessServiceTransactionResponseHeader header = new BusinessServiceTransactionResponseHeader();

        startPointer = populateBasicResponse(header, data);

        // see if the response from the enterprise system is in error
        endPointer = startPointer + 5;
        header.setToken(data.substring(startPointer, endPointer).trim());
        startPointer += 5;

        returnMap.put(responseKey, header);

        //changes for 111079 OFS PSD
        if ("0175".equals(header.getRequestNumber())) {
          endPointer = startPointer + 4;
          recordsReturned = new Integer(data.substring(startPointer, endPointer));
          startPointer += 4;
          businessData = data.substring(startPointer);
        }

      } else if (type.equals(TECHNICAL_ENQUIRY)) {
        TechnicalServiceEnquiryResponse header = new TechnicalServiceEnquiryResponse();

        startPointer = populateBasicResponse(header, data);
        // see if the response from the enterprise system is in error
        if (header.getErrorCode().equals(SUCCESS_RESPONSE_NUMBER)) {

          endPointer = startPointer + 1;
          restartInd = data.substring(startPointer, endPointer);
          startPointer += 1;

          if ("Y".equals(restartInd)) {
            endPointer += 100;
            header.setRestartKey(data.substring(startPointer, endPointer).trim());
            startPointer += 100;
          }

          endPointer += 4;
          recordsReturned = new Integer(data.substring(startPointer, endPointer));
          header.setNumberReturned(data.substring(startPointer, endPointer));
          startPointer += 4;

          businessData = data.substring(startPointer);
        }
        returnMap.put(responseKey, header);

      } else if (type.equals(TECHNICAL_TRANSACTION)) {
        BasicResponseHeader header = new BasicResponseHeader();
        populateBasicResponse(header, data);
        returnMap.put(responseKey, header);
      } else {
        LOG.error("Cannot handle response type:" + type);
        throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
      }
      BasicResponseHeader header = (BasicResponseHeader) returnMap.get(responseKey);
      returnMap.put(reqNumKey, header.getRequestNumber());
      returnMap.put(numberKey, recordsReturned);
      if ((businessData != null) && (businessData.length() != 0)) {
        returnMap.put(dataKey, businessData);
      }
    } catch (NumberFormatException nfe) {
      String error = nfe.getMessage();
      LOG.error("NumberFormatException: " + error, nfe);
    }
    LOG.debug("returnMap is: " + returnMap);
    LOG.debug("getResponseHeader(): leaving...");
    return returnMap;
  }

  private Integer getRecordsReturned(Integer recordsReturned, String data, int startPointer, int endPointer) {
    Integer recordsReturnedResult = recordsReturned;
    try {
      recordsReturnedResult = new Integer(data.substring(startPointer, endPointer));
    } catch (NumberFormatException nfe) {
      LOG.error("error getting recordsReturned", nfe);
      if (LOG.isDebugEnabled()) {
        char[] chars = data.toCharArray();

        for (int i = 0; i < chars.length; i++) {
          LOG.debug("char " + i + " <<" + chars[i] + ">>");
        }

        LOG.debug("Records returned :" + data.substring(startPointer, endPointer));
        LOG.debug("startPointer :" + startPointer);
        LOG.debug("endPointer :" + endPointer);
      }
    }
    return recordsReturnedResult;
  }

  private int populateBasicResponse(BasicResponseHeader header, String data) {
    int startPointer = 0;
    int endPointer = 2;
    header.setType(data.substring(startPointer, endPointer));
    startPointer += 2;

    header.setTimeArrivedClient(new Date());

    endPointer += 11;
    header.setTimeLeftClient(parseTime(data.substring(startPointer, endPointer)));
    startPointer += 11;

    endPointer += 11;
    header.setTimeArrivedHost(parseTime(data.substring(startPointer, endPointer)));
    startPointer += 11;

    endPointer += 11;
    header.setTimeLeftHost(parseTime(data.substring(startPointer, endPointer)));
    startPointer += 11;

    endPointer += 4;
    header.setRequestNumber(data.substring(startPointer, endPointer));
    startPointer += 4;

    endPointer += 4;
    LOG.debug(
        "setting error code, start point = "
            + startPointer
            + ", end point = "
            + endPointer
            + ", to "
            + data.substring(startPointer, endPointer));
    header.setErrorCode(data.substring(startPointer, endPointer));
    startPointer += 4;

    endPointer += 1;
    String errorMessageInd = data.substring(startPointer, endPointer);
    startPointer += 1;

    if ("Y".equals(errorMessageInd)) {
      endPointer += 80;
      header.setErrorMessage(data.substring(startPointer, endPointer).trim());
      startPointer += 80;
    }
    return startPointer;
  }

  /**
   * Creates the sub-header for an enquiry request
   *
   * @param payload         the dao for the message
   * @param messageTemplate messageTemplate
   * @param userId          user id
   * @param brand           brand
   * @return Returns a String
   */
  protected String getRequestHeader(EnterpriseGateway.EnterprisePayload payload, MessageTemplate messageTemplate, String userId, String brand) {
    LOG.debug("getRequestHeader - Entering...");
    StringBuilder header = getBasicRequestHeader(payload.getRequest(), messageTemplate, userId, brand);
    if (messageTemplate.getType().equals(BUSINESS_ENQUIRY)) {
      header.append(getWebInformation(payload.getClientContext()));
      header.append(getSessionInformation(payload.getClientContext().getAuthenticationToken(), payload.getCustomerNumber()));
      header.append(getRestartKey(payload.getClientContext().getRestartKey()));
    } else if (messageTemplate.getType().equals(BUSINESS_TRANSACTION)) {
      header.append(getWebInformation(payload.getClientContext()));
      header.append(getSessionInformation(payload.getClientContext().getAuthenticationToken(), payload.getCustomerNumber()));
    } else if (messageTemplate.getType().equals(TECHNICAL_ENQUIRY)) {
      header.append(getRestartKey(payload.getClientContext().getRestartKey()));
    } else if (messageTemplate.getType().equals(TECHNICAL_TRANSACTION)) {
      //no extra info
    }
    header.append(messageTemplate.getArgumentsIndicator());

    LOG.debug("getRequestHeader - Leaving...");
    return header.toString();
  }

  /**
   * Create the header for a message inbound to the host.
   *
   * @param request         the dao for the message
   * @param messageTemplate messageTemplate
   * @param userId          user id
   * @param brand           brand
   * @return a String
   */
  public StringBuilder getBasicRequestHeader(EnterpriseServiceRequest request, MessageTemplate messageTemplate, String userId, String brand) {
    // encode the standard header
    StringBuilder sb = new StringBuilder();
    sb.append(Utilities.padStringAfter(userId, 6, ' '));
    sb.append(Utilities.padStringAfter(brand, 2, ' '));
    sb.append(Utilities.padStringAfter(messageTemplate.getType(), 2, ' '));
    sb.append(Utilities.padStringBefore(messageTemplate.getRequestNumber(), 4, '0'));
    //Set Host 'Change' & 'Sub-Change' Numbers (check for nulls to allow for backward compatibility)
    if (messageTemplate.getVersionNumber() != null) {
      if (messageTemplate.getVersionNumber().getMajor() != null) {
        sb.append(Utilities.padStringBefore(messageTemplate.getVersionNumber().getMajor(), 12, '0'));
      }
      if (messageTemplate.getVersionNumber().getMinor() != null) {
        sb.append(Utilities.padStringBefore(messageTemplate.getVersionNumber().getMinor(), 3, '0'));
      }
    }
    sb.append(Utilities.padStringBefore(CHARGE, 2, '0'));
    sb.append(timeStamp());

    return sb;
  }

  private String getRestartKey(String restartKey) {
    if (restartKey == null || "".equals(restartKey)) {
      return "N";
    } else {
      return "Y" + Utilities.padStringAfter(restartKey, 100, ' ');
    }
  }

  /**
   * A utility method which formats a String in 'alpha' format required by the
   * host. The input string is padded with trailing spaces and turned to upper
   * case.
   *
   * @param str the input string
   * @param len the length of the host field
   * @return Returns the formatted String
   */
  protected String formatAlpha(String str, int len) {
    String tempString = Utilities.padStringAfter(str, len, ' ');
    return tempString.toUpperCase();
  }

  /**
   * A utility method which formats a String in 'casesensitivealpha' format
   * required by the host. This will be used for fields containing mixed case
   * in the host.
   *
   * @param str the input string
   * @param len the length of the host field
   * @return Returns the formatted String
   */
  protected String formatCaseSensitiveAlpha(String str, int len) {
    return Utilities.padStringAfter(str, len, ' ');
  }

  /**
   * A utility method which formats a String in 'numeric' format required by
   * the host
   *
   * @param str the input string
   * @param len the length of the host field
   * @return Returns the formatted String
   */
  protected String formatNumeric(String str, int len) {
    return Utilities.padStringBefore(str, len, '0');
  }

  /**
   * A utility method which formats a String in 'date' format required by the
   * host
   *
   * @param date the date to be formatted
   * @return String - the formatted date
   */
  protected String formatDate(Date date, int len) {
    String str = " ";
    if (date != null) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
      str = sdf.format(date);
    }
    str = Utilities.padStringAfter(str, len, ' ');
    return str;
  }

  /**
   * A utility method which formats a String in 'time' format required by the
   * host
   *
   * @param str the input string
   * @return Returns the formatted String
   */
  protected String formatTime(String str) {
    return str;
  }

  /* A utility method which takes a BigDecimal object as input and returns a
   * string representation.
   *
   * @param
   BigDecimal the input Boolean
   * @
   return A String

   */
  protected String formatMoney(BigDecimal bigDecimal, int len) {
    String str = " ";
    if (bigDecimal != null) {
      str = String.format("%+.2f", bigDecimal);
    }
    str = Utilities.padStringBefore(str, len, ' ');
    return str;
  }

  /**
   * A utility method which takes a Boolean object as input and returns a
   * string representation.
   *
   * @param bool the input Boolean
   * @return A String 'Y' or 'N'
   */
  protected String formatBoolean(Boolean bool) {
    String str = " ";
    if (bool != null) {
      if (bool.equals(Boolean.FALSE)) {
        str = "N";
      } else {
        str = "Y";
      }
    }
    return str;
  }

  /**
   * A utility method which takes a String as input and returns the Integer.
   *
   * @param str the String to parse
   * @return a Integer
   */
  protected Integer parseInteger(String str) {
    String returnString = str.trim();
    return Integer.parseInt(returnString);
  }

  /**
   * A utility method which takes a String as input and returns the Long.
   *
   * @param str the String to parse
   * @return a Long
   */
  protected Long parseLong(String str) {
    String returnString = str.trim();
    return Long.parseLong(returnString);
  }

  /**
   * A utility method which takes a String as input and returns the same
   * String trimmed.
   *
   * @param str the String to parse
   * @return a trimmed String
   */
  protected String parseString(String str) {
    return str.trim();
  }

  /**
   * A utility method which takes a String as input and returns the Percent
   * object.
   *
   * @param str the String to parse
   * @return Percent object
   */
  protected BigDecimal parsePercent(String str) {
    return new BigDecimal(str.trim());
  }

  /**
   * A utility method which returns a space padded field
   *
   * @param length the length of the string to return
   * @return a padded String
   */
  protected String formatSpaceFiller(int length) {

    return Utilities.padStringAfter(null, length, SPACE_CHAR);
  }

  /**
   * A utility method which takes a date string of the form
   * 'ccyy-mm-ddhh:mm:ss' or 'ccyy-mm-dd' and returns a Date object
   *
   * @param str the string to parse
   * @return a Date
   */
  protected Date parseDate(String str) {

    Date date = null;

    // For refdata dates, the host returns an empty string, not null.
    // This behaviour cannot be changed so we have to workaround it.
    if (!str.contains(ZERO_DATE) && !"".equals(str.trim())) {
      // determine date type ie long/short and call appropriate func
      if (str.length() == 10) {
        date = parseShortDate(str);
      } else {
        if (str.length() == 18) {
          date = parseLongDate(str);
        }
      }
    }

    return date;
  }

  /**
   * A utility method which takes a date string of the form 'ccyy-mm-dd' and
   * returns a Date object
   *
   * @param str the string to parse
   * @return a Date
   */
  protected Date parseShortDate(String str) {
    return parseLongDate(str + TIME_PART_STRING);

  }

  /**
   * A utility method which takes a date string of the form
   * 'ccyy-mm-ddhh:mm:ss' and returns a Date object
   *
   * @param str the string to parse
   * @return a Date
   */
  protected Date parseLongDate(String str) {
    return TIMESTAMP_FORMAT.parseDateTime(str).toDate();
  }

  /**
   * A utility method which takes a boolean string of the form 'Y' or 'N' and
   * returns a Boolean object
   *
   * @param str the string to parse
   * @return a Boolean
   */
  protected Boolean parseBoolean(String str) {

    Boolean bRet = null;

    // validate that incoming param is a Y or an N
    if (str != null) {
      if ("Y".equals(str)) {
        bRet = true;
      }
      if ("N".equals(str)) {
        bRet = false;
      }
    }

    return bRet;
  }

  /**
   * A utility method which takes a time string in either of the forms
   * 'hh:mm:ss' or 'hh:mm:ss:SS' and returns a Date object
   *
   * @param str the string to parse
   * @return a Date
   */
  protected Date parseTime(String str) {

    Calendar cal = Calendar.getInstance();

    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str.substring(0, 2)));
    cal.set(Calendar.MINUTE, Integer.parseInt(str.substring(3, 5)));
    cal.set(Calendar.SECOND, Integer.parseInt(str.substring(6, 8)));

    if (str.length() == 11) {
      // date string is in hh:mm:ss:SS format so process milliseconds value

      String millSec = str.substring(9);
      if (millSec.length() == 2) {
        // The banks host responds in hundredths of a second, add a 0
        millSec = millSec + "0";
      }
      cal.set(Calendar.MILLISECOND, Integer.parseInt(millSec));
    }

    return cal.getTime();
  }

  /**
   * A utility method which takes a space padded right justified money string
   * and returns a Money object.
   *
   * @param str the string to parse
   * @return a Money
   */
  protected BigDecimal parseMoney(String str) {
    String money = str.trim();
    if (money.length() == 0) {
      return null;
    }

    return new BigDecimal(money);
  }

  /**
   * A utility method which returns a String in 'time' format. (hh:mm:ss:mm:)
   *
   * @return Returns the formatted String
   */
  protected String timeStamp() {
    Calendar cal = Calendar.getInstance();

    StringBuilder buf = new StringBuilder();

    int hour = cal.get(Calendar.HOUR_OF_DAY);
    if (hour < 10) {
      buf.append("0");
    }
    buf.append(hour);
    buf.append(":");

    int mins = cal.get(Calendar.MINUTE);
    if (mins < 10) {
      buf.append("0");
    }
    buf.append(mins);
    buf.append(":");

    int secs = cal.get(Calendar.SECOND);
    if (secs < 10) {
      buf.append("0");
    }
    buf.append(secs);
    buf.append(":");

    int millis = cal.get(Calendar.MILLISECOND);
    if (millis > 99) {
      millis = millis / 10;
    }
    if (millis < 10) {
      buf.append("0");
    }
    buf.append(millis);

    return buf.toString();
  }

  /**
   * A utility method which takes a string and checks for the existence of a
   * 'null indicator' in the first character of the string. This indicator is
   * the hex value 0xFD or equivalent to decimal 253. When the String is
   * turned into a bytearray using 'UTF-16' the 8-bit byte returned is encoded
   * using twos complement (as all bytes are in Java). The twos complement
   * equivalent of the binary value 253 is -3 therefore this method checks for
   * the existence of a byte value of -3.
   *
   * @param str the string to check
   * @return true if the mainframe null indicator character is present, false
   * otherwise
   */
  public boolean isMainframeNullString(String str) {

    boolean isNull = false;

    String checkField = str.substring(0, 1);
    byte[] bytes = checkField.getBytes(StandardCharsets.UTF_16LE);

    if (bytes[0] == -39) {

      isNull = true;
    }
    return isNull;
  }

  /**
   * Method to add Web Information in Header
   *
   * @param authenticationToken the authentication token
   * @param customerNumber      customer number
   */
  private String getSessionInformation(String authenticationToken, String customerNumber) {
    StringBuilder tempSb = new StringBuilder();
    tempSb.append(authenticationToken.substring(authenticationToken.length() - 5));
    tempSb.append(setStringLength(customerNumber, 9, ' '));
    return tempSb.toString();
  }

  /**
   * Method to add Web Information in Header
   *
   * @param clientContext the object containing the dao
   */
  private String getWebInformation(ClientContext clientContext) {
    StringBuilder tempSb = new StringBuilder();
    tempSb.append(setStringLength(clientContext.getBrowser(), 32, ' '));
    tempSb.append(setStringLength(clientContext.getDomain(), 100, ' '));
    tempSb.append(setStringLength(clientContext.getOsType(), 32, ' '));
    tempSb.append(setStringLength(clientContext.getReferrerSite(), 100, ' '));
    tempSb.append(setStringLength(clientContext.getSession(), 50, ' '));
    tempSb.append(setStringLength(clientContext.getIpAddress(), 16, ' '));

    tempSb.append(setStringLength(IP_ADDRESS_COUNTRY, 3, ' '));
    tempSb.append(setStringLength(IP_FULL_LOCATION, 100, ' '));
    tempSb.append(setStringLength(IP_ORGANISATION_NAME, 100, ' '));
    tempSb.append(setStringLength(IP_ISP, 50, ' '));
    return tempSb.toString();
  }

  /**
   * A utility method which takes a String as input and returns the same
   * String of required length. i.e. if length of the string is lower than
   * required length it simply padded it with padChar, otherwise make a
   * substring of required length and return it.
   *
   * @param str            the input string
   * @param lengthRequired the full length of the string required
   * @param padChar        the character to be inserted to fill up the string
   * @return String the string of required length.
   */
  protected String setStringLength(String str, int lengthRequired, char padChar) {

    if (str != null && str.length() >= lengthRequired) {
      return str.substring(0, lengthRequired);
    } else {
      return Utilities.padStringAfter(str, lengthRequired, padChar);
    }

  }

  /**
   * A utility method which formats a String in 'date' format required by the
   * host
   *
   * @param date the date to be formatted
   * @return String - the formatted date
   */
  protected String formatDateTime(Date date, int len) {
    String str = " ";
    if (date != null) {
      str = DATE_TIME_FORMAT.format(date);
    }
    str = Utilities.padStringAfter(str, len, ' ');
    return str;
  }
}
