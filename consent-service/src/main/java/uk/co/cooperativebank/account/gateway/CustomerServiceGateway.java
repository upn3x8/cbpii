package uk.co.cooperativebank.account.gateway;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;
import uk.co.cooperativebank.account.mts.framework.DomainObject;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseGateway;

import java.util.List;


/**
 *
 * A @Gateway to access the customer data - the entry point to Spring-Integration.
 *
 * @author Digital-Dev
 */
@MessagingGateway
public interface CustomerServiceGateway extends EnterpriseGateway {
    @Gateway(requestChannel = "customerSecurity", replyChannel = "replyTransformed")
    List<? extends DomainObject> findCustomerByDigitalId(@Payload EnterprisePayload payload);

    @Gateway(requestChannel = "customerEnquiry", replyChannel = "replyTransformed")
    List<? extends DomainObject> findCustomerByAccount(@Payload EnterprisePayload request);

    @Gateway(requestChannel = "customerSecuritySM", replyChannel = "replyTransformedSM")
    List<? extends DomainObject> findCustomerByDigitalIdSM(@Payload EnterprisePayload payload);

    @Gateway(requestChannel = "customerEnquirySM", replyChannel = "replyTransformedSM")
    List<? extends DomainObject> findCustomerByAccountSM(@Payload EnterprisePayload request);
}
