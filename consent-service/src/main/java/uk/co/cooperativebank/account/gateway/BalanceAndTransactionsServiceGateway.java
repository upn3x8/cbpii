package uk.co.cooperativebank.account.gateway;


import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;
import uk.co.cooperativebank.account.mts.framework.DomainObject;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseGateway;

import java.util.List;


/**
 *
 * A @Gateway to access the balance and transaction data - the entry point to Spring-Integration.
 *
 * @author Digital-Dev
 */

@MessagingGateway
public interface BalanceAndTransactionsServiceGateway extends EnterpriseGateway {

    @Gateway(requestChannel = "accountEnquiry", replyChannel = "replyTransformed")
    List<? extends DomainObject> getAccounts(@Payload EnterprisePayload request);

    @Gateway(requestChannel = "accountEnquirySM", replyChannel = "replyTransformedSM")
    List<? extends DomainObject> getAccountsSM(@Payload EnterprisePayload request);
}
