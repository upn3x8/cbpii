package uk.co.cooperativebank.account.mts.encryption;

import uk.co.cooperativebank.account.mts.exceptions.EncryptionAlert;

/**
 * Simple interface that provides encryption and decryption facilities
 * Limitation are that only strings are taken as input and provided as output
 * for both processes
 *
 * @author Craig Parkinson
 */
public interface EncryptionService {

    /**
     * Encodes dao from an object into a flattened string.
     * @param data string to encrypt
     * @return the encrypted string
     * @throws EncryptionAlert an encryption exception
     */
    String encrypt(String data) throws EncryptionAlert;

    /**
     * Decodes a String back into a business object or collection of.
     * @param data string to decrypt
     * @return the decrypted string
     * @throws EncryptionAlert an encryption exception
     * */
    String decrypt(String data) throws EncryptionAlert;

}