package uk.co.cooperativebank.account.mts.exceptions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AlertingException extends RuntimeException {

    public abstract String getAlertID();

    public abstract AlertReasonDetail getAlertReasonDetail();

    public String getLogMessage(String severityCode, List <String> contextParams) {
        List<String> logData = new ArrayList <String>();
        logData.addAll(contextParams);
        logData.add("Alert ID:" + getAlertID());
        logData.add("Severity:" + severityCode);
        logData.add("Reason Code:" + getAlertReasonDetail());
        return logData.stream().collect(Collectors.joining("|"));
    }
}
