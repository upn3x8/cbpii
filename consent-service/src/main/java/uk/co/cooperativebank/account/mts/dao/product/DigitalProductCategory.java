package uk.co.cooperativebank.account.mts.dao.product;

import uk.co.cooperativebank.account.mts.framework.DomainObject;

public class DigitalProductCategory implements DomainObject {
    private String key;
    private String value;

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DigitalProductCategory() {
    }

    public DigitalProductCategory(String key, String value ){
        this.key = key;
        this.value =value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}