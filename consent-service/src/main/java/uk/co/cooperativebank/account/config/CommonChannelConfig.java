package uk.co.cooperativebank.account.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslation;

import java.util.List;
import java.util.Map;

@Configuration
public class CommonChannelConfig {

  private final EnterpriseTranslation hostDecode;

  public CommonChannelConfig(EnterpriseTranslation hostDecode) {
    this.hostDecode = hostDecode;
  }

  @Transformer(inputChannel = "reply", outputChannel = "replyTransformed")
  public List replyHostTemplateTransformerDecode(@Payload String returnedData, @Headers Map<String, Object> headersMap) {
    return hostDecode.decode(returnedData, headersMap);
  }

  @Transformer(inputChannel = "replySM", outputChannel = "replyTransformedSM")
  public List replyHostTemplateTransformerDecodeSM(@Payload String returnedData, @Headers Map<String, Object> headersMap) {
    return hostDecode.decode(returnedData, headersMap);
  }
}