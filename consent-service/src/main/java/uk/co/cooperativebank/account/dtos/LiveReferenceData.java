package uk.co.cooperativebank.account.dtos;

public class LiveReferenceData {
    public LiveReferenceData(String jsonData, String dataType, String dataKey, String sortBy) {
        this.jsonData = jsonData;
        this.dataType = dataType;
        this.dataKey = dataKey;
        this.sortBy = sortBy;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    private String jsonData, dataType, dataKey, sortBy;
}
