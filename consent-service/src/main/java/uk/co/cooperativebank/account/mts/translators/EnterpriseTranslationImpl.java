package uk.co.cooperativebank.account.mts.translators;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import uk.co.cooperativebank.account.mts.exceptions.AlertReasonDetail;
import uk.co.cooperativebank.account.mts.exceptions.EnterpriseTranslationAlert;
import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseGateway;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceRequest;
import uk.co.cooperativebank.account.mts.templates.*;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.*;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.DECODING_FAILED;
import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.ENCODING_FAILED;
import static uk.co.cooperativebank.account.mts.templates.utilities.SensitivePropertyUtil.isPropertyNameSensitive;


/**
 * This class is the core of the Enterprise Translation Service. It's
 * responsibility is twofold:
 * <ul>
 * <li>Parsing all outbound messages (messages from the host), instantiating and
 * returning the relevant business objects to the business service layer</li>
 * <li>Creating all inbound messages (to the host) from the dao passed by the
 * business service layer</li>
 * </ul>
 * <p/>
 * Determination of which message to create/parse is done using the
 * MessageTemplate which is based on the transactionId and the message
 * direction.
 *
 * <p/>
 * This MessageTemplate object then is used to create or parse the actual
 * message.
 *
 * @author Steria
 * @version $Id$
 */
@Slf4j
public class EnterpriseTranslationImpl extends AbstractHostTemplate implements EnterpriseTranslation {

    private final HostTranslatorFactory hostTranslatorFactory;

    private final EnterpriseTranslationConfig enterpriseTranslationConfig;

    // constants defining the host dao types
    private static final String ALPHA_TYPE = "alpha";
    private static final String NULL_ALPHA_TYPE = "null_alpha";
    private static final String CASE_SENSITIVE_ALPHA_TYPE = "casesensitivealpha";
    private static final String NUMERIC_TYPE = "numeric";
    private static final String DATE_TYPE = "Date";
    private static final String TIME_TYPE = "time";
    private static final String BOOLEAN_TYPE = "Boolean";
    private static final String SPACE_FILLER = "space_filler";
    private static final String NUMERIC_FILLER = "numeric_filler";
    private static final String NULL_FILLER = "null_filler";
    private static final String MONEY_TYPE = "Money";
    private static final String CURRENT_CUSTOMER_NUMBER = "currentCustomerNumber";
    private static final String FILLER = "Filler";
    private static final String DATETIME_TYPE = "DateTime";

    private static final String LIST_DIRECTDEBIT_REQ_NUMBER = "0447";
    private static final String FIND_CUSTOMER_BY_ACCOUNT_REQ_NUMBER = "0488";
    private static final String MAINFRAME_SUCCESS_RESPONSE_CODE = "0000";
    private static final String SPI_MASK_STRING = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    public EnterpriseTranslationImpl(HostTranslatorFactory hostTranslatorFactory, EnterpriseTranslationConfig enterpriseTranslationConfig) {
        this.hostTranslatorFactory = hostTranslatorFactory;
        this.enterpriseTranslationConfig = enterpriseTranslationConfig;
    }

    /**
     * Encodes the key
     *
     * @param payload the dao to encode
     * @return a string
     * @throws EnterpriseTranslationAlert an integration exception
     */
    @Override
    @Transformer
    public String encode(@Payload EnterpriseGateway.EnterprisePayload payload) throws EnterpriseTranslationAlert {
        log.debug("encode(): Entering...");
        log.info("key={}", payload.toString());

        EnterpriseServiceRequest request = payload.getRequest();
        String transId = payload.getService().getName();

        MessageTemplate transMessage = EnterpriseServiceTemplates.getInboundMessageTemplate(transId);

        //Encode the header

        StringBuilder buffer = new StringBuilder(getRequestHeader(payload, transMessage, enterpriseTranslationConfig.getUserId(), payload.getClientContext().getBrand()));
        StringBuilder encodeLog = new StringBuilder();
        encodeLog.append("Encoder log: ");

        //Encode the message body
        try {
            List messageFormat = transMessage.getMessageFields();
            for (Object messageFormat1 : messageFormat) {
                MessageField field = (MessageField) messageFormat1;
                String fieldValue = encodeField(request, field, payload.getCustomerNumber(), encodeLog);
                if (field.getOutputLength() != fieldValue.length()) {
                    String error
                            = "Passed in dao is greater than allowed for field name:"
                            + field.getName()
                            + ", value:"
                            + fieldValue
                            + ", expected length:"
                            + field.getOutputLength()
                            + ", actual length:"
                            + fieldValue.length();
                    log.error(error);
                    log.info(encodeLog.toString());
                    throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENCODING_FAILED.getCode(), ENCODING_FAILED.getDescription()));
                }
                buffer.append(fieldValue);
            }
            log.debug(encodeLog.toString());

        } catch (NoSuchMethodException nse) {
            String error = nse.getMessage();
            log.error(error, nse);
            log.debug(encodeLog.toString());
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENCODING_FAILED.getCode(), ENCODING_FAILED.getDescription()));
        } catch (IllegalAccessException iae) {
            String error = "IllegalAccessException";
            log.error(error, iae);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENCODING_FAILED.getCode(), ENCODING_FAILED.getDescription()));
        } catch (InvocationTargetException ite) {
            String error = "InvocationTargetException";
            log.error(error, ite.getTargetException(), ite);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENCODING_FAILED.getCode(), ENCODING_FAILED.getDescription()));
        }
        log.debug("encode(): Leaving...");
        log.info("request=" + buffer);
        return buffer.toString();
    }


    /**
     * Decodes the returned dao
     *
     * @param returnedData the dao to decode
     * @param headersMap headers
     * @return an arraylist list of DOM objects
     * @throws EnterpriseTranslationAlert an integration exception
     */
    @Override
    @Transformer
    public List decode(@Payload String returnedData, @Headers Map<String, Object> headersMap) throws EnterpriseTranslationAlert {

        log.debug("decode(): Entering...");
        if(FIND_CUSTOMER_BY_ACCOUNT_REQ_NUMBER.equals(returnedData.substring(35, 39)) && MAINFRAME_SUCCESS_RESPONSE_CODE.equals(returnedData.substring(39, 43))){
            String maskedReturnedData = maskSPIDetails(returnedData);
            log.info("returnedData=" + maskedReturnedData);
        } else{
            log.info("returnedData=" + returnedData);
        }
        log.info("headers=" + headersMap);

        ArrayList arrayList = new ArrayList();
        String type;
        HashMap headerMap = getResponseHeader(returnedData);
        arrayList.add(headerMap.get("responseInfo"));

        if (null != headerMap.get("dataRecords")) {
            String id = (String) headerMap.get("requestNumber");
            MessageTemplate transMessage = EnterpriseServiceTemplates.getOutboundMessageTemplate(id);
            String useableData = (String) headerMap.get("dataRecords");
            type = transMessage.getMessageType();

            if ("delimited".equals(type)) {
                arrayList.add(decodeDelimitedMessage(useableData, transMessage));
            } else {
                decodeOtherType(type, transMessage, headerMap, useableData, arrayList, headersMap);
            }
        }
        log.debug("decode(): Leaving");
        return arrayList;
    }

    private String maskSPIDetails(String returnedData) {
        String maskedData = returnedData;
        int lengthOfOneRecord=474;
        int endIndex=maskedData.length();
        int startSPIIndex=0;
        int endSPIIndex=0;
        while(endIndex > lengthOfOneRecord){
            endSPIIndex=endIndex-23;
            startSPIIndex=endSPIIndex-67;
            maskedData = maskedData.substring(0,startSPIIndex) + SPI_MASK_STRING +maskedData.substring(endSPIIndex);
            endIndex = endIndex - lengthOfOneRecord;
        }
        return maskedData;
    }



    private void logResponseSummary(int loopSize, String useableData) {
        if (log.isInfoEnabled()) {
            try {
                StringBuilder s = new StringBuilder();
                s.append(" number returned = ");
                s.append(Integer.toString(loopSize));
                if (useableData != null) {
                    s.append(" useable dao size = ");
                    s.append(Integer.toString(useableData.length()));
                }

                log.info(s.toString());
            } catch (Exception e) {
                log.error("Error in logRecordsSummary", e);
            }
        }
    }

    private void decodeOtherType(String type, MessageTemplate transMessage, HashMap headerMap,
                                 String useableData, ArrayList arrayList, Map<String, Object> headersMap) throws EnterpriseTranslationAlert {

        int recordSize = transMessage.getLength();

        int loopSize = 0;
        if (headerMap.get("numberReturned") != null) {
            loopSize = (Integer) headerMap.get("numberReturned");
        }
        String requestNumber = (String)headerMap.get("requestNumber");
        logResponseSummary(loopSize, useableData);

        for (int i = 0; i < loopSize; i++) {
            int startPos = recordSize * i;
            String subRecord = useableData.substring(startPos, startPos + recordSize);
            log.debug("decoding the " + i + " record");
            if ("complex".equals(type)) {
                arrayList.add(decodeComplexMessage(subRecord, transMessage, headersMap, requestNumber));
            } else {
                arrayList.add(decodeSimpleMessage(subRecord, transMessage, requestNumber));
            }
        }
    }

    private Object decodeDelimitedMessage(String message, MessageTemplate template) {
        Object obj = null;
        try {
            Class c = Class.forName(template.getTranslatorClass());
            DelimitedTranslator translator = (DelimitedTranslator) c.newInstance();

            c = Class.forName(template.getMessageClass());
            obj = c.newInstance();

            // delegate the decoding to the given translator
            translator.decode(message, obj);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage(), e);
        }
        return obj;
    }

    private void logInfoForFieldToBeDecoded(Object object, String fieldName, String fieldValue, StringBuilder decodeLog) {

        String processedFieldValue;

        if (isPropertyNameSensitive(object, fieldName)) {
            processedFieldValue = "****";
        } else {
            processedFieldValue = fieldValue;
        }

        decodeLog.append(" ");
        decodeLog.append(fieldName);
        decodeLog.append("<");
        decodeLog.append(processedFieldValue);
        decodeLog.append(">,");
    }

    /**
     * Decodes an actual returned business message
     *
     * @param returnedData the dao to decode
     * @param template the template to use
     * @return an Object
     */
    private Object decodeSimpleMessage(String returnedData, MessageTemplate template, String requestNumber) throws EnterpriseTranslationAlert {
        Object obj = null;

        log.debug("decodeSimpleMessage(): Entering...");
        StringBuilder decodeLog = new StringBuilder();
        decodeLog.append("Decoded m/f message: raw dao - ");
        boolean logInfoIsEnabled = log.isInfoEnabled();

        try {
            BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
            Class c = Class.forName(template.getMessageClass());
            obj = c.newInstance();
            List messageFormat = template.getMessageFields();
            int startPointer = 0;
            int endPointer = 0;

            for (int i = 0; i < messageFormat.size(); i++) {
                MessageField field = (MessageField) messageFormat.get(i);
                int fieldLength = field.getLength();
                String fieldType = field.getType();
                String fieldName = field.getName();
                endPointer += fieldLength;

                if (logInfoIsEnabled && !FILLER.equals(fieldType)) {
                    logInfoForFieldToBeDecoded(obj, fieldName, returnedData.substring(startPointer, endPointer), decodeLog);
                }

                if (FILLER.equals(fieldType)) {
                    decodeLog.append(" <<Filler>>,");
                    // Do nothing as this field is not required ...
                } else if (isMainframeNullString(returnedData.substring(startPointer, endPointer))) {
                    // Return this field as null as the host has sent a null identified string
                    BeanUtils.setProperty(obj, fieldName, null);
                } else if ("Money".equals(fieldType)) {
                    BigDecimal tempMoney = parseMoney(returnedData.substring(startPointer, endPointer));
                    BeanUtils.setProperty(obj, fieldName, tempMoney);
                } else if ("Percent".equals(fieldType)) {
                    BigDecimal percent = parsePercent(returnedData.substring(startPointer, endPointer));
                    BeanUtils.setProperty(obj, fieldName, percent);
                } else if ("Date".equals(fieldType)) {
                    Date tempDate = parseDate(returnedData.substring(startPointer, endPointer));
                    BeanUtils.setProperty(obj, fieldName, tempDate);
                } else if ("String".equals(fieldType)) {
                    String tempString = null;
                    if((requestNumber != null) && requestNumber.equals(LIST_DIRECTDEBIT_REQ_NUMBER) && fieldName.equals("beneficiaryName")){
                        tempString = returnedData.substring(startPointer, endPointer);
                    }else{
                        tempString = parseString(returnedData.substring(startPointer, endPointer));
                    }
                    BeanUtils.setProperty(obj, fieldName, tempString);
                } else if ("Integer".equals(fieldType)) {
                    Integer tempInteger = parseInteger(returnedData.substring(startPointer, endPointer));
                    BeanUtils.setProperty(obj, fieldName, tempInteger);
                } else if ("Long".equals(fieldType)) {
                    Long tempLong = parseLong(returnedData.substring(startPointer, endPointer));
                    BeanUtils.setProperty(obj, fieldName, tempLong);
                } else if ("Boolean".equals(fieldType)) {
                    Boolean tempBool = parseBoolean(returnedData.substring(startPointer, endPointer));
                    if (tempBool != null) {
                        BeanUtils.setProperty(obj, fieldName, tempBool);
                    }

                } else {
                    // ok, added support for custom objects not defined within this file.
                    // We do not want to have to modify external
                    // integration to support the encoding and decoding of new Java types.
                    // so if object supports HostTransaltionInterface we externalise
                    // the transalation so that we
                    // do not have to moidfy this class/package each time we
                    // make a trivial change
                    if (decodeHostTransalationType(obj,
                            fieldType,
                            fieldName,
                            returnedData.substring(startPointer, endPointer))) {
                        decodeLog.append("Decoded customer object '");
                        decodeLog.append(fieldName);
                        decodeLog.append("',");
                    } else {
                        // Field type is not recognised ...
                        String error = "Unknown Field type: " + fieldType + "  for field: " + fieldName;
                        log.error(error);
                        throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
                    }
                }
                startPointer += fieldLength;
            }
        } catch (IllegalAccessException iae) {
            String error = "IllegalAccessException";
            log.error(error + " :" + iae.toString(), iae);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
        } catch (InvocationTargetException ite) {
            String error = "InvocationTargetException";
            log.error(error + " :" + ite.toString(), ite);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
        } catch (ClassNotFoundException cnfe) {
            String error = "ClassNotFoundException";
            log.error(error + " :" + cnfe.toString(), cnfe);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
        } catch (InstantiationException ie) {
            String error = "InstantiationException";
            log.error(error + " :" + ie.toString(), ie);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
        } catch (HostFieldFormatException hfe) {
            log.error("HostFieldFormatException :" + hfe.getMessage(), hfe);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
        }

        if (logInfoIsEnabled) {
            if (obj != null) {
                decodeLog.append(" Record read from mainframe is ");
                decodeLog.append(obj);
            }
            log.debug(decodeLog.toString());
        }

        return obj;
    }

    /**
     * Decodes an actual returned business message
     *
     * @param returnedData the dao to decode
     * @param template the template to use
     * @return an Object
     */
    private Object decodeComplexMessage(String returnedData, MessageTemplate template, Map<String, Object> headersMap, String requestNumber) throws EnterpriseTranslationAlert {
        log.debug("decodeComplexMessage(): Entering...");
        // Get the switchvalue
        List messageFields = template.getMessageFields();
        int startPointer = 0;
        int endPointer = 0;
        int i = 0;
        MessageField switchField = (MessageField) messageFields.get(i);
        endPointer += switchField.getLength();
        while (FILLER.equals(switchField.getType())) {
            startPointer += switchField.getLength();
            i++;
            switchField = (MessageField) messageFields.get(i);
            endPointer += switchField.getLength();
        }
        Map options = template.getOptions();
        String switchValue = returnedData.substring(startPointer, endPointer);

        String decodeId = decodeRequestSpecificCode(switchField, switchValue, headersMap);
        String realTemplateId = (String) options.get(decodeId);

        log.debug("decodeComplexMessage(): Applying option template from switchvalue " + switchValue + ": " + realTemplateId + ":decodedId:" + decodeId);

        MessageTemplate realTemplate = EnterpriseServiceTemplates.getOutboundMessageTemplate(realTemplateId);

        Object obj;

        if ("complex".equals(realTemplate.getMessageType())) {
            obj = decodeComplexMessage(returnedData, realTemplate, headersMap, requestNumber);
        } else {
            obj = decodeSimpleMessage(returnedData, realTemplate, requestNumber);
        }

        log.debug("decodeComplexMessage(): Leaving...");
        return obj;
    }

    /**
     * Sub-classes can override to provide request specific logic
     *
     * @param switchField
     * @param switchValue
     * @param headersMap
     * @return
     */
    protected String decodeRequestSpecificCode(MessageField switchField, String switchValue, Map<String, Object> headersMap) {
        log.debug("decodeRequestSpecificCode(): retruning switchvalue " + switchValue);
        return switchValue;
    }

    private static void logEncodeValue(StringBuilder encodeLog, String fieldType, String fieldName, String fieldValue) {
        try {
            encodeLog.append(fieldType);
            encodeLog.append("-");
            encodeLog.append(fieldName);
            encodeLog.append(" val<");
            encodeLog.append(fieldValue);
            encodeLog.append(">,");
        } catch (Exception e) {
            log.error("Error in logEncodeValue", e);
        }
    }

    /**
     * Encodes a single field
     *
     * @param request the dao to encode
     * @param field the field to be encoded
     *
     */
    private String encodeField(EnterpriseServiceRequest request, MessageField field, String customerNumber, StringBuilder encodeLog)
            throws NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException,
            EnterpriseTranslationAlert {

        // ok, this works like this.
        // we support standard JDK java objects by adding an entry for the releavnt type at the top of this file
        // if the type is defined at the top of the file we simply get a ref to the member var and call a specific
        // transalation function that cna be found within AbstractHostTemplate.
        // If this si a custom object which is not defined as a type within this class we get a ref to the mamaber var
        // which should implement the HostTransalationInterface. We then call encode upon this object.
        // the benefit of soing this is that we can add support for new objects to extrandla integration without having
        // to change ExtrandlaIntegration each time we ant to add a new object
        String s = null;

        String fieldType = field.getType();
        int length = field.getLength();
        // see if an 'ouputlength' var was specified in the message templates
        int outlength = field.getOutputLength();
        // was value specified or not
        if (outlength == MessageField.EMPTY) {
            outlength = length;
        }
        // ok, we have an exception to the rule with FILLER objects.
        // if a field if of type XX_FILLER then we do not have a valid member varr to populate
        // or read from as no actual dat exists for this field
        if (fieldType.equals(SPACE_FILLER)) {
            s = formatSpaceFiller(length);
            logEncodeValue(encodeLog, fieldType, field.getName(), s);
            return s;
        }
        if (fieldType.equals(NUMERIC_FILLER)) {
            s = formatNumeric("0", length);
            logEncodeValue(encodeLog, fieldType, field.getName(), s);
            return s;
        }
        if (fieldType.equals(NULL_FILLER)) {
            s = Utilities.getHostNull(length);
            logEncodeValue(encodeLog, fieldType, field.getName(), s);
            return s;
        }

        // not custom so must be defined as a type within this file
        try {
            //If the host template requires customer number - use the customer number from the header / payload
            //Really the mainframe should never require this as it is already receiving this value in the header.
            //As such, this has been added for legacy reasons
            if (CURRENT_CUSTOMER_NUMBER.equals(field.getName())) {
                log.info("customerNumber requested in message body - using customerNumber from header: " + customerNumber);
                s = customerNumber;
            } else {
                s = BeanUtils.getProperty(request, field.getName());
            }
        } catch (NullPointerException npe) {
            log.info("unable to get field using null on field:" + field.getName(), npe);
        } catch (IllegalArgumentException iae) {
            log.info("unable to get field using null on field:" + field.getName(), iae);
        } catch (InvocationTargetException ite) {
            log.info("unable to get field using null on field:" + field.getName(), ite);
        }

        if (log.isInfoEnabled()) {
            encodeLog.append(" ");
            if (isPropertyNameSensitive(request, field.getName())) {
                encodeLog.append(field.getName());
                encodeLog.append("=<*****>,");
            } else {
                encodeLog.append(field.getName());
                encodeLog.append("=<");
                encodeLog.append(s);
                encodeLog.append(">,");
            }
        }

        if (fieldType.equals(NULL_ALPHA_TYPE)) {
            if (s == null) {
                s = Utilities.getHostNull(length);
            } else {
                s = formatAlpha(s, length);
            }
        } else if (fieldType.equals(ALPHA_TYPE)) {
            s = formatAlpha(s, length);
        } else if (fieldType.equals(CASE_SENSITIVE_ALPHA_TYPE)) {
            s = formatCaseSensitiveAlpha(s, length);
        } else if (fieldType.equals(NUMERIC_TYPE)) {
            s = formatNumeric(s, length);
        } else if (fieldType.equals(DATE_TYPE)) {
            Date date = (Date) PropertyUtils.getProperty(request, field.getName());
            s = formatDate(date, length);
        } else if (fieldType.equals(TIME_TYPE)) {
            s = formatTime(s);
        }else if (fieldType.equals(DATETIME_TYPE)) {
            Date date = (Date) PropertyUtils.getProperty(request, field.getName());
            s = formatDateTime(date, length);
        }else if (fieldType.equals(BOOLEAN_TYPE)) {
            Boolean b = (Boolean) PropertyUtils.getProperty(request, field.getName());
            s = formatBoolean(b);
        } else if (fieldType.equals(MONEY_TYPE)) {
            BigDecimal a = (BigDecimal) PropertyUtils.getProperty(request, field.getName());
            s = formatMoney(a, length);
        } else {
            // check if the field type is a custom object
            // not custom so must be defined as a type within this file
            Object obj = null;
            try {
                obj = PropertyUtils.getProperty(request, field.getName());
            } catch (NullPointerException npe) {
                log.info("unable to get field using null on field:" + field.getName(), npe);
            } catch (IllegalArgumentException iae) {
                log.info("unable to get field using null on field:" + field.getName(), iae);
            } catch (InvocationTargetException ite) {
                log.info("unable to get field using null on field:" + field.getName(), ite);
            }
            String customString = encodeHostTransalationType(obj, fieldType, length, outlength);
            if (customString != null) {
                return customString;
            } else {
                // we need to do some validation here. If we have not found the correct type
                // we should throw an exception. Otherwise the toString representation of th object will
                // be placed into the input buffer.
                String error = "Unknown field type '" + fieldType + "' found processing field '" + field.getName();
                log.error(error);
                throw new EnterpriseTranslationAlert(new AlertReasonDetail(DECODING_FAILED.getCode(), DECODING_FAILED.getDescription()));
            }
        }
        return s;
    }

    /**
     * decode a single field
     *
     * @param obj the object to check for HostTransalationInterface support
     * @param fieldType - the type of the field
     * @param inlength the length of the field.
     * @param outlength the length to be output
     * @return the encoded string or null if type not supported
     * @throws HostFieldFormatException - value within object is invalid
     *
     *
     */
    protected String encodeHostTransalationType(Object obj, String fieldType, int inlength, int outlength) throws HostFieldFormatException {

        HostTranslatorInterface translator;
        // get translatore object for spoecified field type
        try {
            translator = hostTranslatorFactory.getHostTranlator(fieldType);
        } catch (Exception ex) {
            log.info("Handled exception", ex);
            // not found
            return null;
        }

        // perform the encdoing
        String strResult = translator.encode(obj, inlength, outlength);

        return strResult;
    }

    /**
     * checks if the field implements the HostTransalationInterface abnd if so
     * invokes this method to decode the dao
     *
     * @param obj the obj to check for HostTransalationInterface support
     * @param fieldType the type of the field
     * @param fieldName the name of the field
     * @param data the raw dao;
     * @return true if object of type HostTransalationType and parse successful
     * @throws HostFieldFormatException - value within object is invalid
     *
     */
    protected boolean decodeHostTransalationType(Object obj, String fieldType, String fieldName, String data) throws HostFieldFormatException {

        boolean bRet = false;

        try {

            // get object we require
            Object fieldObj = getLastObjectInHiearchcy(obj, fieldName, fieldType);

            if (fieldObj == null) {
                throw new HostFieldFormatException(
                        "Error occured parsing field '" + fieldName + "' of type '" + fieldType + "'");
            }

            HostTranslatorInterface translator;
            // get translatore object for spoecified field type
            try {
                translator = hostTranslatorFactory.getHostTranlator(fieldType);
            } catch (Exception ex) {
                log.info("Handled exception", ex);
                // not found
                return false;
            }

            // perform the decoding
            Object dataObj = translator.decode(data);
            BeanUtils.setProperty(obj, fieldName, dataObj);
            bRet = true;

        } catch (ClassNotFoundException cnfe) {
            String error = "ClassNotFoundException";
            log.error(error, cnfe);
            throw new HostFieldFormatException(cnfe.getMessage());
        } catch (NoSuchMethodException nsme) {
            log.error("No such method exception", nsme);
            throw new HostFieldFormatException("No such method exception");
        } catch (IllegalAccessException iae) {
            String error = "IllegalAccessException";
            log.error(error, iae);
            throw new HostFieldFormatException(iae.getMessage());
        } catch (InvocationTargetException ite) {
            String error = "InvocationTargetException";
            log.error(error, ite);
            throw new HostFieldFormatException(ite.getMessage());
        } catch (InstantiationException iter) {
            String error = "InstantiationException";
            log.error(error, iter);
            throw new HostFieldFormatException(iter.getMessage());
        }

        return bRet;
    }

    /**
     * function takes a base object and a '.' delimietered string object
     * hierarcy If necessary it creates and then return a reference to the final
     * object within the hiearchry string. The object must be created to enable
     * us to reflectively invoke the encode and decode functions All objects bar
     * the last must be valid nbon null objects. This is not a limitation placed
     * upon by this func but by the BeanUtils package
     *
     * @param baseObj the base parent object
     * @param fieldname the name of the field
     * @param fieldtype the type of the field
     * @return true if object of type HostTransalationType and parse successful
     * @throws InstantiationException -
     * @throws ClassNotFoundException -
     * @throws InvocationTargetException -
     * @throws IllegalAccessException -
     * @throws NoSuchMethodException -
     *
     */
    protected Object getLastObjectInHiearchcy(Object baseObj, String fieldname, String fieldtype)
            throws
            InvocationTargetException,
            ClassNotFoundException,
            IllegalAccessException,
            NoSuchMethodException,
            InstantiationException {

        Object fieldObj = PropertyUtils.getProperty(baseObj, fieldname);

        // if object not valid we need to create the objecy to allow us to perform RTTI and subsequently
        // call the encode/decode method
        if (fieldObj == null) {
            Class c = Class.forName(fieldtype);
            Object obj = c.newInstance();
            BeanUtils.setProperty(baseObj, fieldname, obj);
            return obj;
        } else {
            return fieldObj;
        }
    }
}

