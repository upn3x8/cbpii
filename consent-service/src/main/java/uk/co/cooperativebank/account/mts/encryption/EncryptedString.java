package uk.co.cooperativebank.account.mts.encryption;


/**
 * encrypted String
 *
 * @author Craig Parkinson
 */

public class EncryptedString {

    private String mSourceStr = null;
    private String mEncryptedStr = null;

    /**
     * ctor
     */
    public EncryptedString() {
    }

    /**
     * constructor
     *
     * @param sourceStr the source string to set
     */
    public EncryptedString(String sourceStr) {

        mSourceStr = sourceStr;
    }

    /**
     * Gets the sourceStr
     * @return Returns a String
     */
    public String getSourceStr() {
        return mSourceStr;
    }

    /**
     * returns the source string in upper case
     * @return Returns a String
     */
    public String toUpperCase() {
        if (mSourceStr != null) {
            return mSourceStr.toUpperCase();
        }
        else {
            return "";
        }
    }

    /**
     * Sets the sourceStr
     * @param sourceStr The sourceStr to set
     */
    public void setSourceStr(String sourceStr) {
        mSourceStr = sourceStr;
    }

    /**
     * Gets the encryptedStr
     * @return Returns a String
     */
    public String getEncryptedStr() {
        return mEncryptedStr;
    }
    /**
     * Sets the encryptedStr
     * @param encryptedStr The encryptedStr to set
     */
    public void setEncryptedStr(String encryptedStr) {
        this.mEncryptedStr = encryptedStr;
    }

    /**
     * returns the source string
     * @return the source string
     */
    @Override
    public String toString() {
        return mSourceStr;
    }
}