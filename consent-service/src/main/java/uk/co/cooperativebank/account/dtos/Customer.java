package uk.co.cooperativebank.account.dtos;


import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.expression.spel.ast.Identifier;
import uk.co.cooperativebank.account.mts.framework.DomainObject;

import java.io.Serializable;
import java.util.Date;

/**
 * A customer of the Cooperative bank.
 */
public final class Customer implements DomainObject {

  /**
   * Serialisation UID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * The default identifier assigned to a new {@link Customer}.
   */
  private static final String NEW_CUSTOMER = null;

  private String accountNumber;

  /**
   * The unique {@link Identifier} of this {@link Customer}.
   */
  private Customer.ID identifier;

  private String corporatePersonalIndicator;

  private String corporateName;

  private String title;

  private String initials;

  private String surname;

  private Date dateOfBirth;

  private String forename;

  private String middleName;

  private String natCode;

  private String emailAddress;

  private Date lastAccessTime;

  private String homePhone;

  private String mobilePhone;

  private String workPhone;

  private String workPhoneExtn;

  private String expiryDate;

  private String extBlock;

  private String digitalCustomerId;

  /**
   * Constructors No arguments constructor. Creates an Address business object
   * and a CustomerID business object.
   */
  /**
   * Instantiate a {@link Customer} with default values.
   */
  public Customer() {
    identifier = new ID(NEW_CUSTOMER);
  }

  /**
   * Constructor - sets up a Customer.
   *
   * @param customerID     the CustomerID object which holds the 9 digit customer
   *                       identifier(numeric)
   * @param title          the 6 byte title string
   * @param initials       the 5 byte initials string
   * @param surname        the 24 bytes surname string
   * @param forename       the 15 bytes forename string
   * @param middleName     the 15 bytes middle name string
   * @param natCode        the 2 bytes nationality string
   * @param emailAddress   the 70 bytes email id string
   * @param lastAccessTime the Date object
   * @param expiryDate
   */
  public Customer(String accountNumber,
      Customer.ID customerID,
      String title,
      String initials,
      String surname,
      String forename,
      String middleName,
      String natCode,
      String emailAddress,
      Date lastAccessTime,
      String expiryDate) {

    this.accountNumber = accountNumber;
    this.identifier = customerID;
    this.title = title;
    this.initials = initials;
    this.surname = surname;
    this.forename = forename;
    this.middleName = middleName;
    this.natCode = natCode;
    this.emailAddress = emailAddress;
    this.lastAccessTime = lastAccessTime;
    this.expiryDate = expiryDate;
  }


  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  /**
   * Properties
   * <p>
   * /
   * <p>
   * *
   *
   * @return the identifier for this {@link Customer}
   */
  public final Customer.ID getIdentifier() {
    return identifier;
  }

  /**
   * Assign a new identifier to this {@link Customer}.
   *
   * @param identifier the new identifier to be assigned
   */
  public final void setIdentifier(ID identifier) {
    this.identifier = identifier;
  }

  public final String getTitle() {
    return title;
  }

  public final void setTitle(String title) {
    this.title = title;
  }

  public final String getInitials() {
    return initials;
  }

  public final void setInitials(String initials) {
    this.initials = initials;
  }

  public final String getSurname() {
    return surname;
  }

  public final void setSurname(String surname) {
    this.surname = surname;
  }

  public final String getForename() {
    return forename;
  }

  public final void setForename(String forename) {
    this.forename = forename;
  }

  public final String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public final String getName() {
    return title + " " + forename + " " + surname;
  }

  /**
   * Gets the dateOfBirth
   *
   * @return Returns a Date
   */
  public final Date getDateOfBirth() {
    return dateOfBirth;
  }

  /**
   * Sets the dateOfBirth
   *
   * @param dateOfBirth The dateOfBirth to set
   */
  public final void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  /**
   * Gets the lastAccessTime. lastAccessTime is a Date object.
   *
   * @return Returns a Date
   */
  public final Date getLastAccessTime() {
    return lastAccessTime;
  }

  /**
   * Sets the lastAccessTime
   *
   * @param lastAccessTime The lastAccessTime to set
   */
  public final void setLastAccessTime(Date lastAccessTime) {
    this.lastAccessTime = lastAccessTime;
  }

  /**
   * Gets the emailAddress. emailAddress is a 70 byte string.
   *
   * @return Returns a String
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * Sets the emailAddress
   *
   * @param emailAddress The emailAddress to set
   */
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  /**
   * Gets the nationality code
   *
   * @return Returns a String
   */
  public final String getNatCode() {
    return natCode;
  }

  /**
   * Sets the nationality code
   *
   * @param natCode The natCode to set
   */
  public final void setNatCode(String natCode) {
    this.natCode = natCode;
  }

  /**
   * Gets the homePhone
   *
   * @return Returns a String
   */
  public final String getHomePhone() {
    return homePhone;
  }

  /**
   * Sets the homePhone
   *
   * @param homePhone The homePhone string to set
   */
  public final void setHomePhone(String homePhone) {
    this.homePhone = homePhone;
  }

  /**
   * Gets the mobilePhone
   *
   * @return Returns a String
   */
  public final String getMobilePhone() {
    return mobilePhone;
  }

  /**
   * Sets the mobilePhone
   *
   * @param mobilePhone The mobilePhone string to set
   */
  public final void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  /**
   * Gets the workPhone
   *
   * @return Returns a String
   */
  public final String getWorkPhone() {
    return workPhone;
  }


  /**
   * Sets the workPhone
   *
   * @param workPhone The workPhone string to set
   */
  public final void setWorkPhone(String workPhone) {
    this.workPhone = workPhone;
  }

  /**
   * Gets ext_block indicator
   *
   * @return String ext_block
   */
  public final String getExtBlock() {
    return extBlock;
  }

  /**
   * Sets the extBlock
   *
   * @param extBlock the ext block indicator
   */
  public final void setExtBlock(String extBlock) {
    this.extBlock = extBlock;
  }

  /**
   * Gets the workPhoneExtn
   *
   * @return Returns a String
   */
  public final String getWorkPhoneExtn() {
    return workPhoneExtn;
  }

  /**
   * Sets the workPhoneExtn
   *
   * @param workPhoneExtn The workPhoneExtn string to set
   */
  public final void setWorkPhoneExtn(String workPhoneExtn) {
    this.workPhoneExtn = workPhoneExtn;
  }

  /**
   * Gets the corporatePersonalIndicator
   *
   * @return Returns a String
   */
  public final String getCorporatePersonalIndicator() {
    return corporatePersonalIndicator;
  }

  /**
   * Sets the corporatePersonalIndicator
   *
   * @param corporatePersonalIndicator The corporatePersonalIndicator to set
   */
  public final void setCorporatePersonalIndicator(String corporatePersonalIndicator) {
    this.corporatePersonalIndicator = corporatePersonalIndicator;
  }

  /**
   * Gets the corporateName
   *
   * @return Returns a String
   */
  public final String getCorporateName() {
    return corporateName;
  }

  /**
   * Sets the corporateName
   *
   * @param corporateName The corporateName to set
   */
  public final void setCorporateName(String corporateName) {
    this.corporateName = corporateName;
  }

  public final String getExpiryDate() {

    return expiryDate;
  }

  /**
   * @param string
   */
  public final void setExpiryDate(String string) {

    expiryDate = string;
  }


  public String getDigitalCustomerId() {
    return digitalCustomerId;
  }

  public void setDigitalCustomerId(String digitalCustomerId) {
    this.digitalCustomerId = digitalCustomerId;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * The unique {@link Identifier} of a {@link Customer} consisting of a
   * 9-digit numeric code.
   * <p>
   * This type is immutable. If a {@link Customer} is assigned a new
   * identifier for any reason then a new {@link ID} instance should be
   * created.
   */
  public static final class ID implements Serializable {

    /**
     * Serialisation UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The raw identifier value.
     */
    private String id;

    public ID() {

    }

    /**
     * Create a new identifier.
     *
     * @param id the raw identifier value
     */
    public ID(final String id) {
      this.id = id;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this);
    }
  }
}
