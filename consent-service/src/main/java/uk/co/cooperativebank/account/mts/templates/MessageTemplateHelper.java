package uk.co.cooperativebank.account.mts.templates;


import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.exceptions.AlertReasonDetail;
import uk.co.cooperativebank.account.mts.exceptions.EnterpriseTranslationAlert;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.ENTERPRISE_TRANSALATOR_INIT_FAILED;


/**
 * This is a helper class used by the MessageTemplate service. It is responsible for parsing
 * the XML (In string form) obtained from the configuration service and parsing it to create the
 * actual message templates.
 * The XML is parsed and a HashMap is returned keyed on the message direction - either 'inbound' or
 * 'outbound'.  Each of these keys maps to another HashMap of MessageTemplates keyed on transactionId.
 *
 * The helper class then returns a MessageTemplate object for the given usage name.
 * This helper class is also responsible for refreshing the dao if the configuration service changes,
 * this is done via callbacks and the RefreshListener interface.
 *
 * The structure of the XML file follows, the DTD is contained inside the actual file.
 *
 * <pre>
 * <message-templates>
 *
 *   <message id="findCustomer" direction="inbound" type="simple">
 *       <request-number>0001</request-number>
 *       <arguments-indicator>Y</arguments-indicator>
 *       <length>47</length>
 *       <field>
 *           <name>title</name>
 *           <type>alpha</type>
 *           <length>6</length>
 *           <outpoutLength>12</ouputLength>
 *       </field>
 *        .....
 *
 *   </message>
 *
 *   <message id="findCustomer" direction="outbound" type="simple">
 *       <request-number>0001</request-number>
 *       <arguments-indicator>Y</arguments-indicator>
 *       <length>42</length>
 *       <class>uk.co.cooperativebank.bom.Customer</class>
 *       <field>
 *           <name>customerNumber</name>
 *           <type>String</type>
 *           <length>9</length>
 *       </field>
 *        .....
 *
 *   </message>
 *
 *   <message id="getAccounts" direction="outbound" type="complex">
 *       <request-number>0002</request-number>
 *       <arguments-indicator>Y</arguments-indicator>
 *       <length>23</length>
 *       <field>
 *           <name>title</name>
 *           <type>String</type>
 *           <length>6</length>
 *       </field>
 *        .....
 *       <option>
 *           <value>1</value>
 *           <template>getDomesticAccount</template>
 *       </option>
 *        ......
 *   </message>
 * </message-templates>
 *
 * </pre>
 *
 * <p/>
 * As can be seen from the above structure, there are essentially three different styles
 * of message format, all of which are represented by the same MessageTemplate class. They
 * are: <ul><li>Simple inbound<li>
 *       <li>Simple outbound</li>
 *       <li>Complex outbound</li></ul>
 * <p/>
 * For a simple inbound message the valid field types are : alpha, numeric, money, date, and time.
 * <p/>
 * For an outbound message the valid field types are: String, Date, Money, and Filler.  Filler is
 * a type that indicates to the parsing code that this field on the outbound message is not required
 * for further processing.
 * <p/>
 * The concept of a complex outbound message exists to cater for the case where an outbound message requires
 * different processing based on a field in the message.  A complex outbound message format therefore may
 * contain a number of option elements which are used to determine which simple outbound template to use.
 * Also, this message format should only contain one field element whose type is not Filler (ie it may contain
 * any number of filler fields) and this field should be used as the conditional field on which to base
 * further processing.  The best example of this is with the 'getAccounts' transaction.  In this case the
 * outbound message is parsed until the cpvdCode is found. The value of this field is then compared with the
 * value of each of the options and the corresponding simple MessageTemplate to use is found.
 *
 * <em>Implementation Note</em>
 *  It builds them all up front for speed.
 *
 * @author Xansa
l * @version $Id$
 */
public class MessageTemplateHelper {

    private static final Logger LOG = LoggerFactory.getLogger(MessageTemplateHelper.class);

    public void loadResources(List<String> files) {
        files.forEach(this::loadResources);
    }

    public void loadResources(String file) {
        Document xmlDoc = loadXMLDocument(file);
        if (xmlDoc != null) {
            TemplateVersion templateVersion = buildMessageTemplateVersion(xmlDoc);
            buildMessageTemplateCatalogue(xmlDoc, templateVersion);
        } else {
            LOG.error("XML Schema is null");
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENTERPRISE_TRANSALATOR_INIT_FAILED.getCode(), ENTERPRISE_TRANSALATOR_INIT_FAILED.getDescription()));
        }
    }

    private Document loadXMLDocument(String file) {
        LOG.debug("loadXMLDocument(): Entering...");
        InputStream inputStream = this.getClass().getResourceAsStream(file);
        Document schemaDoc = null;
        try {
            SAXBuilder builder = new SAXBuilder();
            schemaDoc = builder.build(inputStream);
        } catch (JDOMException ex) {
            LOG.error("File is not valid XML, cannot build DOM", ex);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENTERPRISE_TRANSALATOR_INIT_FAILED.getCode(), ENTERPRISE_TRANSALATOR_INIT_FAILED.getDescription()));
        } catch (IOException ex) {
            LOG.error("Message template file not found", ex);
            throw new EnterpriseTranslationAlert(new AlertReasonDetail(ENTERPRISE_TRANSALATOR_INIT_FAILED.getCode(), ENTERPRISE_TRANSALATOR_INIT_FAILED.getDescription()));
        }
        LOG.debug("loadXMLDocument(): Leaving...");
        return schemaDoc;
    }

    /**
     * utility method for parsing the configuration passed to it
     */
    private void buildMessageTemplateCatalogue(Document schemaDoc, TemplateVersion versionNumber) {
        LOG.debug("buildMessageTemplateCatalogue(): Entering...");
        // Handle all messages
        List<Element> messages = schemaDoc.getRootElement().getChildren("message");
        boolean calcLength = false;
        for (Element message : messages) {
            MessageTemplate messageTemplate = new MessageTemplate();

            String id = message.getAttributeValue("id");
            String dir = message.getAttributeValue("direction");
            String type = message.getAttributeValue("type");
            String reqNum = message.getChildText("request-number");
            String argInd = message.getChildText("arguments-indicator");
            String service = message.getChildText("service");
            String serviceType = message.getChildText("type");
            String className = message.getChildText("class");
            String translatorClass = message.getChildText("translator");
            int length = 0;
            if (null != message.getChildText("length")) {
                length = Integer.parseInt(message.getChildText("length"));
            } else {
                calcLength = true;
            }
            messageTemplate.setArgumentsIndicator(argInd);
            messageTemplate.setRequestNumber(reqNum);
            messageTemplate.setService(service);
            messageTemplate.setType(serviceType);
            messageTemplate.setDirection(dir);
            messageTemplate.setMessageType(type);
            messageTemplate.setTranslatorClass(translatorClass);
            messageTemplate.setMessageClass(className);
            messageTemplate.setVersionNumber(versionNumber);
            // Get all the errors
            List<Element> errors = message.getChildren("error");
            Map <String, String> errorList = new HashMap <String,String>();
            for (Element field : errors) {
                errorList.put(field.getChildText("code"), field.getChildText("exception"));
            }
            messageTemplate.setErrors(errorList);
            // Get all the fields
            List<Element> fields = message.getChildren("field");
            List<MessageField> fieldList = new ArrayList<MessageField>();
            for (Element field : fields) {
                MessageField messageField = new MessageField();
                messageField.setName(field.getChildText("name"));
                messageField.setType(field.getChildText("type"));
                messageField.setLength(Integer.parseInt(field.getChildText("length")));
                if (calcLength) {
                    length += messageField.getLength();
                }
                // check if we have an outputLength - this is an optional field specified when the ouput
                // field generated during the encode stage is larger than the input field.
                // am example of this is when we encrypt a field and it ends up double the size of the
                // input field
                String outputLength = field.getChildText("outputLength");
                if (outputLength != null) {
                    messageField.setOutputLength(Integer.parseInt(outputLength));
                } else {
                    messageField.setOutputLength(messageField.getLength());
                }
                fieldList.add(messageField);
            }
            LOG.debug("Total of message is:" + length);
            messageTemplate.setLength(length);
            messageTemplate.setMessageFields(fieldList);
            // Get the option fields if present
            List<Element> options = message.getChildren("option");
            Map<String,String> optionMap = new HashMap<String,String>();
            for (Element option : options) {
                String value = option.getChildText("value");
                String template = option.getChildText("template");
                optionMap.put(value, template);
            }
            messageTemplate.setOptions(optionMap);
            if ("inbound".equals(dir)) {
                EnterpriseServiceTemplates.addInboundTemplate(id, reqNum, messageTemplate);
            } else {
                EnterpriseServiceTemplates.addOutboundTemplate(id, reqNum, messageTemplate);
            }
        }
        LOG.debug("buildMessageTemplateCatalogue(): Leaving...");
    }

    private TemplateVersion buildMessageTemplateVersion(Document schemaDoc) {
        LOG.debug("buildMessageTemplateVersion(): Entering...");
        TemplateVersion newTemplateVersion = new TemplateVersion();
        List<Element> messages = schemaDoc.getRootElement().getChildren("interface-version");
        for (Element message : messages) {
            newTemplateVersion.setMajor(message.getChildText("major"));
            newTemplateVersion.setMinor(message.getChildText("minor"));
        }
        LOG.debug("buildMessageTemplateVersion(): Leaving...");
        return newTemplateVersion;
    }
}