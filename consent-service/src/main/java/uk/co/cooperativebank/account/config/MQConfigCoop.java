package uk.co.cooperativebank.account.config;

import com.rabbitmq.jms.admin.RMQDestination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiTemplate;

import javax.jms.Queue;
import javax.naming.NamingException;

@Slf4j
@Configuration
public class MQConfigCoop {

  private final Environment env;

  public MQConfigCoop(Environment env) {
    this.env = env;
  }

  // local reply queue Coop
  @Profile("local")
  @Bean("replyQueue")
  public Queue replyQueue() {
    return new RMQDestination("replyQueue", "", "replyQ", "replyQ");
  }

  // WAS reply queue Coop
  @Profile("!local")
  @Bean("replyQueue")
  public Queue getJndireplyQueue() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.reply.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (Queue) new JndiTemplate().lookup(jndiLookupName);
  }

  // local account enq. queue Coop
  @Profile("local")
  @Bean("accountEnquiryQueue")
  public Queue getLocalAccountEnquiryQueue() {
    return new RMQDestination("accountEnquiry", "", "accountEnquiry", "accountEnquiry");
  }

  // WAS account enq. queue Coop
  @Profile("!local")
  @Bean("accountEnquiryQueue")
  public Queue accountEnquiryQueue() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.account-enq.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (Queue) new JndiTemplate().lookup(jndiLookupName);
  }

  // local customer enq. queue - Coop
  @Profile("local")
  @Bean("customerEnquiryQueue")
  Queue getLocalCustomerEnquiryQueue() {
    return new RMQDestination("customerEnquiryQ", "", "customerEnquiryQ", "customerEnquiryQ");
  }

  // WAS customer enq. queue - Coop
  @Profile("!local")
  @Bean("customerEnquiryQueue")
  public Queue getCustomerEnquiryQueue() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.customer-enq.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (Queue) new JndiTemplate().lookup(jndiLookupName);
  }

}