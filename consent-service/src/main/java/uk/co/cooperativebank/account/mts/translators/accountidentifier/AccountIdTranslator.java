package uk.co.cooperativebank.account.mts.translators.accountidentifier;

import uk.co.cooperativebank.account.mts.dao.account.CreditCardAccount;
import uk.co.cooperativebank.account.mts.dao.account.DomesticAccount;
import uk.co.cooperativebank.account.mts.dao.account.MortgageAccount;
import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;
import uk.co.cooperativebank.account.mts.translators.AbstractHostTranslator;

/**
 *
 * @author Digital-Dev
 */

public class AccountIdTranslator extends AbstractHostTranslator {

    /**
     *
     */
    public AccountIdTranslator() {
        super();

    }

    /**
     * Encodes the key
     * @param obj the data object
     * @param inlength the length of the field.
     * @param outlength the output length
     * @return a string for Percent
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public String encode(Object obj, int inlength, int outlength) throws HostFieldFormatException {
        if (obj == null) {
            return Utilities.getHostNull(outlength);
        }
        String encodeValue;
        if(obj instanceof CreditCardAccount.ID) {
            CreditCardAccount.ID val = (CreditCardAccount.ID) obj;
            encodeValue = val.fullIdentifierValue();
        }else if (obj instanceof MortgageAccount.ID){
            MortgageAccount.ID val = (MortgageAccount.ID) obj;
            encodeValue = val.fullIdentifierValue();
        }else if (obj instanceof DomesticAccount.ID){
            DomesticAccount.ID val = (DomesticAccount.ID) obj;
            encodeValue = val.fullIdentifierValue();
        }else{
            throw new HostFieldFormatException("Unknown Account ID type");
        }

        return encodeValue;

    }

    /**
     * Decodes the key
     *
     * @param str - the string to create the data object
     * @return Object - the decoded object
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public Object decode(String str) throws HostFieldFormatException {
        //this should not be used for decode purpose. For decode use specific account type.
        throw new HostFieldFormatException("AccountIdTranslator is not for Decode Purpose");

    }

}

