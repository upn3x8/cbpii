package uk.co.cooperativebank.account.gateway;

import uk.co.cooperativebank.account.mts.gateway.EnterpriseService;

public enum CustomerEnterpriseServices  implements EnterpriseService {
    findCustomerByDigitalId,
    findCustomerByAccount,
    createDigitalCustomer;

    @Override
    public String getName() {
        return name();
    }
}
