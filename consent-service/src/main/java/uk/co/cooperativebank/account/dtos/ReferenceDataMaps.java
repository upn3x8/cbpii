package uk.co.cooperativebank.account.dtos;

import uk.co.cooperativebank.account.mts.dao.product.DigitalProductCategory;
import uk.co.cooperativebank.account.mts.dao.product.Product;

import java.util.HashMap;
import java.util.Map;

public class ReferenceDataMaps {
    private Map <String, Product> productMap = new HashMap <>();
    private Map <String, DigitalProductCategory> categoryMap = new HashMap<>();

    public Map <String, Product> getProductMap() {
        return productMap;
    }

    public Map <String, DigitalProductCategory> getCategoryMap() {
        return categoryMap;
    }
}
