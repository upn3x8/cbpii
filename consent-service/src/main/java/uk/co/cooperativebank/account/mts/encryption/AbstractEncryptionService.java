package uk.co.cooperativebank.account.mts.encryption;

import uk.co.cooperativebank.account.mts.exceptions.EncryptionAlert;

/**
 * Abstract class that all encryption services should extend.
 *
 * @author Craig Parkinson
 */
public abstract class AbstractEncryptionService implements EncryptionService {

  /**
   * validates an encryption - should be overloaded with specific validation
   * criteria for encryption.
   * routine
   *
   * @param source the input string
   * @param output the encrypted string
   * @return true if successfully validated
   * @throws EncryptionAlert an encryption exception
   */
  protected abstract boolean validateEncrypt(String source, String output) throws EncryptionAlert;

  /**
   * validates a decryption - should be overloaded with specific validation criteria for
   * encryption routine.
   *
   * @param source the input string
   * @param output the encrypted string
   * @return true if successfully validated
   * @throws EncryptionAlert an encryption exception
   */
  protected abstract boolean validateDecrypt(String source, String output) throws EncryptionAlert;

}