package uk.co.cooperativebank.account.gateway;

import uk.co.cooperativebank.account.mts.gateway.EnterpriseService;

public enum BtrEnterpriseServices  implements EnterpriseService {
        getAccount,
        getBalances;

        public String getName() {
            return name();
        }

}
