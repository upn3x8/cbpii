package uk.co.cooperativebank.account.mts.dao.headers;

public class TechnicalServiceEnquiryResponse extends BasicResponseHeader {

    private String numberReturned;
    private String restartKey;

    /**
     * Gets the restartKey
     * @return Returns a String
     */
    public String getRestartKey() {
        return restartKey;
    }
    /**
     * Sets the restartKey
     * @param restartKey The restartKey to set
     */
    public void setRestartKey(String restartKey) {
        this.restartKey = restartKey;
    }

    /**
     * Gets the numberReturned
     * @return Returns a String
     */
    public String getNumberReturned() {
        return numberReturned;
    }
    /**
     * Sets the numberReturned
     * @param numberReturned The numberReturned to set
     */
    public void setNumberReturned(String numberReturned) {
        this.numberReturned = numberReturned;
    }

}