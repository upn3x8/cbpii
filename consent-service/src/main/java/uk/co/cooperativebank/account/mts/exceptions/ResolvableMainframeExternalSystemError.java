package uk.co.cooperativebank.account.mts.exceptions;


import static uk.co.cooperativebank.account.mts.exceptions.MainframeApplicationErrorCode.resolveMainframeApplicationErrorCodeToToken;

/**
 * Represents an application error reported by the mainframe which is treated as
 * a system error by the mid-tier. The mid-tier is able to resolve the error code
 * to an error token for reporting.
 *
 *
 * @author Digital-Dev
 */
public class ResolvableMainframeExternalSystemError extends MainframeExternalSystemError {

    private static final String ERROR_MESSAGE = "Mainframe failed with error code: %1s, mid-tier mapped to token: %2s. Treating as system error";

    public ResolvableMainframeExternalSystemError(String errorCode) {
        super(String.format(ERROR_MESSAGE, errorCode, resolveMainframeApplicationErrorCodeToToken(errorCode)));
    }
}