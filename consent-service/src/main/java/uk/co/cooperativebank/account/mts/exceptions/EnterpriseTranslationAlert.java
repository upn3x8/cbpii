package uk.co.cooperativebank.account.mts.exceptions;

public class EnterpriseTranslationAlert extends AlertingException {

    private final AlertReasonDetail alertReasonDetail;

    /**
     * Constructor for the class
     *
     * @param alertReasonDetail the error code for the exception
     */
    public EnterpriseTranslationAlert(AlertReasonDetail alertReasonDetail) {
        this.alertReasonDetail = alertReasonDetail;
    }

    @Override
    public String getAlertID() {
        return this.getClass().getSimpleName();
    }

    @Override
    public AlertReasonDetail getAlertReasonDetail() {
        return alertReasonDetail;
    }
}
