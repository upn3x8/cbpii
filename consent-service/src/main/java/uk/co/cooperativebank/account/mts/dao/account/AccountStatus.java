package uk.co.cooperativebank.account.mts.dao.account;

/**
 *
 * @author Digital-Dev
 */
public enum AccountStatus {

    ACCOUNT_ACTIVE("00"),// (common for both VISA and DOMESTIC)
    DOMESTIC_ACCOUNT_BLOCKED("01"),// (Note – Visa blocked accounts are filtered at M/F)
    ACCOUNT_UNAVAILABLE("02"),// (common for both VISA and DOMESTIC)
    VISA_IN_ARREARS("03"),
    VISA_OVER_LIMIT("04"),
    VISA_IN_ARREARS_AND_OVER_LIMIT("05"),
    VISA_NO_AUTHORISATION("07"),
    VISA_PENDING("09"),
    VISA_REVOKED("10"),
    VISA_FROZEN("11"),
    VIS_NO_INTEREST("12"),
    VISA_2ND_CARD("16"),
    DOMESTIC_ACCOUNT_CHILD("17"),
    UNKNOWN_ACCOUNT_STATUS(null);

    private final String accountStatusCode;

    AccountStatus(String accountStatusCode) {
        this.accountStatusCode = accountStatusCode;
    }

    public String getAccountStatusCode() {
        return this.accountStatusCode;
    }

    public static AccountStatus getAccountStatusForCode(String accountStatusCode) {

        if (accountStatusCode == null) {
            return UNKNOWN_ACCOUNT_STATUS;
        }

        for (AccountStatus accountStatus : AccountStatus.values()) {
            if (accountStatusCode.equals(accountStatus.getAccountStatusCode())) {
                return accountStatus;
            }
        }

        return UNKNOWN_ACCOUNT_STATUS;

    }

}
