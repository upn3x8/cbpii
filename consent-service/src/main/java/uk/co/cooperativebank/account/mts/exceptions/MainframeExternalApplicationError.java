package uk.co.cooperativebank.account.mts.exceptions;

public abstract class MainframeExternalApplicationError extends ExternalApplicationError {

    public MainframeExternalApplicationError(String errorToken) {
        super(errorToken);
    }

    public void substituteErrorToken(String errorToken) {
        super.setErrorToken(errorToken);
    }
}