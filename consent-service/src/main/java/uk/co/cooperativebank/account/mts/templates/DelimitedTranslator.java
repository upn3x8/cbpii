package uk.co.cooperativebank.account.mts.templates;

public interface DelimitedTranslator {

    /**
     * decodes the raw text and puts the result into the given value object.
     * @param rawText The text string to be decoded
     * @param obj A value object with Bean compliant accessors methods
     */
    void decode(String rawText, Object obj);
}
