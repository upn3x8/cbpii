package uk.co.cooperativebank.account.mts.contexts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.exceptions.AlertReasonDetail;
import uk.co.cooperativebank.account.mts.exceptions.ConfigurationAlert;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.RESOURCE_NOT_FOUND;

public class PropertyUtils {

    private PropertyUtils() {

    }

    private static final Logger LOG = LoggerFactory.getLogger(PropertyUtils.class);

    /**
     * load the properties from .properties file and return Properties.
     *
     * @param clazz
     * @param fileName
     * @return Properties loaded from the resource file
     */
    public static Properties loadProperties(Class clazz, String fileName) {

        Properties properties = new Properties();
        try {
            InputStream resourceAsStream = clazz.getClassLoader().getResourceAsStream(fileName);
            if (resourceAsStream == null) {
                LOG.error("Unable to load resource " + fileName);
                throw new ConfigurationAlert(new AlertReasonDetail(RESOURCE_NOT_FOUND.getCode(), RESOURCE_NOT_FOUND.getDescription()));
            }
            properties.load(resourceAsStream);
        } catch (IOException iOException) {
            LOG.error("Unable to load resource " + fileName + "exception:" + iOException.getMessage(), iOException);
            throw new ConfigurationAlert(new AlertReasonDetail(RESOURCE_NOT_FOUND.getCode(), RESOURCE_NOT_FOUND.getDescription()));
        }

        return properties;
    }

    public static String convertNulls(String stringValue) {
        if (stringValue == null) {
            return "";
        } else {
            return stringValue;
        }
    }

    public static String convertNulls(String stringValue, String defaultValue) {
        if (stringValue == null) {
            return defaultValue;
        } else {
            return stringValue;
        }
    }

    /**
     *
     * @param stringValue
     * @return return true if string parameter is not set to null or contains a
     * blank string
     */
    public static boolean isStringValueSet(String stringValue) {
        if (stringValue == null) {
            return false;
        } else if ("".equals(stringValue.trim())) {
            return false;
        } else {
            return true;
        }
    }

}

