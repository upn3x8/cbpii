package uk.co.cooperativebank.account.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import uk.co.cooperativebank.account.dtos.ReferenceDataMaps;
import uk.co.cooperativebank.account.gateway.GetAccountsTranslation;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceManager;
import uk.co.cooperativebank.account.mts.templates.EnterpriseServiceTemplates;
import uk.co.cooperativebank.account.mts.templates.TranslatorResolver;
import uk.co.cooperativebank.account.mts.translators.CommonTranslatorResolver;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslation;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslationConfig;
import uk.co.cooperativebank.account.mts.translators.HostTranslatorFactory;
import uk.co.cooperativebank.account.mts.translators.accountidentifier.AccountTranslatorResolver;

@Configuration
public class CoDecConfig {

    @Autowired
    HostTranslatorFactory hostTranslatorFactory;

    @Autowired
    EnterpriseTranslationConfig coopEnterpriseTranslationConfig;

    @Autowired
    ReferenceDataMaps referenceDataMaps;

    @Bean
    public EnterpriseTranslationConfig coopEnterpriseTranslationConfig() {
        return new CoopEnterpriseTranslationConfig();
    }


    @Bean("translators")
    public TranslatorResolver[] translatorResolvers(){
        TranslatorResolver[] trans = new TranslatorResolver[2];
        trans[0] = new CommonTranslatorResolver();
        trans[1] = new AccountTranslatorResolver();
        return trans;
    }

    @Bean
    @DependsOn("translators")
    public HostTranslatorFactory hostTranslatorFactory() {
        return new HostTranslatorFactory(translatorResolvers());
    }

    @Bean
    public EnterpriseTranslation hostTemplateTransformerBean() {
        return new GetAccountsTranslation(referenceDataMaps, hostTranslatorFactory, coopEnterpriseTranslationConfig);
    }

    @Bean
    public EnterpriseServiceManager enterpriseServiceManager(){
        return new EnterpriseServiceManager();
    }

    @Bean
    EnterpriseServiceTemplates enterpriseServiceTemplates() {
        return new EnterpriseServiceTemplates();
    }

}