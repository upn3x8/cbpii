package uk.co.cooperativebank.account.mts.templates;

import java.io.Serializable;

public class TemplateVersion implements Serializable {
    private String major;
    private String minor;

    /**
     * @return - the majorNumber version number
     */
    public String getMajor() {
        return major;
    }

    /**
     * @return - the minor version number
     */
    public String getMinor() {
        return minor;
    }

    /**
     * @param major - the major version number
     */
    public void setMajor(String major) {
        this.major = major;
    }

    /**
     * @param minor - the minor version number
     */
    public void setMinor(String minor) {
        this.minor = minor;
    }
}
