package uk.co.cooperativebank.account.mts.dao.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import uk.co.cooperativebank.account.mts.dao.product.Product;
import uk.co.cooperativebank.account.mts.framework.Identifier;
import uk.co.cooperativebank.consent.service.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;

import static uk.co.cooperativebank.account.mts.templates.utilities.SensitivePropertyUtil.reflectObjectToStringExcludingSensitiveProperties;

/**
 * An {@link Account} that is opened by a
 * Customer based on a
 * {@link Product} that is offered by the Cooperative bank.
 * <p>
 * Specifically a domestic account is an account that is both opened with, and
 * provided by Cooperative bank.
 *
 * @see CurrentAccount
 * @see SavingsAccount
 * @see LoanAccount
 */
@Getter
@Setter
public abstract class DomesticAccount extends Account<DomesticAccount> {

  private static final long serialVersionUID = 1L;
  /**
   * The sum of money that is available to be withdrawn.
   * <p>
   * Typically this is any remaining credit balance combined with the total
   * agreed overdraft facility.
   */
  protected BigDecimal availableBalance;
  private BigDecimal forecastClearedBalanceWithAuthorisation;
  private String preNotificationMsg1;
  private String preNotificationMsg2;
  private String preNotificationMsg3;
  private String ibanNumber;
  private String bicNumber;
  private Date lastStatementDate;
  /**
   * The agreed maximum overdraft on the {@link CurrentAccount}.
   */
  private BigDecimal overdraftLimit;
  private BigDecimal lastStatementBalance;
  private BigDecimal currentClearedBalance;
  private BigDecimal currentUnclearedBalance;
  private BigDecimal forecastClearedBalance;
  private BigDecimal forecastUnclearedBalance;
  private String smallValueMarker;
  private String forecastClearedWithAuthBalanceFlag;

  public abstract BigDecimal getAvailableBalance();

  public boolean canUseForecastClearedBalanceWithAuthorisationAsAvailableBalance() {
    return "Y".equalsIgnoreCase(forecastClearedWithAuthBalanceFlag);
  }

  protected BigDecimal calculateAvailableBalance() {
    BigDecimal availableBalance;

    if (canUseForecastClearedBalanceWithAuthorisationAsAvailableBalance()) {
      availableBalance = getForecastClearedBalanceWithAuthorisation().add(getOverdraftLimit());
    } else {
      availableBalance = getForecastClearedBalance().add(getOverdraftLimit());
    }

    return availableBalance;
  }

  public DomesticAccount.ID getAccountID() {
    return (DomesticAccount.ID) getIdentifier();
  }

  private void initialiseDerivedFields() {
    initialiseDomesticAccountSpecificDerivedFields();
  }

  protected abstract void initialiseDomesticAccountSpecificDerivedFields();

  @Override
  public VolatileAccountData getVolatileAccountData() {
    VolatileAccountData volatileData = new VolatileAccountData();
    volatileData.setForecastClearedBalanceWithAuthorisation(getForecastClearedBalanceWithAuthorisation());
    volatileData.setForecastClearedBalance(getForecastClearedBalance());
    volatileData.setForecastUnclearedBalance(getForecastUnclearedBalance());
    volatileData.setLastStatementBalance(getLastStatementBalance());
    volatileData.setOverdraftLimit(getOverdraftLimit());
    volatileData.setCurrentClearedBalance(getCurrentClearedBalance());
    volatileData.setCurrentUnclearedBalance(getCurrentUnclearedBalance());
    return volatileData;
  }

  @Override
  public void clearVolatileAccountData() {
    setForecastClearedBalance(null);
    setForecastClearedBalanceWithAuthorisation(null);
    setForecastUnclearedBalance(null);
    setLastStatementBalance(null);
    setCurrentClearedBalance(null);
    setCurrentUnclearedBalance(null);
    setOverdraftLimit(null);
  }

  @Override
  public void addVolatileAccountData(VolatileAccountData volatileAccountData) {
    setForecastClearedBalance(volatileAccountData.getForecastClearedBalance());
    setForecastClearedBalanceWithAuthorisation(volatileAccountData.getForecastClearedBalanceWithAuthorisation());
    setForecastUnclearedBalance(volatileAccountData.getForecastUnclearedBalance());
    setLastStatementBalance(volatileAccountData.getLastStatementBalance());
    setCurrentClearedBalance(volatileAccountData.getCurrentClearedBalance());
    setCurrentUnclearedBalance(volatileAccountData.getCurrentUnclearedBalance());
    setOverdraftLimit(volatileAccountData.getOverdraftLimit());
  }

  @Override
  public void prepareForPresentation() {
    String delimiterToRemove = ": ";
    setIbanNumber(removePrefix(getIbanNumber(), "IBAN", delimiterToRemove));
    setBicNumber(removePrefix(getBicNumber(), "BIC", delimiterToRemove));

    initialiseDerivedFields();
  }

  @Override
  public boolean isAccountVolatileDataAvailable() {
    return true;
  }

  private String removePrefix(String operand, String prefixToRemove, String delimiterToRemove) {
    if (StringUtils.isNotBlank(operand)
        && StringUtils.startsWithIgnoreCase(operand, prefixToRemove)) {
      return StringUtils.strip(
          StringUtils.removeStartIgnoreCase(operand, prefixToRemove),
          delimiterToRemove);
    }

    return operand;
  }

  /**
   * The unique {@link Identifier} of a {@link DomesticAccount}.
   * <p>
   * The identifier is a 16-digit numeric code which breaks down in to the
   * following components:
   * <ul>
   * <li>6-digit sort code</li>>
   * <li>8-digit account number</li>
   * <li>2-digit account type</li>
   * </ul>
   * <p>
   * This type is immutable. If a {@link DomesticAccount} is assigned a new
   * identifier for any reason then a new {@link DomesticAccount.ID} instance
   * should be created.
   */
  @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "_domestictype")
  @JsonSubTypes( {
      @JsonSubTypes.Type(value = CurrentAccount.ID.class, name = "Current"),
      @JsonSubTypes.Type(value = SavingsAccount.ID.class, name = "Savings"),
      @JsonSubTypes.Type(value = LoanAccount.ID.class, name = "Loan"),
      @JsonSubTypes.Type(value = IndividualSavingsAccount.ID.class, name = "ISA"),
      @JsonSubTypes.Type(value = FixedTermDeposit.ID.class, name = "FixedTermDeposit")
  })
  public static class ID implements Account.ID<DomesticAccount> {

    /**
     * Serialisation UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The sort code component of the identifier.
     */
    @NotNull
    @Pattern(regexp = "\\d{6}")
    private final String sortCode;

    /**
     * The account number component of the identifier.
     */
    @NotNull
    @Pattern(regexp = "\\d{8}")
    private final String accountNumber;

    /**
     * The account type component of the identifier.
     */
    //TODO: need to investigate if an enum is more suitable here.
    @NotNull
    @Pattern(regexp = "\\d{2}")
    private final String accountType;

    /**
     * Default constructor required by Jackson for (de)serialization.
     */
    public ID() {
      this.sortCode = null;
      this.accountNumber = null;
      this.accountType = null;
    }

    public ID(String identifier) {

      this.sortCode = identifier.substring(0, Constants.SORT_CODE_INDEX);
      this.accountNumber = identifier.substring(Constants.SORT_CODE_INDEX, Constants.ACCOUNT_NUMBER_INDEX);
      this.accountType = identifier.substring(Constants.ACCOUNT_NUMBER_INDEX);
    }

    @JsonCreator
    public ID(@JsonProperty("sortCode") final String sortCode,
              @JsonProperty("accountNumber") final String accountNumber,
              @JsonProperty("accountType") final String accountType) {
      this.sortCode = sortCode;
      this.accountNumber = accountNumber;
      this.accountType = accountType;
    }

    public String getSortCode() {
      return sortCode;
    }

    public String getAccountNumber() {
      return accountNumber;
    }

    public String getAccountType() {
      return accountType;
    }

    @JsonIgnore
    public String getFullIdentifierValue() {
      return fullIdentifierValue();
    }

    @Override
    public String fullIdentifierValue() {
      return getSortCode() + getAccountNumber() + getAccountType();
    }

    @Override
    public boolean equals(Object obj) {
      return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
      return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
      return reflectObjectToStringExcludingSensitiveProperties(this);
    }

    @Override
    public String fullIdentifierValueWithTypeZeros() {
      return getSortCode() + getAccountNumber() + ID.ZEROS_ACCOUNT_TYPE;
    }
  }
}
