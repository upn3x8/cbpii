package uk.co.cooperativebank.account.mts.dao.address;

import uk.co.cooperativebank.account.mts.framework.DomainObject;

import java.util.List;

/**
 *
 * @author Digital-Dev
 */
public class Premises implements DomainObject {

    private static final long serialVersionUID = 1L;

    private List<String> premisesList;

    public List<String> getPremisesList() {
        return premisesList;
    }

    public void setPremisesList(List<String> premisesList) {
        this.premisesList = premisesList;
    }



}
