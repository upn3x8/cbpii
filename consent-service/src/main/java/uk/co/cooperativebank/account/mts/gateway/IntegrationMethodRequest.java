package uk.co.cooperativebank.account.mts.gateway;


import uk.co.cooperativebank.account.mts.contexts.ClientContext;

/**
     * All integration method request objects will implement this interface
     *
     * @author dmartin
     */

    public interface IntegrationMethodRequest extends java.io.Serializable {

        ClientContext getClientContext();
    }
