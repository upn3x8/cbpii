package uk.co.cooperativebank.account.mts.translators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.exceptions.AlertReasonDetail;
import uk.co.cooperativebank.account.mts.exceptions.EnterpriseTranslationAlert;
import uk.co.cooperativebank.account.mts.templates.TranslatorResolver;

import static uk.co.cooperativebank.account.mts.exceptions.AlertReasonCode.TRANSLATOR_NOT_FOUND;


/**
 * Gives out <code>MessageTemplate</code> objects from a specified key.
 *
 * @author Craig Parkinson
 */
//@Component TODO - REmove code
public class HostTranslatorFactory {

    private final TranslatorResolver[] translatorResolvers;
    private static final Logger LOG = LoggerFactory.getLogger(HostTranslatorFactory.class);

    public HostTranslatorFactory(TranslatorResolver[] translatorResolvers) {
        this.translatorResolvers = translatorResolvers;
    }

    /**
     * Gets the HostTRansaltorInterface object for the passed in key
     *
     * @param type the class type to create
     * @return the host translation object
     */
    public HostTranslatorInterface getHostTranlator(String type) {

        for (TranslatorResolver resolver : translatorResolvers) {
            if (resolver.canResolve(type)) {
                return resolver.getTranslator(type);
            }
        }
        LOG.error("Invalid object type '" + type + "'");
        throw new EnterpriseTranslationAlert(new AlertReasonDetail(TRANSLATOR_NOT_FOUND.getCode(), TRANSLATOR_NOT_FOUND.getDescription()));

    }
}
