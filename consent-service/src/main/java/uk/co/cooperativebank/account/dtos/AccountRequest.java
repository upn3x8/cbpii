package uk.co.cooperativebank.account.dtos;


import uk.co.cooperativebank.account.mts.dao.account.Account;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceRequest;
import uk.co.cooperativebank.account.mts.gateway.ProcessRequest;

import javax.validation.constraints.NotNull;

public class AccountRequest extends ProcessRequest implements EnterpriseServiceRequest {

  @NotNull
  private Account.ID accountID;


  public Account.ID getAccountID() {
    return accountID;
  }

  public void setAccountID(
      Account.ID accountID) {
    this.accountID = accountID;
  }
}