package uk.co.cooperativebank.account.dtos;

public class ReferenceTypeMapping {

    private String type;
    private String alias;
    private String data_class;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getData_class() {
        return data_class;
    }

    public void setData_class(String data_class) {
        this.data_class = data_class;
    }

    public ReferenceTypeMapping(String type, String alias, String data_class) {
        this.type = type;
        this.alias = alias;
        this.data_class = data_class;
    }
}
