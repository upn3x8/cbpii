package uk.co.cooperativebank.account.mts.dao.headers;

import java.io.Serializable;
import java.util.Date;

public class BasicResponseHeader implements Serializable {

    private String type;
    private Date timeLeftClient;
    private Date timeArrivedHost;
    private Date timeLeftHost;
    private Date timeArrivedClient;
    private String requestNumber;
    private String errorCode;
    private String errorMessage;

    /**
     * Gets the type
     * @return Returns a String
     */
    public String getType() {
        return type;
    }
    /**
     * Sets the type
     * @param type The type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the timeLeftClient
     * @return Returns a Date
     */
    public Date getTimeLeftClient() {
        return timeLeftClient;
    }
    /**
     * Sets the timeLeftClient
     * @param timeLeftClient The timeLeftClient to set
     */
    public void setTimeLeftClient(Date timeLeftClient) {
        this.timeLeftClient = timeLeftClient;
    }

    /**
     * Gets the timeArrivedHost
     * @return Returns a Date
     */
    public Date getTimeArrivedHost() {
        return timeArrivedHost;
    }
    /**
     * Sets the timeArrivedHost
     * @param timeArrivedHost The timeArrivedHost to set
     */
    public void setTimeArrivedHost(Date timeArrivedHost) {
        this.timeArrivedHost = timeArrivedHost;
    }

    /**
     * Gets the timeLeftHost
     * @return Returns a Date
     */
    public Date getTimeLeftHost() {
        return timeLeftHost;
    }
    /**
     * Sets the timeLeftHost
     * @param timeLeftHost The timeLeftHost to set
     */
    public void setTimeLeftHost(Date timeLeftHost) {
        this.timeLeftHost = timeLeftHost;
    }

    /**
     * Gets the timeArrivedClient
     * @return Returns a Date
     */
    public Date getTimeArrivedClient() {
        return timeArrivedClient;
    }
    /**
     * Sets the timeArrivedClient
     * @param timeArrivedClient The timeArrivedClient to set
     */
    public void setTimeArrivedClient(Date timeArrivedClient) {
        this.timeArrivedClient = timeArrivedClient;
    }

    /**
     * Gets the requestNumber
     * @return Returns a String
     */
    public String getRequestNumber() {
        return requestNumber;
    }
    /**
     * Sets the requestNumber
     * @param requestNumber The requestNumber to set
     */
    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    /**
     * Gets the errorCode
     * @return Returns a String
     */
    public String getErrorCode() {
        return errorCode;
    }
    /**
     * Sets the errorCode
     * @param errorCode The errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the errorMessage
     * @return Returns a String
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    /**
     * Sets the errorMessage
     * @param errorMessage The errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder output = new StringBuilder();
        appendProp(output, "type", type);
        appendProp(output, "timeLeftClient", timeLeftClient);
        appendProp(output, "timeArrivedHost", timeArrivedHost);
        appendProp(output, "timeLeftHost", timeLeftHost);
        appendProp(output, "timeArrivedClient", timeArrivedClient);
        appendProp(output, "requestNumber", requestNumber);
        appendProp(output, "errorCode", errorCode);
        appendProp(output, "errorMessage", errorMessage);
        if (output.length() == 0) {
            output.append("(unassigned)");
        }
        output.insert(0, "ResponseHeader: ");
        return output.toString();
    }

    /**
     * Used as part of toString
     *
     * @param output - String buffer used for output
     * @param name - name of attribute being 'string'-ed
     * @param value - value of attribute being 'string'-ed
     */
    protected final void appendProp(StringBuilder output, String name, Object value) {
        if (value != null) {
            if (output.length() > 0) {
                output.append("; ");
            }
            output.append(name).append("=").append(value);
        }
    }

}
