package uk.co.cooperativebank.account.mts.templates.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Utility class for string manipulation and other utility methods.
 * @author Kevin Rudland
 * @version $Revision$
 */

public class Utilities {

    private Utilities() {
    }

    private static final Logger AUDIT_LOG = LoggerFactory.getLogger("AUDIT");
    private static final Logger LOG = LoggerFactory.getLogger(Utilities.class);

    /**
     * padString creates a fixed length string padded by a specified character.
     * @param contents the main contents of the string
     * @param finalLength the full length of the padded string
     * @param padChar the character to be inserted to fill up the string
     * @return String the padded string
     */
    public static String padStringBefore(
            String contents,
            int finalLength,
            char padChar) {
        String newContents = contents;
        if (null == newContents) {
            newContents = "";
        }
        StringBuilder buffer = new StringBuilder(newContents);
        buffer = buffer.reverse();
        while (buffer.length() < finalLength) {
            buffer.append(padChar);
        }
        return buffer.reverse().toString();
    }

    /**
     * padString creates a fixed length string padded by a specified character.
     * @param contents the main contents of the string
     * @param finalLength the full length of the padded string
     * @param padChar the character to be inserted to fill up the string
     * @return String the padded string
     */
    public static String padStringAfter(
            String contents,
            int finalLength,
            char padChar) {

        String newContents = contents;
        if (null == newContents) {
            newContents = "";
        }
        StringBuilder buffer = new StringBuilder(newContents);
        while (buffer.length() < finalLength) {
            buffer.append(padChar);
        }
        return buffer.toString();
    }

    /**
     * convertDateToString creates a fixed length string padded by a specified character.
     * @param date the date to be converted
     * @return String the date in format YYYY-MM-DD
     */
    public static String convertDateToString(Date date) {
        return convertDateToString(date, "-");
    }

    /**
     * Creates a time string from the passed in date.
     * @param date the date to be converted
     * @return String the time in format HHmmss
     * @see SimpleDateFormat
     */
    public static String convertDateToHostTimeStamp(Date date) {

        StringBuilder buf = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        buf.append(sdf.format(date));

        Calendar cal = new GregorianCalendar();
        cal.setTime(date);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour < 10) {
            buf.append("0");
        }
        buf.append(hour);
        buf.append(":");

        int mins = cal.get(Calendar.MINUTE);
        if (mins < 10) {
            buf.append("0");
        }
        buf.append(mins);
        buf.append(":");

        int secs = cal.get(Calendar.SECOND);
        if (secs < 10) {
            buf.append("0");
        }
        buf.append(secs);
        buf.append(".");

        int millis = cal.get(Calendar.MILLISECOND);
        if (millis > 99) {
            millis = millis / 10;
        }
        if (millis < 10) {
            buf.append("0");
        }
        buf.append(millis);

        return buf.toString();
    }

    /**
     * convertDateToString creates a fixed length string padded by a specified character.
     * @param date the date to be converted
     * @param sep - the seperator to use
     * @return String the date in format YYYY-MM-DD
     */
    public static String convertDateToString(Date date, String sep) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        String year = Integer.toString(calendar.get(Calendar.YEAR));
        String month = Integer.toString(calendar.get(Calendar.MONTH) + 1);
        String day = Integer.toString(calendar.get(Calendar.DATE));

        StringBuilder theDate = new StringBuilder(Utilities.padStringBefore(year, 4, '0'));
        theDate.append(sep);
        theDate.append(Utilities.padStringBefore(month, 2, '0'));
        theDate.append(sep);
        theDate.append(Utilities.padStringBefore(day, 2, '0'));

        return theDate.toString();
    }

    /**
     * convertStringsToDateWithDate - takes a date (with time hopefully) and yaer/month/day
     * and overwrites these things
     *
     * @param date - the date
     * @param year - the new year
     * @param month - the new month (remember scale goes from 0 to 11 where 0 is January)
     * @param day - the day
     * @return Date - the new date
     */
    public static Date convertStringsToDateWithDate(
            Date date,
            int year,
            int month,
            int day) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);

        return cal.getTime();
    }

    /**
     * Takes fields with time and year/month/day
     * and creates a Date
     *
     * @param year - the new year
     * @param month - the new month (remember scale goes from 0 to 11 where 0 is January)
     * @param day - the day
     * @param hour 24-hour clock. E.g., at 10:04:15.250 PM the HOUR_OF_DAY is 22
     * @param minute indicating the minute within the hour. E.g., at 10:04:15.250 PM the MINUTE is 4.
     * @param second indicating the second within the minute. E.g., at 10:04:15.250 PM the SECOND is 15
     * @param millisecond indicating the millisecond within the second. E.g., at 10:04:15.250 PM the MILLISECOND is 250.
     * @return Date - the new date
     */
    public static Date getDate(
            int year,
            int month,
            int day,
            int hour,
            int minute,
            int second,
            int millisecond) {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, millisecond);

        return cal.getTime();
    }

    /**
     * Takes a string field in the format dd/MM/yyyy
     * and creates a Date
     *
     * @param date the date as a String
     * @return Date the real date
     * @throws ParseException -
     */
    public static Date getDate(String date) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return sdf.parse(date);
    }

    /**
     * Takes a string field in the ISO format yyyy-MM-dd
     * and creates a Date
     *
     * @param date the date as a String
     * @return Date the real date
     * @throws ParseException -
     */
    public static Date parseIsoDate(String date) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(date);
    }

    /**
     * Takes a string field in the format yyyy-MM-ddHH:mm:ss.SSS
     * and creates a Date
     *
     * @param date the date as a String
     * @return Date the real date
     * @throws ParseException -
     */
    public static Date parseDateTime(String date) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss.SS");
        return sdf.parse(date);
    }

    /**
     * gets the time from a date via a format
     *
     * @param date - the date to format
     * @return String - the formated date
     */
    public static String getTime(Date date) {
        return new SimpleDateFormat("HH:mm:ss.SSS").format(date);
    }

    /**
     * gets the year from a date
     *
     * @param date - the date to format
     * @return String - the formated date
     */
    public static String getYear(Date date) {
        return formatDate(date, "yyyy");
    }

    /**
     * gets the month from a date
     *
     * @param date - the date to format
     * @return String - the formated date
     */
    public static String getMonth(Date date) {
        return formatDate(date, "MM");
    }

    /**
     * gets the day from a date
     *
     * @param date - the date to format
     * @return String - the formated date
     */
    public static String getDay(Date date) {
        return formatDate(date, "dd");
    }

    /**
     * formats a date ?
     *
     * @param date - the date to format
     * @param format - the desired format
     * @return String - the formated date
     */
    public static String formatDate(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }


    /** Uses log4j to write audit messages
     *
     * @param component the system component which is writing the audit message
     * @param customerNumber the customer Number
     * @param transactionKey the transaction which is currently under way
     * @param cloneId the clone in which the transaction is taking place
     * @param status the status of the transaction e.g. Entering or Leaving
     */
    public static void audit(
            String component,
            String customerNumber,
            String transactionKey,
            String cloneId,
            String status) {

        StringBuilder buf = new StringBuilder("Comp:");
        buf.append(component);
        buf.append(" Cust:");
        buf.append(customerNumber);
        buf.append(" Trans:");
        buf.append(transactionKey);
        buf.append(" Clone:");
        buf.append(cloneId);
        buf.append("    :");
        buf.append(status);

        String message = new String(buf);
        AUDIT_LOG.info(message);

    }

    /** Uses log4j to write audit messages
     *
     * @param component the system component which is writing the audit message
     * @param customerNumber the customer Number
     * @param transactionKey the transaction which is currently under way
     * @param cloneId the clone in which the transaction is taking place
     * @param optionalMessage an additional message String
     * @param status the status of the transaction e.g. Entering or Leaving
     */
    public static void audit(
            String component,
            String customerNumber,
            String transactionKey,
            String cloneId,
            String optionalMessage,
            String status) {

        StringBuilder buf = new StringBuilder("Comp:");
        buf.append(component);
        buf.append(" Cust:");
        buf.append(customerNumber);
        buf.append(" Trans:");
        buf.append(transactionKey);
        buf.append(" Clone:");
        buf.append(cloneId);
        buf.append(" ");
        buf.append(optionalMessage);
        buf.append("    :");
        buf.append(status);

        String message = new String(buf);
        AUDIT_LOG.info(message);

    }

    /**
     * Returns true is the passed in string is contained in the passed in string array
     * @param value the value to check
     * @param array the array to check if the string is present
     * @return true if the passed in string is contained in the string array. false if not.
     */
    public static boolean isContainedIn(String value, String[] array) {
        for (String s : array) {
            if (value.equals(s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Calculates the age from the passed in dateOfBirth
     * @param dateOfBirth the date of Birth
     * @return int - the age of the user in completed years only or
     * negative if the dob entered is in future
     */
    public static int calculateAge(Date dateOfBirth) {

        Calendar today = new GregorianCalendar();
        today.setTime(getJustDate(new Date()));
        Calendar cal = new GregorianCalendar();
        cal.setTime(getJustDate(dateOfBirth));
        int years = today.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
        int months = today.get(Calendar.MONTH) - cal.get(Calendar.MONTH);
        int days = today.get(Calendar.DATE) - cal.get(Calendar.DATE);
        int age = (months < 0 || months == 0 && days < 0) ? years - 1 : years;
        return age;
    }

    /**
     * Calculates the date after adding the passed in years and months to initial date
     * @param date the initial date to which years and months are to be added
     * @param yearsAtAddress to add
     * @param monthsAtAddress to add
     * @return date - the object encapsulating the date after adding years and
     * months to the initial date.
     */
    public static Date calculateDate(
            Date date,
            int yearsAtAddress,
            int monthsAtAddress) {

        Calendar cal = new GregorianCalendar();
        cal.setTime(getJustDate(date));
        cal.add(Calendar.YEAR, yearsAtAddress);
        cal.add(Calendar.MONTH, monthsAtAddress);
        return cal.getTime();
    }

    /**
     * trims the time off a date
     *
     * @param date - date to be trimmed
     * @return Date - the trimmed date
     */
    public static Date getJustDate(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Gets the string of what the host is expecting for a null value.
     * This will be padded to the passed in length.
     * @param length the length to pad to.
     * @return the value the host is expecting
     */
    public static String getHostNull(int length) {
        StringBuilder hostNull= new StringBuilder(new String());
        byte[] b = new byte[1];
        b[0] = -39;
        /* use the encoding as ISO-8859-1 which will send Ù value in payload as expected by mainframe.*/
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(b);
        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(byteInputStream, StandardCharsets.ISO_8859_1));
            String line;
            while((line=in.readLine())!= null){
                hostNull.append(line);
            }
        } catch (UnsupportedEncodingException ex) {
            LOG.error("Encoding is not supported", ex);
        } catch (IOException ex) {
            LOG.error("Unable to read input stream", ex);
        }
        return padStringAfter(hostNull.toString(), length, ' ');
    }

    /**
     * Generate a new transaction token, to be used for enforcing a single
     * request for a particular transaction.
     *
     * @param wrappedToken token and path string
     * @return String the token as a string
     */
    public static String hashTokenWrapper(String wrappedToken) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(wrappedToken.getBytes());
            return toHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            LOG.info("Handled exception", e);
            return null;
        }
    }

    /**
     * Convert a byte array to a String of hexadecimal digits and return it.
     *
     * @param buffer The byte array to be converted
     * @return - String - the conversion
     */
    public static String toHex(byte[] buffer) {

        StringBuilder sb = new StringBuilder();
        String s;
        for (byte b : buffer) {
            s = Integer.toHexString((int) b & 0xff);
            if (s.length() < 2) {
                sb.append('0');
            }
            sb.append(s);
        }
        return sb.toString();

    }

    /**
     * A method which takes a String as input
     * and returns the same String of required length.
     * i.e. if length of the string is lower than required length it simply padded it
     * with padChar, otherwise make a substring of required length and return it.
     * @param str the input string
     * @param lengthRequired the full length of the string required
     * @param padChar the character to be inserted to fill up the string
     * @return String the string of required length.
     */
    public static String extractFixedLengthSubString(
            String str,
            int lengthRequired,
            char padChar) {
        if (str != null && str.length() >= lengthRequired) {
            return str.substring(0, lengthRequired);
        } else {
            return padStringAfter(str, lengthRequired, padChar);
        }
    }

    public static boolean checkAndCompareRetirementDate(String tempDate){
        Date custTempDate;
        Date currentTempDate;
        SimpleDateFormat tempSDF = new SimpleDateFormat("dd-MM-yyyy");
        Calendar currentDate = Calendar.getInstance();
        String dateNow = tempSDF.format(currentDate.getTime());
        try {
            custTempDate = tempSDF.parse(tempDate);
            currentTempDate= tempSDF.parse(dateNow);
        } catch (ParseException pEx){
            return false;
        }
        return custTempDate.equals(currentTempDate) || custTempDate.after(currentTempDate);
    }

    public static boolean isNumberNumeric(String tempStr){
        try {
            Integer.parseInt(tempStr);
        }
        catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }


    /**
     * Calculates the days from the passed in date
     * @param dateObj the date
     * @return int - the days between given date and current date
     *
     */
    public static float calculateDaysFromCurrentDate(Date dateObj) {

        Date today = new Date();
        return (float)(today.getTime()-dateObj.getTime())/(1000*60*60*24);
    }

}