package uk.co.cooperativebank.account.mts.gateway;

import uk.co.cooperativebank.account.mts.framework.DomainObject;

import java.util.List;


public interface EnterpriseServiceCall {
    List<? extends DomainObject> service(EnterpriseGateway.EnterprisePayload payload);

}
