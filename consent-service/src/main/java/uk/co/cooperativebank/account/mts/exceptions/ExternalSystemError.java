package uk.co.cooperativebank.account.mts.exceptions;

public abstract class ExternalSystemError extends ExternalError {

    public ExternalSystemError(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public String toString() {
        return getMessage();
    }
}