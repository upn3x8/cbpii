package uk.co.cooperativebank.account.mts.translators;

import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;

import java.io.Serializable;

/**
 * Class is used as a translator for the class
 * java.util.Date into a Time format for the host
 *
 * @author Ron Tomlinson
 */

public abstract class AbstractHostTranslator implements Serializable, HostTranslatorInterface {

    /**
     * base transaltor
     *
     * @param obj - obje to translate
     * @param clazz -  class to turn it into
     *
     * @throws HostFieldFormatException - when transaltion fails
     */
    protected void assertObjectType(Object obj, Class clazz) throws HostFieldFormatException {
        // check object type
        if (null == obj || !(clazz.isAssignableFrom(obj.getClass()))) {
            throw new HostFieldFormatException("invalid object type expected :" + clazz.getName() + ", got:" + obj);
        }
    }

}