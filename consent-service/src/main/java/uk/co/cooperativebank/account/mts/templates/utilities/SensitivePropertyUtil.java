package uk.co.cooperativebank.account.mts.templates.utilities;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.templates.SensitiveInformation;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Digital-Dev
 */
public class SensitivePropertyUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SensitivePropertyUtil.class);

    private SensitivePropertyUtil() {

    }

    private static void addSensitiveFields(Class clazz, List <String> sensitiveProperties) {
        Field[] fieldList = clazz.getDeclaredFields();

        boolean isSensitiveInformation;

        //Add any of the objects properties that are annotated as being @Sensitive to the sensitiveProperties list
        for (Field field : fieldList) {
            try {
                isSensitiveInformation = field.isAnnotationPresent(SensitiveInformation.class);
                if (isSensitiveInformation) {
                    sensitiveProperties.add(field.getName());
                }
            } catch (ClassFormatError cfe) {
                LOG.info("ClassFormatError while reading isAnnotationPresent", cfe);
            } catch (Exception e) {
                LOG.error("Error while reading isAnnotationPresent", e);
            }
        }

        Class superClass = clazz.getSuperclass();
        if (superClass != null) {
            addSensitiveFields(superClass, sensitiveProperties);
        }
    }

    public static String reflectObjectToStringExcludingSensitiveProperties(Object object) {

        //Nothing to reflect
        if (object == null) {
            return "Null Object";
        }

        List<String> sensitiveProperties = new ArrayList();

        addSensitiveFields(object.getClass(), sensitiveProperties);

        //Use reflection to perform the toString - omitting any sensitive properties
        return new ReflectionToStringBuilder(object).setExcludeFieldNames(sensitiveProperties.toArray(new String[sensitiveProperties.size()])).toString();
    }

    public static boolean isPropertyNameSensitive(Object object, String propertyName) {
        if (object == null) {
            return false;
        }

        //Don't attempt to check properties that refer to sub objects e.g. previousAddress.postCode
        if (propertyName.contains(".")) {
            return false;
        }

        return isPropertyNameSensitiveInClass(object.getClass(), propertyName);
    }

    private static boolean isPropertyNameSensitiveInClass(Class clazz, String propertyName) {

        try {
            Field field = clazz.getDeclaredField(propertyName);
            return field.isAnnotationPresent(SensitiveInformation.class);

        } catch (NoSuchFieldException nsfError) {

            Class superclass = clazz.getSuperclass();
            if (superclass != null) {
                return isPropertyNameSensitiveInClass(superclass, propertyName);
            }
            else {
                //NB. Can occur when decoding host tempalte with properties that don't exist in object
                LOG.debug("No such field name in class - " + propertyName);
                return false;
            }
        } catch (Exception e) {
            //Can cccur in test if there is no test dependency on javax.validation / validation-api
            LOG.error("Error in isPropertyNameSensitive", e);
            return false;
        }
    }

}
