package uk.co.cooperativebank.account.mts.exceptions;

/**
 * EncryptionAlert are thrown if there are problems with the using the
 * EncrptionService service.
 *
 * @author Craig Parkinson
 * @version $Revision$
 *
 */
public class EncryptionAlert extends AlertingException {

    private final AlertReasonDetail alertReasonDetail;

    /**
     * Constructor for the class
     *
     * @param alertReasonDetail the error code for the exception
     */
    public EncryptionAlert(AlertReasonDetail alertReasonDetail) {
        this.alertReasonDetail = alertReasonDetail;
    }

    @Override
    public String getAlertID() {
        return this.getClass().getSimpleName();
    }

    @Override
    public AlertReasonDetail getAlertReasonDetail() {
        return alertReasonDetail;
    }

}
