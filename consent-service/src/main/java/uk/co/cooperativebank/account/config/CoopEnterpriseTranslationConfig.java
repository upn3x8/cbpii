package uk.co.cooperativebank.account.config;

import org.springframework.beans.factory.annotation.Value;
import uk.co.cooperativebank.account.mts.translators.EnterpriseTranslationConfig;

public class CoopEnterpriseTranslationConfig implements EnterpriseTranslationConfig {

    @Value("${mainframe.connection.coop.userid}")
    private String userId;

    @Value("${mainframe.connection.coop.brand}")
    private String brand;

    @Value("${database.digitalcustomer.coop.prefix}")
    private String digitalCustomerIdPrefix;

    public String getUserId() {
        return userId;
    }

    public String getBrand() {
        return brand;
    }

    public String getDigitalCustomerIdPrefix() {
        return digitalCustomerIdPrefix;
    }

}
