package uk.co.cooperativebank.account.mts.dao.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.math.BigDecimal;

@JsonTypeName("FixedTermDeposit")
public class FixedTermDeposit extends DomesticAccount {

    @Override
    public BigDecimal getAvailableBalance() {
        return null; /* Available balance not allowed */
    }

    @Override
    protected void initialiseDomesticAccountSpecificDerivedFields() {
        /* Do nothing*/
    }

    public static class ID extends DomesticAccount.ID {

        public ID() {
            super();
        }

        public ID(String identifier) {
            super(identifier);
        }

        @JsonCreator
        public ID(@JsonProperty("sortCode") final String sortCode,
                  @JsonProperty("accountNumber") final String accountNumber,
                  @JsonProperty("accountType") final String accountType) {
            super(sortCode, accountNumber, accountType);
        }
    }
}