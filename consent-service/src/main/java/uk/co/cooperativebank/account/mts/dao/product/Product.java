package uk.co.cooperativebank.account.mts.dao.product;

import uk.co.cooperativebank.account.mts.dao.account.Account;
import uk.co.cooperativebank.account.mts.framework.DomainObject;

/**
 * A banking {@link Product} offered by the Cooperative bank for
 *
 * {@link Account} from.
 *
 * The {@link Product} defines a template of services to be offered on an
 * {@link Account}.
 */
public class Product implements DomainObject {

    /**
     * Serialisation UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The {@link Category} which this {@link Product} belongs to.
     */
    private Category category;

    /**
     * If this {@link Product} is classed as an e-product or not.
     */
    private boolean eProduct;
    private String productCode;
    private String companyCode;
    private String corpPerInd;
    private String shortDesc;
    private String longDesc;
    private String abbrDesc;
    private String productCat;
    private String sequence;
    private String corporateIdentifier;
    private String companyId;

    public final Category getCategory() {
        return category;
    }

    public final void setCategory(Category category) {
        this.category = category;
    }

    public boolean isEProduct() {
        return eProduct;
    }

    public void setEProduct(boolean eProduct) {
        this.eProduct = eProduct;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCorpPerInd() {
        return corpPerInd;
    }

    public void setCorpPerInd(String corpPerInd) {
        this.corpPerInd = corpPerInd;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public String getAbbrDesc() {
        return abbrDesc;
    }

    public void setAbbrDesc(String abbrDesc) {
        this.abbrDesc = abbrDesc;
    }

    public String getProductCat() {
        return productCat;
    }

    public void setProductCat(String productCat) {
        this.productCat = productCat;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getCorporateIdentifier() {
        return corporateIdentifier;
    }

    public void setCorporateIdentifier(String corporateIdentifier) {
        this.corporateIdentifier = corporateIdentifier;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

}