package uk.co.cooperativebank.account.mts.dao.product;

import org.apache.commons.lang3.StringUtils;

/**
 * The DigitalAccountType of the account.
 */
public enum DigitalAccountType {

    CURRENT("C"),
    SAVINGS("S"),
    LOAN("L"),
    FIXED_TERM_DEPOSIT("F"),
    ISA("I"),
    CREDITCARD("V"),
    MORTGAGE("M");

    private final String code;


    private DigitalAccountType(final String code) {
        this.code  = code;
    }

    public String getName() {
        return code;
    }

    public static DigitalAccountType getDigitalAccountType(String code){

        for (DigitalAccountType digitalAccountType : DigitalAccountType.values()) {
            if (StringUtils.equals(digitalAccountType.getName(), code)) {
                return digitalAccountType;
            }
        }
        throw new IllegalArgumentException("Unrecognised digitalAccountType code: " + code);

    }
}