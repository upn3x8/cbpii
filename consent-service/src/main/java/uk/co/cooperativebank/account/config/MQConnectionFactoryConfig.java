package uk.co.cooperativebank.account.config;

import com.rabbitmq.jms.admin.RMQConnectionFactory;
import com.rabbitmq.jms.admin.RMQDestination;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.jms.JmsHeaderMapper;
import org.springframework.jndi.JndiTemplate;
import org.springframework.messaging.MessageChannel;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.naming.NamingException;

@Slf4j
@EnableIntegration
@Configuration
public class MQConnectionFactoryConfig {

  private final Environment env;

  public MQConnectionFactoryConfig(Environment env) {
    this.env = env;
  }

  // Local connection factory
  @Profile("local")
  @Bean
  public ConnectionFactory connectionFactory() {
    RMQConnectionFactory factory = new RMQConnectionFactory();
    factory.setUsername(env.getProperty("rabbit.mq.username"));
    factory.setPassword(env.getProperty("rabbit.mq.password"));
    factory.setVirtualHost(env.getProperty("rabbit.mq.virtualHost"));
    factory.setHost(env.getProperty("rabbit.mq.host"));
    factory.setPort(env.getProperty("rabbit.mq.port", Integer.class));
    return factory;
  }

  // WAS connection factory
  @Profile("!local")
  @Bean
  public ConnectionFactory getJndiConnectionFactory() throws NamingException {
    String jndiLookupName = env.getProperty("mts.queue.jndi.name");
    log.info("Setting up JNDI datasource for rabbit connection factory, name:{}", jndiLookupName);
    return (ConnectionFactory) new JndiTemplate().lookup(jndiLookupName);
  }

  @Bean
  public JmsHeaderMapper headermapper() {
    return new CustomJmsHeaderMapper();
  }

  @Bean("reply")
  public MessageChannel reply() {
    return new DirectChannel();
  }

  @Bean("replySM")
  public MessageChannel replySM() {
    return new DirectChannel();
  }

  @Bean("replyTransformed")
  public MessageChannel replyTransformed() {
    return new DirectChannel();
  }

  @Bean("replyTransformedSM")
  public MessageChannel replyTransformedSM() {
    return new DirectChannel();
  }
}