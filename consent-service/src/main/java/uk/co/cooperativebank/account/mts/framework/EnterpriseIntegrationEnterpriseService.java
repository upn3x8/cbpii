package uk.co.cooperativebank.account.mts.framework;

import uk.co.cooperativebank.account.mts.gateway.EnterpriseService;

public enum EnterpriseIntegrationEnterpriseService implements EnterpriseService {

        findCustomerByAccount,

        getAccountCorrespondenceData,

        getAccounts,
        getAccount,

        findCustomerByDigitalId,

        getCustomerStandingOrder,
        getAccountStandingOrder,
        getCustomerFundsTransfers,
        getBillPaymentsByCustomer,
        findCustomerByPassNumber,

        createDigitalCustomer;
        @Override
        public String getName() {
            return name();
        }
    }
