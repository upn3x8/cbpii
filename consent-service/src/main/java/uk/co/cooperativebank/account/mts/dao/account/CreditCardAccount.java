package uk.co.cooperativebank.account.mts.dao.account;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.templates.SensitiveInformation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;

import static uk.co.cooperativebank.account.mts.templates.utilities.SensitivePropertyUtil.reflectObjectToStringExcludingSensitiveProperties;

/**
 * An {@link Account} for customers who hold one or more credit cards.
 */
@JsonTypeName("CreditCardAccount")
public class CreditCardAccount extends Account<CreditCardAccount> {

  private static final long serialVersionUID = 1L;

  private static final Logger LOG = LoggerFactory.getLogger(CreditCardAccount.class);

  /**
   * The amount of credit that can still be spent by the customer.
   */
  private BigDecimal availableToSpend;

  /**
   * The current balance
   */
  private BigDecimal currentBalance;

  /**
   * The maximum credit limit that has been agreed for this
   * {@link CreditCardAccount}.
   */
  private BigDecimal creditLimit;

  private BigDecimal fullRepaymentAmount;

  private BigDecimal minimumRepaymentAmount;

  private Date dueDate;

  private Date statementDate;

  private String informationMessage;

  /**
   * 19 digit account number returned from EBS
   */
  private String ebsAccountNumber;

  @SensitiveInformation
  private String unmaskedIdentifier;

  public final BigDecimal getAvailableToSpend() {
    return availableToSpend;
  }

  public final void setAvailableToSpend(BigDecimal availableToSpend) {
    this.availableToSpend = availableToSpend;
  }

  public final BigDecimal getAvailableCredit() {
    return availableToSpend;
  }

  public final void setAvailableCredit(BigDecimal availableToSpend) {
    this.availableToSpend = availableToSpend;
  }

  public final BigDecimal getCreditLimit() {
    return creditLimit;
  }

  public final void setCreditLimit(BigDecimal creditLimit) {
    this.creditLimit = creditLimit;
  }

  public final BigDecimal getCurrentBalance() {
    return currentBalance;

  }

  public final void setCurrentBalance(BigDecimal currentBalance) {
    this.currentBalance = currentBalance;
  }

  public BigDecimal getFullRepaymentAmount() {
    return fullRepaymentAmount;
  }

  public void setFullRepaymentAmount(BigDecimal fullRepaymentAmount) {
    this.fullRepaymentAmount = fullRepaymentAmount;
  }

  public BigDecimal getMinimumRepaymentAmount() {
    return minimumRepaymentAmount;
  }

  public void setMinimumRepaymentAmount(BigDecimal minimumRepaymentAmount) {
    this.minimumRepaymentAmount = minimumRepaymentAmount;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  public Date getStatementDate() {
    return statementDate;
  }

  public void setStatementDate(Date statementDate) {
    this.statementDate = statementDate;
  }

  public String getInformationMessage() {
    informationMessage = evaluateInformationMessageUsingStatus(getStatus());
    return informationMessage;
  }

  public void setInformationMessage(String informationMessage) {
    this.informationMessage = informationMessage;
  }

  public String getUnmaskedIdentifier() {
    return unmaskedIdentifier;
  }

  public void setUnmaskedIdentifier(String unmaskedIdentifier) {
    this.unmaskedIdentifier = unmaskedIdentifier;
  }

  public String getEbsAccountNumber() {
    return ebsAccountNumber;
  }

  public void setEbsAccountNumber(String ebsAccountNumber) {
    this.ebsAccountNumber = ebsAccountNumber;
  }

  @Override
  public VolatileAccountData getVolatileAccountData() {
    VolatileAccountData volatileData = new VolatileAccountData();
    volatileData.setCreditLimit(getCreditLimit());
    volatileData.setCurrentBalance(getCurrentBalance());
    volatileData.setAvailableCredit(getAvailableCredit());
    volatileData.setAvailableToSpend(getAvailableToSpend());
    volatileData.setInformationMessage(getInformationMessage());
    return volatileData;
  }

  @Override
  public void clearVolatileAccountData() {
    setCreditLimit(null);
    setCurrentBalance(null);
    setAvailableCredit(null);
    setInformationMessage(null);
  }

  @Override
  public void addVolatileAccountData(VolatileAccountData volatileAccountData) {
    setCreditLimit(volatileAccountData.getCreditLimit());
    setCurrentBalance(volatileAccountData.getCurrentBalance());
    setAvailableCredit(volatileAccountData.getAvailableCredit());
    setInformationMessage(volatileAccountData.getInformationMessage());
  }

  @Override
  public void prepareForPresentation() {

    if (!isAccountVolatileDataAvailable()) {
      setIsDetailAvailable(false);
      return;
    }

    setCurrentBalance(reverseSign(getCurrentBalance()));
    if (getAvailableCredit().intValue() < 0) {
      setAvailableCredit(new BigDecimal(0));
    }
  }

  @Override
  public boolean isAccountVolatileDataAvailable() {
    return !AccountStatus.ACCOUNT_UNAVAILABLE.equals(getAccountStatus());
  }

  private BigDecimal reverseSign(BigDecimal operand) {

    int signum = operand.signum();
    if (signum == -1) {
      return operand.abs();
    } else if (signum == 1) {
      return operand.negate();
    }

    return operand;
  }

  /**
   * Null means statement is NOT available.
   *
   * @return true when statementDate is not null and false when statementDate
   * is NULL.
   */
  public boolean isStatementAvailable() {

    boolean isStatementAvailable;

    if (statementDate == null) {
      isStatementAvailable = false;
    } else {
      isStatementAvailable = true;
    }

    LOG.debug(String.format("Statement for Account %s is Available ? %s", getIdentifier().fullIdentifierValue(), isStatementAvailable));

    return isStatementAvailable;
  }

  private String evaluateInformationMessageUsingStatus(String status) {
    String message = null;
    if (status != null) {
      if ("03".equals(status)) {
        message = "SingleView.creditCardArrears";
      } else if ("04".equals(status)) {
        message = "SingleView.creditCardOverLimit";
      } else if ("05".equals(status)) {
        message = "SingleView.creditCardArrearsAndOverLimit";
      }
    }
    return message;
  }

  /**
   * The unique {@link Identifier} of a {@link CreditCardAccount}.
   * <p>
   * The expected format is a 16-digit numeric code.
   * <p>
   * This type is immutable. If a {@link CreditCardAccount} is assigned a new
   * identifier for any reason then a new {@link CreditCardAccount.ID}
   * instance should be created.
   */
  public static class ID implements Account.ID<CreditCardAccount> {

    /**
     * Serialisation UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The raw value of the identifier.
     */
    @NotNull
    @Pattern(regexp = "[0-9X]+")
    private final String rawIdValue;

    /**
     * Default constructor required by Jackson for (de)serialization.
     */
    public ID() {
      rawIdValue = null;
    }

    /**
     * Construct a new identifier with the specified account identifier.
     *
     * @param id the 16-digit identifier
     */
    @JsonCreator
    public ID(@JsonProperty("rawIdValue") final String id) {
      this.rawIdValue = id;
    }

    public String getRawIdValue() {
      return rawIdValue;
    }

    @Override
    public String fullIdentifierValue() {
      return getRawIdValue();
    }

    @Override
    public boolean equals(Object obj) {
      return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
      return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String fullIdentifierValueWithTypeZeros() {
      throw new UnsupportedOperationException("No Type for Credit Card");
    }

    @Override
    public final String toString() {
      return reflectObjectToStringExcludingSensitiveProperties(this);
    }
  }
}
