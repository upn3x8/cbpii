package uk.co.cooperativebank.account.mts.dao.account;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.math.BigDecimal;

/**
 * An {@link Account} that has been opened based on a savings
 * {@link uk.co.cooperativebank.digital.dao.Product} that is
 * offered by the Cooperative bank to its
 * {@link Customer}.
 */
@JsonTypeName("SavingsAccount")
public class SavingsAccount extends DomesticAccount {

    /**
     * The sum of money that is available to be withdrawn.
     *
     * Typically this is any remaining credit balance combined with the total
     * agreed overdraft facility.
     */
    private BigDecimal availableBalance;

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public static class ID extends DomesticAccount.ID {

        public ID() {
            super();
        }

        public ID(String identifier) {
            super(identifier);
        }

        @JsonCreator
        public ID(@JsonProperty("sortCode") final String sortCode,
                  @JsonProperty("accountNumber") final String accountNumber,
                  @JsonProperty("accountType") final String accountType) {
            super(sortCode, accountNumber, accountType);
        }
    }

    @Override
    protected void initialiseDomesticAccountSpecificDerivedFields() {
        availableBalance = calculateAvailableBalance();
    }
}