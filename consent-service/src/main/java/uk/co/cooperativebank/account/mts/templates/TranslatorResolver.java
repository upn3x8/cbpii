package uk.co.cooperativebank.account.mts.templates;

import uk.co.cooperativebank.account.mts.translators.HostTranslatorInterface;

/**
 * Interface to provide translators
 * @author Digital-Dev
 */
public interface TranslatorResolver {

    HostTranslatorInterface getTranslator(String type);

    boolean canResolve(String type);
}

