package uk.co.cooperativebank.account.mts.templates;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class EnterpriseServiceTemplates {

    /**
     * to store template for a service number
     */
    private static final Map <String, MessageTemplate> INBOUND_TEMPLATES = new ConcurrentHashMap <>();
    /**
     * to store template for a service number
     */
    private static final Map<String, MessageTemplate> OUTBOUND_TEMPLATES = new ConcurrentHashMap<>();
    /**
     * to store service number for a service name
     */
    private static final Map<String, String> SERVICE_NUMBERS = new ConcurrentHashMap<>();

    private static final String MESSAGE_TEMPLATE_FILE_LOCATION = "/enterpriseintegration-host-templates.xml";

    public EnterpriseServiceTemplates(){

    }

    public void loadCommonTemplates() {
        new MessageTemplateHelper().loadResources(MESSAGE_TEMPLATE_FILE_LOCATION);
    }


    public static synchronized void addInboundTemplate(String serviceName, String serviceNumber, MessageTemplate template) {
        SERVICE_NUMBERS.put(serviceName, serviceNumber);
        INBOUND_TEMPLATES.put(serviceNumber, template);
    }


    public static synchronized void addOutboundTemplate(String serviceName, String serviceNumber, MessageTemplate template) {
        SERVICE_NUMBERS.put(serviceName, serviceNumber);
        OUTBOUND_TEMPLATES.put(serviceNumber, template);
    }


    public static synchronized MessageTemplate getInboundMessageTemplate(String serviceNumberOrName) {
        MessageTemplate template = INBOUND_TEMPLATES.get(SERVICE_NUMBERS.get(serviceNumberOrName));
        if (template == null) {
            return INBOUND_TEMPLATES.get(serviceNumberOrName);
        } else {
            return template;
        }
    }


    public static synchronized MessageTemplate getOutboundMessageTemplate(String serviceNumberOrName) {
        MessageTemplate template = OUTBOUND_TEMPLATES.get(serviceNumberOrName);
        if (template == null) {
            String serviceNumber = SERVICE_NUMBERS.get(serviceNumberOrName);
            if (serviceNumber != null) {
                template = OUTBOUND_TEMPLATES.get(serviceNumber);
            }
        }
        return template;
    }

}

