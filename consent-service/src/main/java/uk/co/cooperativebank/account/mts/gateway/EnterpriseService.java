package uk.co.cooperativebank.account.mts.gateway;

public interface EnterpriseService {
    String getName();
}
