package uk.co.cooperativebank.account.mts.templates;

/**
 * @author Des McGuigan
 */
public class MessageField {
    /**
     * Constant for empty
     */
    public static final int EMPTY = -1;

    private String name = null;
    private String value = null;
    private String type = null;
    private int length = 0;

    // field is option which is used when the ouput lenght id different
    // from the input field length. Currently this occurs with translators that perform
    // encryption during the encode phase
    private int outputLength = EMPTY;

    /**
     * Gets the field name
     * @return Returns a String
     */
    public String getName() {
        return name;
    }
    /**
     * Sets the field name
     * @param name The name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the field value
     * @return Returns a String
     */
    public String getValue() {
        return value;
    }
    /**
     * Sets the field value
     * @param value The value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the field type
     * @return Returns a String
     */
    public String getType() {
        return type;
    }
    /**
     * Sets the field type
     * @param type The type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the field length
     * @return Returns an int
     */
    public int getLength() {
        return length;
    }
    /**
     * Sets the field length
     * @param length The length to set
     */
    public void setLength(int length) {
        this.length = length;
    }
    /**
     * Gets the outputLength
     * @return Returns a int
     */
    public int getOutputLength() {
        return outputLength;
    }
    /**
     * Sets the outputLength
     * @param outputLength The outputLength to set
     */
    public void setOutputLength(int outputLength) {
        this.outputLength = outputLength;
    }

}