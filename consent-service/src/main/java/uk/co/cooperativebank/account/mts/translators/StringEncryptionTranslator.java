package uk.co.cooperativebank.account.mts.translators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cooperativebank.account.mts.encryption.EncryptedString;
import uk.co.cooperativebank.account.mts.encryption.EncryptionService;
import uk.co.cooperativebank.account.mts.encryption.StringEncryptionService;
import uk.co.cooperativebank.account.mts.exceptions.EncryptionAlert;
import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;


/**
 * Class is used as a translator to encrypt and decrypt strings
 *
 * @author Craig Parkinson
 */
public class StringEncryptionTranslator extends AbstractHostTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(StringEncryptionTranslator.class);

    /**
     * Default constructor
     */
    public StringEncryptionTranslator() {
    }

    /**
     * Encodes the key
     * @param obj the dao object
     * @param inlength the length of the field.
     * @param outlength the length of the field.
     * @return an encrypted string
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public String encode(Object obj, int inlength, int outlength) throws HostFieldFormatException {

        String result;
        if (obj != null) {
            assertObjectType(obj, EncryptedString.class);

            // check lenghts output must twice the input
            if ((inlength * 2) != (outlength)) {
                throw new HostFieldFormatException("Ouput size should be twice the input size");
            }

            // encrypt the string
            // use StringEncryptionService to decrypt string
            EncryptionService srv = StringEncryptionService.getInstance();

            try {
                result = srv.encrypt(((EncryptedString) obj).getSourceStr());
            } catch (EncryptionAlert ex) {
                LOG.error("EncryptionAlert: " + ex.getMessage(), ex);
                throw new HostFieldFormatException(ex.getMessage());
            }

            // now pad with space upto the length of the ouput field
            result = Utilities.padStringAfter(result, outlength, ' ');
        }
        else {
            result = Utilities.padStringBefore("", outlength, ' ');
        }

        return result;
    }

    /**
     * Encodes the key
     * @param str - the string to create the dao object
     * @return Object - the decoded object
     * @throws HostFieldFormatException a field format exception
     */
    @Override
    public Object decode(String str) throws HostFieldFormatException {

        if (str == null) {
            throw new HostFieldFormatException("Invalid string. Value is null");
        }

        // use StringEncryptionService to decrypt string
        EncryptionService srv = StringEncryptionService.getInstance();

        String result = null;
        try {
            result = srv.decrypt(str);
        } catch (EncryptionAlert ex) {
            LOG.error("EncryptionAlert: " + ex.getMessage(), ex);
            throw new HostFieldFormatException(ex.getMessage());
        }

        return result;
    }
}
