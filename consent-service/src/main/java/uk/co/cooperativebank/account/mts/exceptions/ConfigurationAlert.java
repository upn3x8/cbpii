package uk.co.cooperativebank.account.mts.exceptions;

public class ConfigurationAlert extends AlertingException {

    private final String alertId;

    private final AlertReasonDetail alertReasonDetail;

    public ConfigurationAlert(AlertReasonDetail alertReasonDetail) {
        this.alertId = this.getClass().getSimpleName();
        this.alertReasonDetail = alertReasonDetail;

    }

    public ConfigurationAlert(String alertId, AlertReasonDetail alertReasonDetail) {
        this.alertId = alertId;
        this.alertReasonDetail = alertReasonDetail;

    }

    @Override
    public String getAlertID() {
        return alertId;
    }

    @Override
    public AlertReasonDetail getAlertReasonDetail() {
        return alertReasonDetail;
    }

}