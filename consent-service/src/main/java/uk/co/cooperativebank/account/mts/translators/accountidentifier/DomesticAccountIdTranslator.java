package uk.co.cooperativebank.account.mts.translators.accountidentifier;


import uk.co.cooperativebank.account.mts.dao.account.DomesticAccount;
import uk.co.cooperativebank.account.mts.exceptions.HostFieldFormatException;
import uk.co.cooperativebank.account.mts.templates.utilities.Utilities;
import uk.co.cooperativebank.account.mts.translators.AbstractHostTranslator;
import uk.co.cooperativebank.consent.service.Constants;

/**
 * @author Digital-Dev
 *
 */
public class DomesticAccountIdTranslator extends AbstractHostTranslator {

    /**
     *
     */
    public DomesticAccountIdTranslator() {
        super();

    }

    /**
     * Encodes the key
     * @param obj the data object
     * @param inlength the length of the field.
     * @param outlength the output length
     * @return a string for AccountId
     * @throws HostFieldFormatException a field format exception
     */

    @Override
    public String encode(Object obj, int inlength, int outlength) throws HostFieldFormatException {
        if (obj == null) {
            return Utilities.getHostNull(outlength);
        }
        assertObjectType(obj, DomesticAccount.ID.class);

        DomesticAccount.ID val = (DomesticAccount.ID) obj;

        return val.getSortCode() + val.getAccountNumber() + val.getAccountType();
    }

    @Override
    public Object decode(String str) throws HostFieldFormatException {
        String str1 = str.substring(0, Constants.SORT_CODE_INDEX);
        String str2 = str.substring(Constants.SORT_CODE_INDEX, Constants.ACCOUNT_NUMBER_INDEX);
        String str3 = str.substring(Constants.ACCOUNT_NUMBER_INDEX, 16);
        return new DomesticAccount.ID( str1, str2, str3);
    }

}
