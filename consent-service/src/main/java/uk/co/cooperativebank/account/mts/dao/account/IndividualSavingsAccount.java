package uk.co.cooperativebank.account.mts.dao.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.math.BigDecimal;

@JsonTypeName("ISA")
public class IndividualSavingsAccount extends DomesticAccount {

    private BigDecimal availableBalance;

    @Override
    protected void initialiseDomesticAccountSpecificDerivedFields() {
        this.availableBalance = calculateAvailableBalance();
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public static class ID extends DomesticAccount.ID {

        public ID() {
            super();
        }

        public ID(String identifier) {
            super(identifier);
        }

        @JsonCreator
        public ID(@JsonProperty("sortCode") final String sortCode,
                  @JsonProperty("accountNumber") final String accountNumber,
                  @JsonProperty("accountType") final String accountType) {
            super(sortCode, accountNumber, accountType);
        }
    }
}
