package uk.co.cooperativebank;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import uk.co.cooperativebank.account.AccountBasePackage;
import uk.co.cooperativebank.consent.BasePackage;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = {BasePackage.class, AccountBasePackage.class})
@IntegrationComponentScan("uk.co.cooperativebank.account")
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {

  @PostConstruct
  public void setupTimeZone() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }

  /**
   * Proxies service level beans to perform method validation.
   * @return
   */
  @Bean
  public MethodValidationPostProcessor getMethodValidationPostProcessor(){
    MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
    processor.setValidator(validator());
    return processor;
  }

  @Bean
  public LocalValidatorFactoryBean validator(){
    return new LocalValidatorFactoryBean();
  }
}
