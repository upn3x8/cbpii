package uk.co.cooperativebank.consent.config;


import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;

@Slf4j
@Configuration
@PropertySource("classpath:datasource.properties")
public class DataConfiguration {

  private final Environment env;

  public DataConfiguration(Environment env) {
    this.env = env;
  }

  @Profile("local")
  @Bean("consentDataSource")
  public DataSource getDataSource() {
    log.info("Setting up local datasource.");
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
    dataSource.setUrl(env.getProperty("jdbc.url"));
    dataSource.setUsername(env.getProperty("jdbc.username"));
    dataSource.setPassword(env.getProperty("jdbc.password"));
//    dataSource.setMaxIdle(env.getProperty("dbcp.maxIdle", Integer.class));
    return dataSource;
  }

  @Profile("!local")
  @Bean("consentDataSource")
  public DataSource getJndiDataSource() throws NamingException {
    String jndiLookupName = env.getProperty("datasource.jndi.name");
    log.info("Setting up JNDI datasource, name:{}", jndiLookupName);
    return (DataSource) new JndiTemplate().lookup(jndiLookupName);
  }

  @Profile("local")
  @Bean("mtsDataSource")
  DataSource mtsDataSource() throws SQLException {
    OracleDataSource dataSource = new OracleDataSource();
    dataSource.setURL(env.getProperty("mts.jdbc.url"));
    dataSource.setUser(env.getProperty("mts.jdbc.username"));
    dataSource.setPassword(env.getProperty("mts.jdbc.password"));
    dataSource.setImplicitCachingEnabled(true);
    dataSource.setFastConnectionFailoverEnabled(true);
    return dataSource;
  }

  @Profile("!local")
  @Bean("mtsDataSource")
  public DataSource getJndiMtsDataSource() throws NamingException {
    String jndiLookupName = env.getProperty("mts.datasource.jndi.name");
    log.info("Setting up JNDI datasource for reference data maps, name:{}", jndiLookupName);
    return (DataSource) new JndiTemplate().lookup(jndiLookupName);
  }
}
