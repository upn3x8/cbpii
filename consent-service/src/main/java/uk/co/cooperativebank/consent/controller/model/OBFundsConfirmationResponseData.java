package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

/**
 * OBFundsConfirmationResponseData
 */
@Builder
@Validated
public class OBFundsConfirmationResponseData {
  @JsonProperty("FundsConfirmationId")
  private String fundsConfirmationId;

  @JsonProperty("ConsentId")
  private String consentId;

  @JsonProperty("CreationDateTime")
  private OffsetDateTime creationDateTime;

  @JsonProperty("FundsAvailable")
  private Boolean fundsAvailable;

  @JsonProperty("Reference")
  private String reference;

  @JsonProperty("InstructedAmount")
  private OBFundsConfirmationDataInstructedAmount instructedAmount;

  public OBFundsConfirmationResponseData fundsConfirmationId(String fundsConfirmationId) {
    this.fundsConfirmationId = fundsConfirmationId;
    return this;
  }

  /**
   * Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation resource.
   *
   * @return fundsConfirmationId
   **/
  @NotNull
  @Size(min = 1, max = 40)
  public String getFundsConfirmationId() {
    return fundsConfirmationId;
  }

  public void setFundsConfirmationId(String fundsConfirmationId) {
    this.fundsConfirmationId = fundsConfirmationId;
  }

  public OBFundsConfirmationResponseData consentId(String consentId) {
    this.consentId = consentId;
    return this;
  }

  /**
   * Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation consent resource.
   *
   * @return consentId
   **/
  @NotNull
  @Size(min = 1, max = 128)
  public String getConsentId() {
    return consentId;
  }

  public void setConsentId(String consentId) {
    this.consentId = consentId;
  }

  public OBFundsConfirmationResponseData creationDateTime(OffsetDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
    return this;
  }

  /**
   * Date and time at which the resource was created.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   *
   * @return creationDateTime
   **/
  @NotNull
  @Valid
  public OffsetDateTime getCreationDateTime() {
    return creationDateTime;
  }

  public void setCreationDateTime(OffsetDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  public OBFundsConfirmationResponseData fundsAvailable(Boolean fundsAvailable) {
    this.fundsAvailable = fundsAvailable;
    return this;
  }

  /**
   * Flag to indicate the result of a confirmation of funds check.
   *
   * @return fundsAvailable
   **/
  @NotNull
  public Boolean isFundsAvailable() {
    return fundsAvailable;
  }

  public void setFundsAvailable(Boolean fundsAvailable) {
    this.fundsAvailable = fundsAvailable;
  }

  public OBFundsConfirmationResponseData reference(String reference) {
    this.reference = reference;
    return this;
  }

  /**
   * Unique reference, as assigned by the CBPII, to unambiguously refer to the request related to the payment transaction.
   *
   * @return reference
   **/
  @NotNull
  @Size(min = 1, max = 35)
  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public OBFundsConfirmationResponseData instructedAmount(OBFundsConfirmationDataInstructedAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
    return this;
  }

  /**
   * Get instructedAmount
   *
   * @return instructedAmount
   **/
  @NotNull
  @Valid
  public OBFundsConfirmationDataInstructedAmount getInstructedAmount() {
    return instructedAmount;
  }

  public void setInstructedAmount(OBFundsConfirmationDataInstructedAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationResponseData that = (OBFundsConfirmationResponseData) o;

    return new EqualsBuilder()
        .append(fundsConfirmationId, that.fundsConfirmationId)
        .append(consentId, that.consentId)
        .append(creationDateTime, that.creationDateTime)
        .append(fundsAvailable, that.fundsAvailable)
        .append(reference, that.reference)
        .append(instructedAmount, that.instructedAmount)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(fundsConfirmationId)
        .append(consentId)
        .append(creationDateTime)
        .append(fundsAvailable)
        .append(reference)
        .append(instructedAmount)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("fundsConfirmationId", fundsConfirmationId)
        .append("consentId", consentId)
        .append("creationDateTime", creationDateTime)
        .append("fundsAvailable", fundsAvailable)
        .append("reference", reference)
        .append("instructedAmount", instructedAmount)
        .toString();
  }
}

