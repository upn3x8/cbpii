package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * OBFundsConfirmationResponse
 */
@Builder
@Validated
public class OBFundsConfirmationResponse {
  @JsonProperty("Data")
  private OBFundsConfirmationResponseData data;

  public OBFundsConfirmationResponse data(OBFundsConfirmationResponseData data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   *
   * @return data
   **/
  @NotNull
  @Valid
  public OBFundsConfirmationResponseData getData() {
    return data;
  }

  public void setData(OBFundsConfirmationResponseData data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationResponse that = (OBFundsConfirmationResponse) o;

    return new EqualsBuilder()
        .append(data, that.data)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(data)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("data", data)
        .toString();
  }
}

