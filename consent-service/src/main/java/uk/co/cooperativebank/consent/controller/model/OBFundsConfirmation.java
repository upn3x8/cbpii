package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * OBFundsConfirmation
 */
@Builder
@Validated
public class OBFundsConfirmation {
  public static final String UK_OBIE_SORT_CODE_ACCOUNT_NUMBER = "UK.OBIE.SortCodeAccountNumber";
  @JsonProperty("Data")
  private OBFundsConfirmationData data = null;

  public OBFundsConfirmation data(OBFundsConfirmationData data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   *
   * @return data
   **/
  @NotNull
  @Valid
  public OBFundsConfirmationData getData() {
    return data;
  }

  public void setData(OBFundsConfirmationData data) {
    this.data = data;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmation that = (OBFundsConfirmation) o;

    return new EqualsBuilder()
        .append(data, that.data)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(data)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("data", data)
        .toString();
  }
}

