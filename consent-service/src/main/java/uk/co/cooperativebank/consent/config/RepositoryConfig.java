package uk.co.cooperativebank.consent.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import uk.co.cooperativebank.consent.repository.RepositoryPackage;

@Configuration
@EnableJpaRepositories(basePackageClasses = {RepositoryPackage.class})
public class RepositoryConfig {

}
