package uk.co.cooperativebank.consent.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import uk.co.cooperativebank.consent.domain.FundsConfirmationConsent;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface FundsConfirmationConsentRepository extends
    CrudRepository<FundsConfirmationConsent, Long> {

  Optional<FundsConfirmationConsent> findByConsentIdAndTppId(Long id, String tppId);

  // Flushes so any in-session entities are not out of sync.
  @Modifying(clearAutomatically = true)
  @Query("update FundsConfirmationConsent "
      + "set status = :status, statusUpdated = :statusUpdated "
      + "where consentId = :consentId "
      + "and status = "
      + "uk.co.cooperativebank.consent.domain.FundsConfirmationConsent$Status.AWAITINGAUTHORISATION "
      + "and expiryDate > :statusUpdated")
  int updateAwaitingAuthConfirmationConsent(@Param("consentId") Long consentId,
                                            @Param("status") FundsConfirmationConsent.Status status,
                                            @Param("statusUpdated") LocalDateTime statusUpdated);

  // Flushes so any in-session entities are not out of sync.
  @Modifying(clearAutomatically = true)
  @Query("update FundsConfirmationConsent "
      + "set status = :status, statusUpdated = :statusUpdated "
      + "where consentId = :consentId "
      + "and status = "
      + "uk.co.cooperativebank.consent.domain.FundsConfirmationConsent$Status.AUTHORISED "
      + "and expiryDate > :statusUpdated")
  int updateAuthorisedConfirmationConsent(@Param("consentId") Long consentId,
                                            @Param("status") FundsConfirmationConsent.Status status,
                                            @Param("statusUpdated") LocalDateTime statusUpdated);

  List<FundsConfirmationConsent> findByDcId(String dcId);

  @Query("select c from FundsConfirmationConsent c where consentId = :consentId "
      + "and tppId = :tppId "
      + "and status = uk.co.cooperativebank.consent.domain.FundsConfirmationConsent$Status.AUTHORISED "
      + "and expiryDate > :currentDate")
  Optional<FundsConfirmationConsent> findAuthorisedConfirmation(@Param("consentId") Long consentId,
                                                                @Param("tppId") String tppId,
                                                                @Param("currentDate") LocalDateTime currentDate);
}
