package uk.co.cooperativebank.consent.repository;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

@Repository
public class HealthCheckRepository {

  @PersistenceContext
  private EntityManager entityManager;

  public boolean isDbConnected() {
    try {
      Query nativeQuery = entityManager.createNativeQuery("select 1 from dual");
      return nativeQuery.getSingleResult() != null;
    } catch (PersistenceException e) {
      return false;
    }
  }
}
