package uk.co.cooperativebank.consent.controller.exception;

public class BadRequestException extends ApiException {
  private static final int CODE = 400;

  public BadRequestException(String msg) {
    super(CODE, msg);
  }
}
