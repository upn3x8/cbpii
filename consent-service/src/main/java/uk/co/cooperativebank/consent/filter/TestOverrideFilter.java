package uk.co.cooperativebank.consent.filter;


import org.apache.commons.lang3.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Arrays;

/**
 * Inspects the header for a tppId header and moves it to a known key.
 */
public class TestOverrideFilter implements Filter {

  public static final String HEADER_PREFIX = "header-";

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) servletRequest;
    MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(req);

    // grab the query params prefixed with 'header-' and re-inject them as a header.
    String queryString = mutableRequest.getQueryString();

    if (null != queryString) {
      // split into each
      Arrays.stream(queryString.split("&"))
          .filter(s -> s.startsWith(HEADER_PREFIX))
          .map(s -> s.substring(HEADER_PREFIX.length()))
          .forEach(s -> {
            String[] parts = s.split("=");
            if (parts.length == 2
                && StringUtils.isNotBlank(parts[0])
                && StringUtils.isNotBlank(parts[1])) {
              mutableRequest.putHeader(parts[0], URLDecoder.decode(parts[1]));
            }
          });
    }

    filterChain.doFilter(mutableRequest, servletResponse);
  }

  @Override
  public void destroy() {
  }

}
