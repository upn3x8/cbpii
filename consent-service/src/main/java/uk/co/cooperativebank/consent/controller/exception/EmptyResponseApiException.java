package uk.co.cooperativebank.consent.controller.exception;

public class EmptyResponseApiException extends ApiException {
  private static final int code = 204;

  public EmptyResponseApiException() {
    super(code, null);
  }
}
