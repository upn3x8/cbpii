package uk.co.cooperativebank.consent.domain;

import javax.persistence.Column;

//@Data
//@Entity
//@Table(name = "FUNDS_CONFIRMATION_CONSENT")
public class MtsAccount {

  @Column(name = "sort_code")
  private String sortCode;

  @Column(name = "account_number")
  private String accountNumber;

  @Column(name = "account_type")
  private String accountType;


}
