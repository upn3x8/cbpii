package uk.co.cooperativebank.consent.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import uk.co.cooperativebank.account.config.BrandConfiguration;
import uk.co.cooperativebank.account.dtos.AccountRequest;
import uk.co.cooperativebank.account.dtos.Customer;
import uk.co.cooperativebank.account.dtos.CustomerRequest;
import uk.co.cooperativebank.account.gateway.BalanceAndTransactionsServiceGateway;
import uk.co.cooperativebank.account.gateway.BtrEnterpriseServices;
import uk.co.cooperativebank.account.gateway.CustomerEnterpriseServices;
import uk.co.cooperativebank.account.gateway.CustomerServiceGateway;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.dao.account.Account;
import uk.co.cooperativebank.account.mts.dao.account.DomesticAccount;
import uk.co.cooperativebank.account.mts.gateway.EnterpriseServiceManager;
import uk.co.cooperativebank.consent.controller.exception.BadRequestException;
import uk.co.cooperativebank.consent.controller.exception.NotFoundException;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static uk.co.cooperativebank.account.mts.dao.account.AccountStatus.ACCOUNT_ACTIVE;

@Slf4j
@Service
@Transactional
public class DefaultCustomerAccountService implements CustomerAccountService {

  private final EnterpriseServiceManager callUtil;
  private final BalanceAndTransactionsServiceGateway balanceAndTransactionsServiceGateway;
  private final CustomerServiceGateway customerServiceGateway;
  private final BrandConfiguration brandConfiguration;

  private String[] allowedProductCodeList;


  @Autowired
  public DefaultCustomerAccountService(EnterpriseServiceManager callUtil,
                                       BalanceAndTransactionsServiceGateway balanceAndTransactionsServiceGateway,
                                       CustomerServiceGateway customerServiceGateway,
                                       BrandConfiguration brandConfiguration,
                                       @NotNull @Value("${mainframe.account.product.code.allowed}") String productCodeCsvList) {
    this.callUtil = callUtil;
    this.balanceAndTransactionsServiceGateway = balanceAndTransactionsServiceGateway;
    this.customerServiceGateway = customerServiceGateway;
    this.brandConfiguration = brandConfiguration;
    this.allowedProductCodeList = productCodeCsvList.split(",");
    if (allowedProductCodeList == null || allowedProductCodeList.length == 0) {
      throw new IllegalArgumentException("Allowed product types must be configured!");
    }
  }

  /**
   * accountIdentification is 6 chars sortCode + 8 chars account number
   *
   * @param accountIdentification the id
   * @return the customer
   */
  @Override
  public Customer findCustomerByAccount(String accountIdentification, ClientContext clientContext) {
    if (accountIdentification.length() < Constants.ACCOUNT_NUMBER_INDEX) {
      throw new BadRequestException("Identification not Valid");
    }

    final CustomerRequest request = createCustomerRequest(accountIdentification);
    request.setClientContext(clientContext);
    try {
      final List<Customer> customers = callUtil.call(payload -> {
            payload.setRequest(request);
            payload.setService(CustomerEnterpriseServices.findCustomerByAccount);
            if (brandConfiguration.isCoopCustomer(clientContext.getBrand())) {
              return customerServiceGateway.findCustomerByAccount(payload);
            } else {
              return customerServiceGateway.findCustomerByAccountSM(payload);
            }
          },
          request.getClientContext(),
          "");

      return customers.stream()
          .filter(customer -> StringUtils.isNotBlank(customer.getDigitalCustomerId()))
          .findFirst()
          .orElseThrow(() -> new NotFoundException(null));
    } catch (Exception e) {
      String message = "Customer with Identification: " + accountIdentification + " not found";
      log.debug(message, e);
      throw new NotFoundException(message);
    }
  }

  @Override
  public Optional<Boolean> customerAccountHasEnoughFunds(String customerNumber, String accountIdentification, BigDecimal amount, ClientContext clientContext) {

    if (accountIdentification.length() < Constants.INDENTIFIER_LENGTH) {
      throw new BadRequestException("Identification not Valid");
    }

    DomesticAccount account = getDomesticAccount(customerNumber, accountIdentification, clientContext);

    // Enforce the account having ACTIVE status otherwise we cannot continue
    if (ACCOUNT_ACTIVE != account.getAccountStatus()) {
      return Optional.empty();
    }

    final BigDecimal availableBalance = account.getAvailableBalance();
    return Optional.of(availableBalance.compareTo(amount) >= 0);
  }

  @Override
  public DomesticAccount getDomesticAccount(String customerNumber, String accountIdentification, ClientContext clientContext) {

    if (accountIdentification.length() < Constants.INDENTIFIER_LENGTH) {
      throw new BadRequestException("Identification not Valid");
    }
    final AccountRequest request = createAccountRequest(accountIdentification);
    request.setClientContext(clientContext);

    return getPreparedCustomerAccount(request, customerNumber);

  }


  private DomesticAccount getPreparedCustomerAccount(AccountRequest request, String customerNumber) {

    List<Account> accounts = Collections.emptyList();

    try {
      accounts = callUtil.call(payload -> {
        payload.setRequest(request);
        payload.setService(BtrEnterpriseServices.getAccount);
        if (brandConfiguration.isCoopCustomer(request.getClientContext().getBrand())) {
          return balanceAndTransactionsServiceGateway.getAccounts(payload);
        } else {
          return balanceAndTransactionsServiceGateway.getAccountsSM(payload);
        }
      }, request.getClientContext(), customerNumber);

    } catch (Exception e) {
      log.debug("Customer with Identification: {} not found", request.getAccountID().fullIdentifierValue(), e);
      throw new NotFoundException("Customer with Identification:" + request.getAccountID().fullIdentifierValue() + " not found");
    }

    if (accounts.isEmpty()) {
      throw new NotFoundException("Customer with Identification: " + request.getAccountID().fullIdentifierValue() + " not found");
    }

    Account account = accounts.get(0);

    // does this account match one of the allowed product types?
    if (log.isDebugEnabled()) {
      log.debug("Account ID: {}, productCode: {}", account.getIdentifier().fullIdentifierValue(), account.getProductCode());
    }

    Optional<String> foundCode = Arrays.stream(allowedProductCodeList)
        .filter(code -> code.equals(account.getProductCode()))
        .findFirst();

    if (foundCode.isPresent()) {
      account.prepareForPresentation();
      return (DomesticAccount) account;
    }

    log.warn("Cannot find a customer account {} with an allowed product type!", request.getAccountID().fullIdentifierValue());
    throw new NotFoundException("Customer with Identification: " + request.getAccountID().fullIdentifierValue() + " not found");

  }

  private CustomerRequest createCustomerRequest(String accountIdentification) {
    //Creates customer request with no account type
    final CustomerRequest request = new CustomerRequest();
    request.setSortCode(accountIdentification.substring(0, Constants.SORT_CODE_INDEX));
    request.setAccountNumber(accountIdentification.substring(Constants.SORT_CODE_INDEX, Constants.ACCOUNT_NUMBER_INDEX));
    return request;
  }

  /**
   * Requires the full coop/smile sort code, account number and accountType.
   *
   * @param accountIdentification
   * @return
   */
  private AccountRequest createAccountRequest(String accountIdentification) {
    Assert.isTrue(StringUtils.isNotBlank(accountIdentification), "Must supply an account identification");
    Assert.isTrue(accountIdentification.length() == Constants.INDENTIFIER_LENGTH, "Account Identification should include sort code, account number and accountType");

    final AccountRequest request = new AccountRequest();
    request.setAccountID(new DomesticAccount.ID(
        accountIdentification.substring(0, Constants.SORT_CODE_INDEX),
        accountIdentification.substring(Constants.SORT_CODE_INDEX, Constants.ACCOUNT_NUMBER_INDEX),
        accountIdentification.substring(Constants.ACCOUNT_NUMBER_INDEX, Constants.INDENTIFIER_LENGTH)));
    return request;
  }

}