package uk.co.cooperativebank.consent.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "FUNDS_CONFIRMATION")
@Entity
public class FundsConfirmation {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNDS_CONFIRMATION_SEQ")
  @SequenceGenerator(sequenceName = "FUNDS_CONFIRMATION_SEQ", allocationSize = 1, name = "FUNDS_CONFIRMATION_SEQ")
  @Column(name = "CONFIRMATION_ID")
  private Long confirmationId;

  @ManyToOne
  @JoinColumn(name = "CONSENT_ID")
  private FundsConfirmationConsent confirmationConsent;

  @Column(name = "REFERENCE")
  private String reference;

  @Column(name = "AMOUNT")
  private BigDecimal amount;

  @Column(name = "CURRENCY")
  private String currency;

  @Column(name = "APPROVED")
  private Boolean approved;

  @Column(name = "CREATED")
  @Type(type = "uk.co.cooperativebank.consent.jpa.usertype.LocalDateTimeUserType")
  private LocalDateTime created;

  public FundsConfirmation() {
  }

}
