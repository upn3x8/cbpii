package uk.co.cooperativebank.consent.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import uk.co.cooperativebank.consent.repository.HealthCheckRepository;

@Slf4j
@Controller
public class HealthCheckApiController {

  private static final String HEALTH_CHECK_URL = "/health";
  private HealthCheckRepository repository;

  @Autowired
  public HealthCheckApiController(HealthCheckRepository repository) {
    this.repository = repository;
  }

  @GetMapping(value = HEALTH_CHECK_URL)
  public ResponseEntity<String> healthCheck() {

    if (repository.isDbConnected()) {
      return ResponseEntity.status(200).contentType(MediaType.TEXT_PLAIN).body("OK");
    }

    return ResponseEntity.status(500).body("UNAVAILABLE");

  }

}
