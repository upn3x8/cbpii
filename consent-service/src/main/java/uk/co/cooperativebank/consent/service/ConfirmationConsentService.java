package uk.co.cooperativebank.consent.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import uk.co.cooperativebank.account.config.BrandConfiguration;
import uk.co.cooperativebank.account.dtos.Customer;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.dao.account.DomesticAccount;
import uk.co.cooperativebank.consent.controller.exception.ApiException;
import uk.co.cooperativebank.consent.controller.exception.BadRequestException;
import uk.co.cooperativebank.consent.controller.exception.NotFoundException;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApproval;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApprove;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentCreateData;
import uk.co.cooperativebank.consent.domain.FundsConfirmationConsent;
import uk.co.cooperativebank.consent.repository.FundsConfirmationConsentRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static uk.co.cooperativebank.account.mts.dao.account.AccountStatus.ACCOUNT_ACTIVE;
import static uk.co.cooperativebank.consent.service.Constants.REGEX_NOT_BLANK;

/**
 * Contains business logic for calls passed in from the Controllers.
 */
@Slf4j
@Service
@Validated
@Transactional
public class ConfirmationConsentService {

  public static final String FIXED_SCHEME_NAME = "UK.OBIE.SortCodeAccountNumber";

  private CustomerAccountService customerAccountService;
  private FundsConfirmationConsentRepository confirmationConsentRepository;
  private BrandConfiguration brandConfiguration;

  @Autowired
  public ConfirmationConsentService(CustomerAccountService customerAccountService, FundsConfirmationConsentRepository confirmationConsentRepository, BrandConfiguration brandConfiguration) {
    this.customerAccountService = customerAccountService;
    this.confirmationConsentRepository = confirmationConsentRepository;
    this.brandConfiguration = brandConfiguration;
  }
  //0002 - get accounts by dcid

  /**
   * Create a ConfirmationConsent, this is the item that needs to be approved by the PSU/Customer.
   * <b>Identification is six-digit sort code followed by an eight-digit account number.</b>
   *
   * @param confirmationConsent
   * @param tppId
   * @param clientContext       the context which contains extra information for use by fraud
   */
  public FundsConfirmationConsent createConfirmationConsent(@NotNull(message = "Must provide a non-null argument") OBFundsConfirmationConsentCreateData confirmationConsent,
                                                            @Pattern(regexp = REGEX_NOT_BLANK, message = "Must provide a tppId") String tppId,
                                                            @Valid ClientContext clientContext) {

    final LocalDateTime now = LocalDateTime.now();
    validateExpiryDate(confirmationConsent, now);

    // fix the brand within the client context. This is because the client context may have been
    // generated without a sort code.
    clientContext.setBrand(brandConfiguration.getBrandByIdentification(confirmationConsent.getDebtorAccount().getIdentification()));

    // find the customer
    final Customer customer = customerAccountService.findCustomerByAccount(confirmationConsent.getDebtorAccount().getIdentification(), clientContext);

    // determine if this customer has a product that is applicable.
    // will throw a NotFoundException if the productCode cannot be matched.
    DomesticAccount domesticAccount = customerAccountService.getDomesticAccount(customer.getIdentifier().getId(), customer.getAccountNumber(), clientContext);
    if (ACCOUNT_ACTIVE != domesticAccount.getAccountStatus()) {
      throw new BadRequestException("Cannot create a confirmation consent for an account that is not ACTIVE");
    }

    // Populate a ConfirmationConsent and save
    final FundsConfirmationConsent consent = FundsConfirmationConsent.builder()
        .identification(customer.getAccountNumber())
        .tppId(tppId)
        .dcId(customer.getDigitalCustomerId())
        .cId(customer.getIdentifier().getId())
        .expiryDate(confirmationConsent.getExpirationDateTime().toLocalDateTime())
        .status(FundsConfirmationConsent.Status.AWAITINGAUTHORISATION)
        .statusUpdated(now)
        .created(now)
        .build();

    confirmationConsentRepository.save(consent);

    return consent;
  }

  /**
   * Returns a single FundsConfirmationConsent.
   *
   * @param consentId a consent Id
   * @param tppId
   * @return a list of {@link FundsConfirmationConsent}
   * @throws @see NotFoundException
   */
  public FundsConfirmationConsent retrieveConfirmationConsent(@NotNull(message = "Must provide a consentId") Long consentId,
                                                              @Pattern(regexp = REGEX_NOT_BLANK, message = "Must provide a tppId") String tppId) throws NotFoundException {
    Optional<FundsConfirmationConsent> foundConsent = confirmationConsentRepository.findByConsentIdAndTppId(consentId, tppId);

    if (foundConsent.isPresent()) {
      return foundConsent.get();
    }

    throw new NotFoundException("Consent not found");
  }

  /**
   * Returns a list of FundsConfirmationConsent records.
   *
   * @param dcId the digital customer Identification
   * @return a list of {@link FundsConfirmationConsent}
   */
  public List<FundsConfirmationConsent> retrieveConfirmationConsent(
      @Pattern(regexp = REGEX_NOT_BLANK, message = "Must provide a valid dcid") String dcId) {
    return confirmationConsentRepository.findByDcId(dcId);
  }

  public void updateConfirmationConsent(OBFundsConfirmationConsentApprove obFundsConfirmationConsentApprove) throws NotFoundException {

    Assert.notNull(obFundsConfirmationConsentApprove, "Must provide a non-null confirmationConsentApprove");
    OBFundsConfirmationConsentApproval data = obFundsConfirmationConsentApprove.getData();
    Assert.notNull(data.getConsentId(), "Must provide a non-null consentId");
    Assert.notNull(data.getAction(), "Must provide a non-null approval action");

    FundsConfirmationConsent.Status status = mapFromAction(data.getAction());
    long consentId = Long.parseLong(data.getConsentId());
    int rowsUpdated;

    switch (data.getAction()) {
      case APPROVE:
      case REJECT: {
        rowsUpdated = confirmationConsentRepository.updateAwaitingAuthConfirmationConsent(consentId, status, LocalDateTime.now());
        break;
      }
      case REVOKE:
      default:
        rowsUpdated = confirmationConsentRepository.updateAuthorisedConfirmationConsent(consentId, status, LocalDateTime.now());
        break;
    }

    // If it doesn't fail, it has worked
    if (rowsUpdated != 1) {
      throw new NotFoundException("Cannot find consent: " + data.getConsentId());
    }

  }

  /**
   * Simple mapping to update the ConfirmationConsent based on the action enum provided.
   *
   * @param action the action to be performed on the consent {@link OBFundsConfirmationConsentApproval.ActionEnum}
   * @return a {@link FundsConfirmationConsent.Status}
   */
  private FundsConfirmationConsent.Status mapFromAction(OBFundsConfirmationConsentApproval.ActionEnum action) {

    switch (action) {
      case APPROVE:
        return FundsConfirmationConsent.Status.AUTHORISED;
      case REJECT:
        return FundsConfirmationConsent.Status.REJECTED;
      case REVOKE:
        return FundsConfirmationConsent.Status.REVOKED;
      default:
        throw new ApiException(500, "An OBFundsConfirmationConsentApproval.ActionEnum was provided "
            + "that cannot be mapped to a ConfirmationConsent.Status");
    }
  }

  private void validateExpiryDate(@NotNull(message = "Must provide a non-null argument") OBFundsConfirmationConsentCreateData confirmationConsent, LocalDateTime now) {
    if (now.isAfter(confirmationConsent.getExpirationDateTime().toLocalDateTime())) {
      throw new BadRequestException("Expiration date must be after the current date/time.");
    }

    if (now.plusMonths(13).isBefore(confirmationConsent.getExpirationDateTime().toLocalDateTime())) {
      throw new BadRequestException("Expiration date must be within 13 months of creation date");
    }
  }
}