package uk.co.cooperativebank.consent.filter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.consent.controller.exception.BadRequestException;

import java.io.IOException;
import java.util.Base64;

@Slf4j
@Component
public class ClientContextFactory {

  /**
   * Builds a client context from a base64 encoded context if possible
   *
   * @param base64EncodedContext client context in encoded in base64
   * @return a client context
   * @throws BadRequestException exception thrown if no context could be build from the base64EncodedContext
   */
  public ClientContext getClientContext(String base64EncodedContext) {

    if (StringUtils.isBlank(base64EncodedContext)) {
      throw new BadRequestException("Mandatory header x-coop-clientcontext couldn't be found!");
    }

    try {
      byte[] decoded = Base64.getDecoder().decode(base64EncodedContext);

      //intentionally didnt wire the object mapper due to the configuration set below.
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      return mapper.readValue(decoded, ClientContext.class);
    } catch (IOException e) {
      log.error("Unable to decode client context");
      throw new BadRequestException("Mandatory header " + ClientContextFilter.CLIENT_CONTEXT_HEADER + " couldn't be found!");
    }
  }
}
