package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * OBError
 */
@Builder
@Validated
public class OBError {
  @JsonProperty("ErrorCode")
  private String errorCode;

  @JsonProperty("Message")
  private String message;

  @JsonProperty("Path")
  private String path;

  @JsonProperty("Url")
  private String url;

  /**
   * Low level textual error code, e.g., UK.OBIE.Field.Missing
   *
   * @return errorCode
   **/
  @NotNull
  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public OBError message(String message) {
    this.message = message;
    return this;
  }

  /**
   * A description of the error that occurred. e.g., 'A mandatory field isn't supplied' or 'RequestedExecutionDateTime must be in future' OBIE doesn't standardise this field
   *
   * @return message
   **/
  @NotNull
  @Size(min = 1, max = 500)
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public OBError path(String path) {
    this.path = path;
    return this;
  }

  /**
   * Recommended but optional reference to the JSON Path of the field with error, e.g., Data.Initiation.InstructedAmount.Currency
   *
   * @return path
   **/
  @Size(min = 1, max = 500)
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public OBError url(String url) {
    this.url = url;
    return this;
  }

  /**
   * URL to help remediate the problem, or provide more information, or to API Reference, or help etc
   *
   * @return url
   **/
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBError obError = (OBError) o;

    return new EqualsBuilder()
        .appendSuper(super.equals(o))
        .append(errorCode, obError.errorCode)
        .append(message, obError.message)
        .append(path, obError.path)
        .append(url, obError.url)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .appendSuper(super.hashCode())
        .append(errorCode)
        .append(message)
        .append(path)
        .append(url)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("errorCode", errorCode)
        .append("message", message)
        .append("path", path)
        .append("url", url)
        .toString();
  }
}

