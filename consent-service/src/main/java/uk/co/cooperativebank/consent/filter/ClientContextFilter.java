package uk.co.cooperativebank.consent.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StringUtils;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

/**
 * Inspects the header for a client context provided by MTS and
 * builds one if it doesn't exist.
 */
public class ClientContextFilter implements Filter {

  public static final String CLIENT_CONTEXT_HEADER = "x-coop-clientcontext";
  public static final String NO_CONTEXT_PROVIDED = "No Client Context Found";
  private static final String JSESSIONID = "jsessionid";
  public static final String MOBILE_CHANNEL = "MOBILE_CHANNEL";
  public static final String COOP_BRAND = "COOP";

  @Override
  public void init(FilterConfig filterConfig) {
    // no implementation required
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) servletRequest;
    MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(req);

    // add a consistent header from the known header types
    if (!StringUtils.hasText(mutableRequest.getHeader(CLIENT_CONTEXT_HEADER))) {
      mutableRequest.putHeader(CLIENT_CONTEXT_HEADER, buildContextHeader(mutableRequest));
    }

    filterChain.doFilter(mutableRequest, servletResponse);
  }

  @Override
  public void destroy() {
    // no implementation required
  }

  private String buildContextHeader(ServletRequest servletRequest) throws JsonProcessingException {

    HttpServletRequest request = (HttpServletRequest) servletRequest;

    // build a bunch of stuff
    ObjectMapper objectMapper = new ObjectMapper();

    ClientContext context = ClientContext.builder()
        .browser(request.getHeader(HttpHeaders.USER_AGENT))
        .ipAddress(request.getRemoteAddr())
        .authenticationToken(UUID.randomUUID().toString())
        .channel(MOBILE_CHANNEL)
        .brand(COOP_BRAND)
        .jSessionId(request.getHeader(JSESSIONID))
        .referrerSite(request.getHeader(HttpHeaders.REFERER))
        .userAgent(request.getHeader(HttpHeaders.USER_AGENT))
        .build();

    return new String(Base64.getEncoder().encode(objectMapper.writeValueAsString(context).getBytes()));
  }
}
