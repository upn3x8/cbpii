package uk.co.cooperativebank.consent.service;

import org.springframework.validation.annotation.Validated;
import uk.co.cooperativebank.account.dtos.Customer;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.account.mts.dao.account.DomesticAccount;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * Service for managing calls to the mainframe to retrieve customer {@link Customer} account details.
 *
 * @author williams.adeho
 */
@Validated
public interface CustomerAccountService {

  /**
   * Retrieves a customer details with the supplied customer number.
   *
   * @param identification the identification number of the customer.
   * @param clientContext  the client context for the mainframe.
   * @return the customer.
   */
  Customer findCustomerByAccount(@Pattern(regexp = Constants.REGEX_NOT_BLANK, message = CustomerAccountConstant.CID_MUST_NOT_BE_BLANK) String identification, @Valid @NotNull ClientContext clientContext);

  /**
   * Retrieves the first DomesticAccount that has valid product type
   * @param customerNumber
   * @param accountIdentification
   * @param clientContext
   * @return
   */
  DomesticAccount getDomesticAccount(String customerNumber, String accountIdentification, ClientContext clientContext);


    /**
     * Checks that a customer account available balance is more than the provided amount.
     *
     * @param customerNumber the identification number of the customer.
     * @param identification the account number of the customer.
     * @param clientContext  the client context for the mainframe.
     * @return true or false.
     */
  Optional<Boolean> customerAccountHasEnoughFunds(@Pattern(regexp = Constants.REGEX_NOT_BLANK, message = CustomerAccountConstant.CID_MUST_NOT_BE_BLANK) String customerNumber,
                                                  @Pattern(regexp = Constants.REGEX_NOT_BLANK, message = CustomerAccountConstant.IDENTIFICATION_MUST_NOT_BE_BLANK) String identification,
                                                  @NotNull BigDecimal amount,
                                                  @Valid @NotNull ClientContext clientContext);
}
