package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * OBFundsConfirmationData
 * TODO: Add regexes where applicable for the different types i.e. ensure a consentId looks like a number
 */
@Builder
@Validated
public class OBFundsConfirmationData {

  @JsonProperty("ConsentId")
  private String consentId = null;

  @JsonProperty("Reference")
  private String reference = null;

  @JsonProperty("InstructedAmount")
  private OBFundsConfirmationDataInstructedAmount instructedAmount = null;

  public OBFundsConfirmationData consentId(String consentId) {
    this.consentId = consentId;
    return this;
  }

  /**
   * Unique identification as assigned by the ASPSP to uniquely identify the funds confirmation consent resource.
   *
   * @return consentId
   **/
  @NotNull
  @Size(min = 1, max = 128)
  public String getConsentId() {
    return consentId;
  }

  public void setConsentId(String consentId) {
    this.consentId = consentId;
  }

  public OBFundsConfirmationData reference(String reference) {
    this.reference = reference;
    return this;
  }

  /**
   * Unique reference, as assigned by the CBPII, to unambiguously refer to the request related to the payment transaction.
   *
   * @return reference
   **/
  @NotNull
  @Size(min = 1, max = 35)
  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public OBFundsConfirmationData instructedAmount(OBFundsConfirmationDataInstructedAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
    return this;
  }

  /**
   * Get instructedAmount
   *
   * @return instructedAmount
   **/
  @NotNull
  @Valid
  public OBFundsConfirmationDataInstructedAmount getInstructedAmount() {
    return instructedAmount;
  }

  public void setInstructedAmount(OBFundsConfirmationDataInstructedAmount instructedAmount) {
    this.instructedAmount = instructedAmount;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationData that = (OBFundsConfirmationData) o;

    return new EqualsBuilder()
        .append(consentId, that.consentId)
        .append(reference, that.reference)
        .append(instructedAmount, that.instructedAmount)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(consentId)
        .append(reference)
        .append(instructedAmount)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("consentId", consentId)
        .append("reference", reference)
        .append("instructedAmount", instructedAmount)
        .toString();
  }
}

