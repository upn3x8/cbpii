package uk.co.cooperativebank.consent.repository;

import org.springframework.data.repository.CrudRepository;
import uk.co.cooperativebank.consent.domain.FundsConfirmation;

public interface FundsConfirmationRepository extends CrudRepository<FundsConfirmation, Long> {
}
