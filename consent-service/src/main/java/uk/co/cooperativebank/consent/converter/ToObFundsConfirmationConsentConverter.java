package uk.co.cooperativebank.consent.converter;

import org.springframework.core.convert.converter.Converter;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsent;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentCreateDataDebtorAccount;
import uk.co.cooperativebank.consent.domain.FundsConfirmationConsent;
import uk.co.cooperativebank.consent.service.ConfirmationConsentService;
import uk.co.cooperativebank.consent.service.Constants;

import java.time.ZoneOffset;

public class ToObFundsConfirmationConsentConverter implements Converter<FundsConfirmationConsent, OBFundsConfirmationConsent> {
  @Override
  public OBFundsConfirmationConsent convert(FundsConfirmationConsent source) {

    OBFundsConfirmationConsentCreateDataDebtorAccount debtorAccount = OBFundsConfirmationConsentCreateDataDebtorAccount.builder()
        .identification(source.getIdentification().substring(0, Constants.ACCOUNT_NUMBER_INDEX))
        .schemeName(ConfirmationConsentService.FIXED_SCHEME_NAME)
        .build();

    return OBFundsConfirmationConsent.builder()
        .consentId(Long.toString(source.getConsentId()))
        .creationDateTime(source.getCreated().atOffset(ZoneOffset.UTC))
        .debtorAccount(debtorAccount)
        .expirationDateTime(source.getExpiryDate().atOffset(ZoneOffset.UTC))
        .status(OBFundsConfirmationConsent.StatusEnum.valueOf(source.getStatus().name()))
        .statusUpdateDateTime(source.getStatusUpdated().atOffset(ZoneOffset.UTC))
        .tppId(source.getTppId())
        .build();
  }
}
