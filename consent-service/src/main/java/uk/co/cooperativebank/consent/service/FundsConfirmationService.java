package uk.co.cooperativebank.consent.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uk.co.cooperativebank.account.config.BrandConfiguration;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.consent.controller.exception.EmptyResponseApiException;
import uk.co.cooperativebank.consent.controller.exception.NotFoundException;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationData;
import uk.co.cooperativebank.consent.domain.FundsConfirmation;
import uk.co.cooperativebank.consent.domain.FundsConfirmationConsent;
import uk.co.cooperativebank.consent.repository.FundsConfirmationConsentRepository;
import uk.co.cooperativebank.consent.repository.FundsConfirmationRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Contains business logic for calls passed in from the Controllers.
 */
@Slf4j
@Service
@Transactional
public class FundsConfirmationService {

  private CustomerAccountService customerAccountService;
  private FundsConfirmationRepository consentRepository;
  private FundsConfirmationConsentRepository confirmationConsentRepository;
  private BrandConfiguration brandConfiguration;

  @Autowired
  public FundsConfirmationService(CustomerAccountService customerAccountService,
                                  FundsConfirmationRepository consentRepository,
                                  FundsConfirmationConsentRepository confirmationConsentRepository,
                                  BrandConfiguration brandConfiguration) {
    this.customerAccountService = customerAccountService;
    this.consentRepository = consentRepository;
    this.confirmationConsentRepository = confirmationConsentRepository;
    this.brandConfiguration = brandConfiguration;
  }

  /**
   * Create a FundsConfirmation, TPP called so the client context will not have the correct brand.
   *
   * @param createFundsConfirmation
   * @param tppId
   */
  public FundsConfirmation createFundsConfirmation(@NotNull(message = "Must provide a non-null confirmationConsent") OBFundsConfirmationData createFundsConfirmation,
                                                   @Pattern(regexp = Constants.REGEX_NOT_BLANK) String tppId,
                                                   @Valid ClientContext clientContext) {

    // find the consent this FundsConfirmation should apply to
    Optional<FundsConfirmationConsent> foundConsent = confirmationConsentRepository
        .findAuthorisedConfirmation(Long.parseLong(createFundsConfirmation.getConsentId()), tppId, LocalDateTime.now());

    if (!foundConsent.isPresent()) {
      throw new NotFoundException("Cannot find consent");
    }

    FundsConfirmationConsent fundsConfirmationConsent = foundConsent.get();

    // set the brand from the found consent
    clientContext.setBrand(brandConfiguration.getBrandByIdentification(fundsConfirmationConsent.getIdentification()));

    final BigDecimal amount = new BigDecimal(createFundsConfirmation.getInstructedAmount().getAmount());
    Optional<Boolean> hasFunds = customerAccountService.customerAccountHasEnoughFunds(fundsConfirmationConsent.getCId(), fundsConfirmationConsent.getIdentification(), amount, clientContext);

    // We can't complete the request as the account isn't active
    if (!hasFunds.isPresent()) {
      log.warn("Unable to provide FundsConfirmation for tppId: {}, consentId:{}", tppId, createFundsConfirmation.getConsentId());
      throw new EmptyResponseApiException();
    }

    FundsConfirmation fundsConfirmation = FundsConfirmation.builder()
        .confirmationConsent(fundsConfirmationConsent)
        .amount(new BigDecimal(createFundsConfirmation.getInstructedAmount().getAmount()))
        .currency(createFundsConfirmation.getInstructedAmount().getCurrency())
        .reference(createFundsConfirmation.getReference())
        .approved(hasFunds.get())
        .created(LocalDateTime.now()) // can't use annotation to do this.. :-(
        .build();

    consentRepository.save(fundsConfirmation);

    return fundsConfirmation;
  }
}
