package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * An array of detail error codes, and messages, and URLs to documentation to help remediation.
 */

@Validated
public class OBErrorResponse {
  @JsonProperty("Code")
  private String code = null;

  @JsonProperty("Id")
  private String id = null;

  @JsonProperty("Message")
  private String message;

  @JsonProperty("Errors")
  @Valid
  private List<OBError> errors = new ArrayList<>();

  public OBErrorResponse code(String code) {
    this.code = code;
    return this;
  }

  /**
   * High level textual error code, to help categorize the errors.
   *
   * @return code
   **/
  @NotNull
  @Size(min = 1, max = 40)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public OBErrorResponse id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A unique reference for the error instance, for audit purposes, in case of unknown/unclassified errors.
   *
   * @return id
   **/
  @Size(min = 1, max = 40)
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBErrorResponse message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Brief Error message, e.g., 'There is something wrong with the request parameters provided'
   *
   * @return message
   **/
  @NotNull
  @Size(min = 1, max = 500)
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public OBErrorResponse errors(List<OBError> errors) {
    this.errors = errors;
    return this;
  }

  public OBErrorResponse addErrorsItem(OBError errorsItem) {
    this.errors.add(errorsItem);
    return this;
  }

  /**
   * Get errors
   *
   * @return errors
   **/
  @NotNull
  @Valid
  @Size(min = 1)
  public List<OBError> getErrors() {
    return errors;
  }

  public void setErrors(List<OBError> errors) {
    this.errors = errors;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBErrorResponse that = (OBErrorResponse) o;

    return new EqualsBuilder()
        .appendSuper(super.equals(o))
        .append(code, that.code)
        .append(id, that.id)
        .append(message, that.message)
        .append(errors, that.errors)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .appendSuper(super.hashCode())
        .append(code)
        .append(id)
        .append(message)
        .append(errors)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("code", code)
        .append("id", id)
        .append("message", message)
        .append("errors", errors)
        .toString();
  }
}

