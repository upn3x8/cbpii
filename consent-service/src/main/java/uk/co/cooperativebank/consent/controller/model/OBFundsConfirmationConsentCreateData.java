package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * OBFundsConfirmationConsentCreateData
 */
@Builder
@Validated
public class OBFundsConfirmationConsentCreateData {

  @JsonProperty("ExpirationDateTime")
  private OffsetDateTime expirationDateTime;

  @JsonProperty("DebtorAccount")
  private OBFundsConfirmationConsentCreateDataDebtorAccount debtorAccount;

  /**
   * Specified date and time the funds confirmation authorisation will expire.  If this is not populated, the authorisation will be open ended.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   *
   * @return expirationDateTime
   **/
  @Valid
  @NotNull
  public OffsetDateTime getExpirationDateTime() {
    return expirationDateTime;
  }

  public void setExpirationDateTime(OffsetDateTime expirationDateTime) {
    this.expirationDateTime = expirationDateTime;
  }

  /**
   * Get debtorAccount
   *
   * @return debtorAccount
   **/
  @NotNull

  @Valid

  public OBFundsConfirmationConsentCreateDataDebtorAccount getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(OBFundsConfirmationConsentCreateDataDebtorAccount debtorAccount) {
    this.debtorAccount = debtorAccount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationConsentCreateData that = (OBFundsConfirmationConsentCreateData) o;

    return new EqualsBuilder()
        .append(expirationDateTime, that.expirationDateTime)
        .append(debtorAccount, that.debtorAccount)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(expirationDateTime)
        .append(debtorAccount)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("expirationDateTime", expirationDateTime)
        .append("debtorAccount", debtorAccount)
        .toString();
  }
}

