package uk.co.cooperativebank.consent.filter;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Inspects the header for a tppId header and moves it to a known key.
 */
@Slf4j
public class TppHeaderFilter implements Filter {

  // X-SSL-Client-Cert-Subject , X-SSL-Client-Cert-Issuer
  public static final String X_TPP_ID_HEADER = "x-tpp-id";   // the 'consistent' header used in the code
  public static final String X_SSL_CLIENT_CERT_SUBJECT_HEADER = "X-SSL-Client-Cert-Subject";   // one of the possible headers
  public static final String X_SSL_CLIENT_CERT_ISSUER_HEADER = "X-SSL-Client-Cert-Issuer";    // another one of the possible headers

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) servletRequest;
    MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(req);

    String sub = mutableRequest.getHeader(X_SSL_CLIENT_CERT_SUBJECT_HEADER);
    String issuer = mutableRequest.getHeader(X_SSL_CLIENT_CERT_ISSUER_HEADER);

    log.debug("Cert headers - sub:{}, issuer:{}", sub, issuer);

    String tppHeader = sub == null ? "" : sub;
    tppHeader = tppHeader + (issuer == null ? "" : issuer);

    if (StringUtils.isNotBlank(tppHeader)) {
      mutableRequest.putHeader(X_TPP_ID_HEADER, tppHeader);
    }

    filterChain.doFilter(mutableRequest, servletResponse);
  }

  @Override
  public void destroy() {
  }

}
