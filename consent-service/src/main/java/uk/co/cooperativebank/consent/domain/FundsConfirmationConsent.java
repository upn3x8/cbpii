package uk.co.cooperativebank.consent.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import uk.co.cooperativebank.consent.service.Constants;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "FUNDS_CONFIRMATION_CONSENT")
@Entity
public class FundsConfirmationConsent {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNDS_CONFIRMATION_CONSENT_SEQ")
  @SequenceGenerator(sequenceName = "FUNDS_CONFIRMATION_CONSENT_SEQ", allocationSize = 1, name = "FUNDS_CONFIRMATION_CONSENT_SEQ")
  @Column(name = "CONSENT_ID")
  private Long consentId;
  @Column(name = "TPP_ID")
  private String tppId;
  @Column(name = "C_ID")
  private String cId;
  @Column(name = "DC_ID")
  private String dcId;
  @Column(name = "IDENTIFICATION")
  @Size(min = Constants.ACCOUNT_NUMBER_INDEX, max = Constants.ACCOUNT_NUMBER_INDEX)
  private String identification;

  @Column(name = "EXPIRY_DATE")
  @Type(type = "uk.co.cooperativebank.consent.jpa.usertype.LocalDateTimeUserType")
  private LocalDateTime expiryDate;

  @Enumerated(EnumType.STRING)
  private Status status;

  @Type(type = "uk.co.cooperativebank.consent.jpa.usertype.LocalDateTimeUserType")
  @Column(name = "STATUS_UPDATED")
  private LocalDateTime statusUpdated;

  @Type(type = "uk.co.cooperativebank.consent.jpa.usertype.LocalDateTimeUserType")
  @Column(name = "CREATED")
  private LocalDateTime created;

  public FundsConfirmationConsent() {
  }

  public enum Status {AUTHORISED, AWAITINGAUTHORISATION, REJECTED, REVOKED}

}
