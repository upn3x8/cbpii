package uk.co.cooperativebank.consent.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmation;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationResponse;
import uk.co.cooperativebank.consent.converter.ToOBFundsConfirmationResponseData;
import uk.co.cooperativebank.consent.domain.FundsConfirmation;
import uk.co.cooperativebank.consent.filter.ClientContextFactory;
import uk.co.cooperativebank.consent.filter.ClientContextFilter;
import uk.co.cooperativebank.consent.service.FundsConfirmationService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static uk.co.cooperativebank.consent.filter.TppHeaderFilter.X_TPP_ID_HEADER;

@Slf4j
@Controller
public class FundsConfirmationsApiController {

  private static final String FUNDS_CONFIRMATIONS_URL = "/funds-confirmations";
  private final FundsConfirmationService fundsConfirmationService;
  private final ClientContextFactory contextFactory;

  @Autowired
  public FundsConfirmationsApiController(FundsConfirmationService fundsConfirmationService, ClientContextFactory contextFactory) {
    this.fundsConfirmationService = fundsConfirmationService;
    this.contextFactory = contextFactory;
  }

  @PostMapping(value = FUNDS_CONFIRMATIONS_URL,
      produces = {"application/json; charset=utf-8"},
      consumes = {"application/json"})
  public ResponseEntity<OBFundsConfirmationResponse> createFundsConfirmations(
      @Valid @RequestBody OBFundsConfirmation obFundsConfirmationParam,
      @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
      HttpServletRequest request) {

    // grab the client context
    final ClientContext context = contextFactory.getClientContext(request.getHeader(ClientContextFilter.CLIENT_CONTEXT_HEADER));

    // grab the x-tpp-id from the header. Annotation doesn't work in this instance..
    String xTppId = request.getHeader(X_TPP_ID_HEADER);
    HeaderValidation.validateXttpIdHeader(xTppId);

    log.info("POST {}, TppId: {}, Identifier: {}",
        FUNDS_CONFIRMATIONS_URL,
        xTppId,
        obFundsConfirmationParam.getData().getConsentId());


    FundsConfirmation fundsConfirmation = fundsConfirmationService.createFundsConfirmation(obFundsConfirmationParam.getData(), xTppId, context);

    ToOBFundsConfirmationResponseData converter = new ToOBFundsConfirmationResponseData();

    return new ResponseEntity<>(OBFundsConfirmationResponse.builder()
        .data(converter.convert(fundsConfirmation)).build(), HttpStatus.CREATED);

  }

}
