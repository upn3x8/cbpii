package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

/**
 * OBFundsConfirmationConsent
 */
@Builder
@Validated
public class OBFundsConfirmationConsent {
  @JsonProperty("ConsentId")
  private String consentId;

  @JsonProperty("TppId")
  private String tppId;

  @JsonProperty("CreationDateTime")
  private OffsetDateTime creationDateTime;

  @JsonProperty("Status")
  private StatusEnum status;

  @JsonProperty("StatusUpdateDateTime")
  private OffsetDateTime statusUpdateDateTime;

  @JsonProperty("ExpirationDateTime")
  private OffsetDateTime expirationDateTime;

  @JsonProperty("DebtorAccount")
  private OBFundsConfirmationConsentCreateDataDebtorAccount debtorAccount;

  public OBFundsConfirmationConsent consentId(String consentId) {
    this.consentId = consentId;
    return this;
  }

  /**
   * Unique identification as assigned to identify the funds confirmation consent resource.
   *
   * @return consentId
   **/
  @NotNull
  @Size(min = 1, max = 128)
  public String getConsentId() {
    return consentId;
  }

  public void setConsentId(String consentId) {
    this.consentId = consentId;
  }

  public OBFundsConfirmationConsent creationDateTime(OffsetDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
    return this;
  }

  /**
   * Date and time at which the resource was created.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   *
   * @return creationDateTime
   **/
  @NotNull
  @Valid
  public OffsetDateTime getCreationDateTime() {
    return creationDateTime;
  }

  public void setCreationDateTime(OffsetDateTime creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  public OBFundsConfirmationConsent status(StatusEnum status) {
    this.status = status;
    return this;
  }

  @NotNull
  @Size(min = 1, max = 128)
  public String getTppId() {
    return tppId;
  }

  public void setTppId(String tppId) {
    this.tppId = tppId;
  }

  public OBFundsConfirmationConsent tppId(String tppId) {
    this.tppId = tppId;
    return this;
  }

  /**
   * Specifies the status of consent resource in code form.
   *
   * @return status
   **/
  @NotNull
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public OBFundsConfirmationConsent statusUpdateDateTime(OffsetDateTime statusUpdateDateTime) {
    this.statusUpdateDateTime = statusUpdateDateTime;
    return this;
  }

  /**
   * Date and time at which the resource status was updated.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   *
   * @return statusUpdateDateTime
   **/
  @NotNull
  @Valid
  public OffsetDateTime getStatusUpdateDateTime() {
    return statusUpdateDateTime;
  }

  public void setStatusUpdateDateTime(OffsetDateTime statusUpdateDateTime) {
    this.statusUpdateDateTime = statusUpdateDateTime;
  }

  public OBFundsConfirmationConsent expirationDateTime(OffsetDateTime expirationDateTime) {
    this.expirationDateTime = expirationDateTime;
    return this;
  }

  /**
   * Specified date and time the funds confirmation authorisation will expire. If this is not populated, the authorisation will be open ended.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   *
   * @return expirationDateTime
   **/
  @Valid
  public OffsetDateTime getExpirationDateTime() {
    return expirationDateTime;
  }

  public void setExpirationDateTime(OffsetDateTime expirationDateTime) {
    this.expirationDateTime = expirationDateTime;
  }

  public OBFundsConfirmationConsent debtorAccount(OBFundsConfirmationConsentCreateDataDebtorAccount debtorAccount) {
    this.debtorAccount = debtorAccount;
    return this;
  }

  /**
   * Get debtorAccount
   *
   * @return debtorAccount
   **/
  @NotNull
  @Valid
  public OBFundsConfirmationConsentCreateDataDebtorAccount getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(OBFundsConfirmationConsentCreateDataDebtorAccount debtorAccount) {
    this.debtorAccount = debtorAccount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationConsent that = (OBFundsConfirmationConsent) o;

    return new EqualsBuilder()
        .append(consentId, that.consentId)
        .append(tppId, that.tppId)
        .append(creationDateTime, that.creationDateTime)
        .append(status, that.status)
        .append(statusUpdateDateTime, that.statusUpdateDateTime)
        .append(expirationDateTime, that.expirationDateTime)
        .append(debtorAccount, that.debtorAccount)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(consentId)
        .append(tppId)
        .append(creationDateTime)
        .append(status)
        .append(statusUpdateDateTime)
        .append(expirationDateTime)
        .append(debtorAccount)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("consentId", consentId)
        .append("tppId", tppId)
        .append("creationDateTime", creationDateTime)
        .append("status", status)
        .append("statusUpdateDateTime", statusUpdateDateTime)
        .append("expirationDateTime", expirationDateTime)
        .append("debtorAccount", debtorAccount)
        .toString();
  }

  /**
   * Specifies the status of consent resource in code form.
   */
  public enum StatusEnum {
    AUTHORISED("Authorised"),

    AWAITINGAUTHORISATION("AwaitingAuthorisation"),

    REJECTED("Rejected"),

    REVOKED("Revoked");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }
  }
}

