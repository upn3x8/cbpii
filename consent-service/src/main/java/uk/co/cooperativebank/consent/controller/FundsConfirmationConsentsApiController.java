package uk.co.cooperativebank.consent.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import uk.co.cooperativebank.account.mts.contexts.ClientContext;
import uk.co.cooperativebank.consent.controller.exception.BadRequestException;
import uk.co.cooperativebank.consent.controller.exception.NotFoundException;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsent;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentCreate;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentListResponse;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentResponse;
import uk.co.cooperativebank.consent.converter.ToObFundsConfirmationConsentConverter;
import uk.co.cooperativebank.consent.domain.FundsConfirmationConsent;
import uk.co.cooperativebank.consent.filter.ClientContextFactory;
import uk.co.cooperativebank.consent.filter.ClientContextFilter;
import uk.co.cooperativebank.consent.filter.TppHeaderFilter;
import uk.co.cooperativebank.consent.service.ConfirmationConsentService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static uk.co.cooperativebank.consent.filter.TppHeaderFilter.X_TPP_ID_HEADER;

@Slf4j
@Controller
public class FundsConfirmationConsentsApiController {

  private static final String FUNDS_CONFIRMATION_CONSENTS_URL = "/funds-confirmation-consents";
  private static final String FUNDS_CONFIRMATION_CONSENTS_CONSENT_ID_URL = "/funds-confirmation-consents/{ConsentId}";
  private final ClientContextFactory contextFactory;
  private ConfirmationConsentService confirmationConsentService;

  @Autowired
  public FundsConfirmationConsentsApiController(ConfirmationConsentService confirmationConsentService, ClientContextFactory contextFactory) {
    this.confirmationConsentService = confirmationConsentService;
    this.contextFactory = contextFactory;
  }

  @PostMapping(value = FUNDS_CONFIRMATION_CONSENTS_URL,
      produces = {"application/json; charset=utf-8"})
  public ResponseEntity<OBFundsConfirmationConsentResponse> createFundsConfirmationConsents(
      @Valid @RequestBody OBFundsConfirmationConsentCreate obFundsConfirmationConsentParam,
      HttpServletRequest request) {

    // grab the x-tpp-id from the header. Annotation doesn't work in this instance..
    String xTppId = request.getHeader(TppHeaderFilter.X_TPP_ID_HEADER);
    HeaderValidation.validateXttpIdHeader(xTppId);

    // grab the client context. For this endpoint the client context will have been injected using a
    // filter and thus the request header annotation doesn't work
    final ClientContext context = contextFactory.getClientContext(request.getHeader(ClientContextFilter.CLIENT_CONTEXT_HEADER));
    FundsConfirmationConsent consent = confirmationConsentService.createConfirmationConsent(obFundsConfirmationConsentParam.getData(), xTppId, context);
    log.info("POST {}, TppId: {}, Identifier: {}",
        FUNDS_CONFIRMATION_CONSENTS_URL,
        xTppId,
        StringUtils.abbreviate(obFundsConfirmationConsentParam.getData().getDebtorAccount().getIdentification(), 10));

    ToObFundsConfirmationConsentConverter converter = new ToObFundsConfirmationConsentConverter();

    OBFundsConfirmationConsentResponse consentResponse = OBFundsConfirmationConsentResponse.builder()
        .data(converter.convert(consent))
        .build();

    return new ResponseEntity<>(consentResponse, HttpStatus.CREATED);

  }

  @DeleteMapping(value = "/funds-confirmation-consents/{ConsentId}",
      produces = {"application/json; charset=utf-8"})
  public ResponseEntity<Void> deleteFundsConfirmationConsentsConsentId(
      @PathVariable("ConsentId") String consentId,
      @RequestHeader(value = "x-tpp-id", required = false) String xTppId) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @GetMapping(value = FUNDS_CONFIRMATION_CONSENTS_URL,
      produces = {"application/json; charset=utf-8"})
  public ResponseEntity<OBFundsConfirmationConsentListResponse> getFundsConfirmationConsents(
      @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
      @Valid @RequestParam(value = "dcid") String dcId) {

    log.info("GET: {}, dcId: {}", FUNDS_CONFIRMATION_CONSENTS_URL, dcId);

    ToObFundsConfirmationConsentConverter converter = new ToObFundsConfirmationConsentConverter();
    List<OBFundsConfirmationConsent> obFundsConfirmationConsents = confirmationConsentService.retrieveConfirmationConsent(dcId)
        .stream()
        .map(converter::convert)
        .collect(Collectors.toList());

    OBFundsConfirmationConsentListResponse listResponse = OBFundsConfirmationConsentListResponse.builder().data(obFundsConfirmationConsents).build();
    return new ResponseEntity<>(listResponse, HttpStatus.OK);
  }


  @GetMapping(value = FUNDS_CONFIRMATION_CONSENTS_CONSENT_ID_URL,
      produces = {"application/json; charset=utf-8"})
  public ResponseEntity<OBFundsConfirmationConsentResponse> getFundsConfirmationConsentsConsentId(
      @PathVariable("ConsentId") String consentId,
      HttpServletRequest request) throws NotFoundException {

    // grab the x-tpp-id from the header. Annotation doesn't work in this instance..
    String xTppId = request.getHeader(TppHeaderFilter.X_TPP_ID_HEADER);
    HeaderValidation.validateXttpIdHeader(xTppId);

    log.info("GET: {}, ConsentId:{}, tppId:{}", FUNDS_CONFIRMATION_CONSENTS_CONSENT_ID_URL, consentId, xTppId);

    FundsConfirmationConsent fundsConfirmationConsent = confirmationConsentService.retrieveConfirmationConsent(Long.parseLong(consentId), xTppId);

    ToObFundsConfirmationConsentConverter converter = new ToObFundsConfirmationConsentConverter();
    OBFundsConfirmationConsentResponse consentResponse = OBFundsConfirmationConsentResponse.builder()
        .data(converter.convert(fundsConfirmationConsent))
        .build();

    return new ResponseEntity<>(consentResponse, HttpStatus.OK);

  }

}