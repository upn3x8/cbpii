package uk.co.cooperativebank.consent.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import uk.co.cooperativebank.ApplicationConfiguration;
import uk.co.cooperativebank.consent.filter.ClientContextFilter;
import uk.co.cooperativebank.consent.filter.TestOverrideFilter;
import uk.co.cooperativebank.consent.filter.TppHeaderFilter;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class bootstraps the application i.e. tells the servlet container which
 * classes to use in order to configure the application.
 */
public class WebApplication extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] {ApplicationConfiguration.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[] {WebMvcConfiguration.class};
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] {"/"};
  }

  /**
   * Adds in the additional filters that are needed by the API.
   *
   * @return
   */
  @Override
  protected Filter[] getServletFilters() {

    Filter[] originalFilters = super.getServletFilters();
    List<Filter> originalFilterList = (originalFilters != null) ? Arrays.asList(originalFilters) : new ArrayList<>();
    originalFilterList.add(new ClientContextFilter());
    originalFilterList.add(new TppHeaderFilter());
    return originalFilterList.toArray(new Filter[] {});
  }
}
