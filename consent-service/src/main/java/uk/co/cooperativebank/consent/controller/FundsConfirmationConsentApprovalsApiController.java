package uk.co.cooperativebank.consent.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import uk.co.cooperativebank.consent.controller.exception.NotFoundException;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationConsentApprove;
import uk.co.cooperativebank.consent.service.ConfirmationConsentService;

import javax.validation.Valid;

@Slf4j
@Controller
public class FundsConfirmationConsentApprovalsApiController {

  public static final String FUNDS_CONFIRMATION_CONSENT_APPROVALS_URL = "/funds-confirmation-consent-approvals";
  private ConfirmationConsentService confirmationConsentService;

  @Autowired
  public FundsConfirmationConsentApprovalsApiController(ConfirmationConsentService confirmationConsentService) {
    this.confirmationConsentService = confirmationConsentService;
  }

  @PostMapping(value = FUNDS_CONFIRMATION_CONSENT_APPROVALS_URL,
      produces = {"application/json; charset=utf-8"},
      consumes = {"application/json"})
  public ResponseEntity<Void> approveFundsConfirmationConsents(
      @Valid @RequestBody OBFundsConfirmationConsentApprove obFundsConfirmationConsentApprove,
      @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress) throws NotFoundException {

    log.info("POST {}, consentId:{}, action: {}",
        FUNDS_CONFIRMATION_CONSENT_APPROVALS_URL,
        obFundsConfirmationConsentApprove.getData().getConsentId(),
        obFundsConfirmationConsentApprove.getData().getAction().name());

    confirmationConsentService.updateConfirmationConsent(obFundsConfirmationConsentApprove);

    return new ResponseEntity<>(HttpStatus.OK);
  }

}
