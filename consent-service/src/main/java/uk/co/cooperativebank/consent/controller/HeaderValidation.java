package uk.co.cooperativebank.consent.controller;

import org.apache.commons.lang3.StringUtils;
import uk.co.cooperativebank.consent.controller.exception.BadRequestException;

import static uk.co.cooperativebank.consent.filter.TppHeaderFilter.X_TPP_ID_HEADER;

public class HeaderValidation {

  public static void validateXttpIdHeader(String xTppIdHeader) {
    if (StringUtils.isBlank(xTppIdHeader)) {
      throw new BadRequestException("Mandatory header x-tpp-id couldn't be found!");
    }

    if (xTppIdHeader.length() > 128) {
      throw new BadRequestException(X_TPP_ID_HEADER + " should be between 1 and 128 chars");
    }
  }
}
