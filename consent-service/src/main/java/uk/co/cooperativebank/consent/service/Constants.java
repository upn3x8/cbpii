package uk.co.cooperativebank.consent.service;

public interface Constants {

  // validation constants
  String REGEX_NOT_BLANK = "\\S+$";
  int ACCOUNT_NUMBER_INDEX = 14;
  int SORT_CODE_INDEX = 6;
  int INDENTIFIER_LENGTH = 16;

}
