package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Unambiguous identification of the account of the debtor to which a confirmation of funds consent will be applied.
 */
@Builder
@Validated
public class OBFundsConfirmationConsentCreateDataDebtorAccount {

  public static final String UK_OBIE_SORT_CODE_ACCOUNT_NUMBER_PATTERN = "UK\\.OBIE\\.SortCodeAccountNumber";
  private static final String IDENTIFICATION_PATTERN = "^[0-9]{14}$";

  @JsonProperty("SchemeName")
  private String schemeName;

  @JsonProperty("Identification")
  private String identification;

  @JsonProperty("Name")
  private String name;

  @JsonProperty("SecondaryIdentification")
  private String secondaryIdentification;

  public OBFundsConfirmationConsentCreateDataDebtorAccount schemeName(String schemeName) {
    this.schemeName = schemeName;
    return this;
  }

  /**
   * Name of the identification scheme, in a coded form as published in an external list.
   *
   * @return schemeName
   **/
  @Pattern(regexp = UK_OBIE_SORT_CODE_ACCOUNT_NUMBER_PATTERN)
  @NotNull
  public String getSchemeName() {
    return schemeName;
  }

  public void setSchemeName(String schemeName) {
    this.schemeName = schemeName;
  }

  public OBFundsConfirmationConsentCreateDataDebtorAccount identification(String identification) {
    this.identification = identification;
    return this;
  }

  /**
   * Identification assigned by an institution to identify an account. This identification is known by the account owner.
   *
   * @return identification
   **/
  @Pattern(regexp = IDENTIFICATION_PATTERN)
  public String getIdentification() {
    return identification;
  }

  public void setIdentification(String identification) {
    this.identification = identification;
  }

  public OBFundsConfirmationConsentCreateDataDebtorAccount name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the account, as assigned by the account servicing institution. Usage: The account name is the name or names of the account owner(s) represented at an account level. The account name is not the product name or the nickname of the account.
   *
   * @return name
   **/

  @Size(max = 70)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OBFundsConfirmationConsentCreateDataDebtorAccount secondaryIdentification(String secondaryIdentification) {
    this.secondaryIdentification = secondaryIdentification;
    return this;
  }

  /**
   * This is secondary identification of the account, as assigned by the account servicing institution.  This can be used by building societies to additionally identify accounts with a roll number (in addition to a sort code and account number combination).
   *
   * @return secondaryIdentification
   **/

  @Size(max = 34)
  public String getSecondaryIdentification() {
    return secondaryIdentification;
  }

  public void setSecondaryIdentification(String secondaryIdentification) {
    this.secondaryIdentification = secondaryIdentification;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationConsentCreateDataDebtorAccount that = (OBFundsConfirmationConsentCreateDataDebtorAccount) o;

    return new EqualsBuilder()
        .append(schemeName, that.schemeName)
        .append(identification, that.identification)
        .append(name, that.name)
        .append(secondaryIdentification, that.secondaryIdentification)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(schemeName)
        .append(identification)
        .append(name)
        .append(secondaryIdentification)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("schemeName", schemeName)
        .append("identification", identification)
        .append("name", name)
        .append("secondaryIdentification", secondaryIdentification)
        .toString();
  }
}

