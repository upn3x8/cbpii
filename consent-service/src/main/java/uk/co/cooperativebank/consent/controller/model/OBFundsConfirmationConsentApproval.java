package uk.co.cooperativebank.consent.controller.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * OBFundsConfirmationConsentApproval
 */
@Builder
@Validated
public class OBFundsConfirmationConsentApproval {
  @JsonProperty("ConsentId")
  private String consentId = null;
  @JsonProperty("Action")
  private ActionEnum action = null;

  public OBFundsConfirmationConsentApproval consentId(String consentId) {
    this.consentId = consentId;
    return this;
  }

  /**
   * Unique identification as assigned to identify the funds confirmation consent resource.
   *
   * @return consentId
   **/
  @NotNull
  @Size(min = 1, max = 128)
  public String getConsentId() {
    return consentId;
  }

  public void setConsentId(String consentId) {
    this.consentId = consentId;
  }

  public OBFundsConfirmationConsentApproval action(ActionEnum action) {
    this.action = action;
    return this;
  }

  /**
   * Updates the consent to approve or revoke the consent.
   *
   * @return action
   **/
  @NotNull
  public ActionEnum getAction() {
    return action;
  }

  public void setAction(ActionEnum action) {
    this.action = action;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OBFundsConfirmationConsentApproval that = (OBFundsConfirmationConsentApproval) o;

    return new EqualsBuilder()
        .append(consentId, that.consentId)
        .append(action, that.action)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(consentId)
        .append(action)
        .toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("consentId", consentId)
        .append("action", action)
        .toString();
  }

  /**
   * Updates the consent to approve or revoke the consent.
   */
  public enum ActionEnum {
    APPROVE("APPROVE"),
    REJECT("REJECT"),
    REVOKE("REVOKE");

    private String value;

    ActionEnum(String value) {
      this.value = value;
    }

    @JsonCreator
    public static ActionEnum fromValue(String text) {
      for (ActionEnum b : ActionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }
  }
}

