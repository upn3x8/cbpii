package uk.co.cooperativebank.consent.converter;

import org.springframework.core.convert.converter.Converter;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationDataInstructedAmount;
import uk.co.cooperativebank.consent.controller.model.OBFundsConfirmationResponseData;
import uk.co.cooperativebank.consent.domain.FundsConfirmation;

import java.time.ZoneOffset;

public class ToOBFundsConfirmationResponseData implements Converter<FundsConfirmation, OBFundsConfirmationResponseData> {
  @Override
  public OBFundsConfirmationResponseData convert(FundsConfirmation source) {

    return OBFundsConfirmationResponseData.builder()
        .fundsConfirmationId(Long.toString(source.getConfirmationId()))
        .consentId(Long.toString(source.getConfirmationConsent().getConsentId()))
        .creationDateTime(source.getCreated().atOffset(ZoneOffset.UTC))
        .fundsAvailable(source.getApproved())
        .instructedAmount(OBFundsConfirmationDataInstructedAmount.builder()
            .amount(source.getAmount().toPlainString())
            .currency(source.getCurrency()).build())
        .reference(source.getReference())
        .build();

  }
}
