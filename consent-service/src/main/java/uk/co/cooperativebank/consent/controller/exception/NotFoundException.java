package uk.co.cooperativebank.consent.controller.exception;

public class NotFoundException extends ApiException {
  private static final int CODE = 404;

  public NotFoundException(String msg) {
    super(CODE, msg);
  }
}
