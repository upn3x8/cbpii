package uk.co.cooperativebank.consent.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import uk.co.cooperativebank.consent.domain.DomainPackage;

import javax.sql.DataSource;
import java.util.Collections;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:hibernate.properties")
public class JpaConfiguration {

  private final Environment env;

  private final DataSource dataSource;

  public JpaConfiguration(Environment env, @Qualifier("consentDataSource") DataSource dataSource) {
    this.env = env;
    this.dataSource = dataSource;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

    HibernateJpaVendorAdapter hibernateJpa = new HibernateJpaVendorAdapter();
    hibernateJpa.setDatabasePlatform(env.getProperty("hibernate.dialect"));
    hibernateJpa.setShowSql(env.getProperty("hibernate.show_sql", Boolean.class));

    LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
    emf.setDataSource(dataSource);
    emf.setPackagesToScan(DomainPackage.class.getPackage().getName());
    emf.setJpaVendorAdapter(hibernateJpa);
    emf.setJpaPropertyMap(Collections.singletonMap("javax.persistence.validation.mode", "none"));

    return emf;
  }

  @Bean
  public JpaTransactionManager transactionManager() {
    JpaTransactionManager txnMgr = new JpaTransactionManager();
    txnMgr.setEntityManagerFactory(entityManagerFactory().getObject());
    return txnMgr;
  }

}
