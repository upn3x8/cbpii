package uk.co.cooperativebank.consent.service;

public class CustomerAccountConstant {

  public static final String CID_MUST_NOT_BE_BLANK = "Customer Number must not be blank";
  public static final String IDENTIFICATION_MUST_NOT_BE_BLANK = "Identification must not be blank";

  private CustomerAccountConstant() {

  }
}
