package uk.co.cooperativebank.consent.controller;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.method.MethodConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import uk.co.cooperativebank.consent.controller.exception.ApiException;
import uk.co.cooperativebank.consent.controller.exception.EmptyResponseApiException;
import uk.co.cooperativebank.consent.controller.model.OBError;
import uk.co.cooperativebank.consent.controller.model.OBErrorResponse;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

  private static final String GENERAL_EXCEPTION = "Service unavailable due to an unhandled error. Please contact service desk.";
  private static final String GENERAL_ERROR_CODE = "500";

  /**
   * Global fallback handler to respond using a compliant error format.
   *
   * @param ex
   * @param request
   * @return
   */

  @ExceptionHandler(value = {Exception.class})
  public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {

    OBErrorResponse errorResponse = new OBErrorResponse()
        .code(GENERAL_ERROR_CODE)
        .id(GENERAL_ERROR_CODE)
        .message(GENERAL_EXCEPTION);

    log.error(ex.getLocalizedMessage());

    return new ResponseEntity<>(
        errorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Handles ApiExceptions
   *
   * @param ex
   * @param request
   * @return
   */
  @ExceptionHandler(value = {ApiException.class})
  public ResponseEntity<Object> handleAll(ApiException ex, WebRequest request) {

    OBErrorResponse errorResponse = new OBErrorResponse()
        .code(Integer.toString(ex.getCode()))
        .id(GENERAL_ERROR_CODE)
        .message(ex.getMessage());

    log.error(ex.getLocalizedMessage());

    return new ResponseEntity<>(
        errorResponse, new HttpHeaders(), HttpStatus.valueOf(ex.getCode()));
  }

  /**
   * Handles ApiExceptions
   *
   * @param ex
   * @param request
   * @return
   */
  @ExceptionHandler(value = {EmptyResponseApiException.class})
  public ResponseEntity<Object> handleAll(EmptyResponseApiException ex, WebRequest request) {

    return new ResponseEntity<>(
        null, new HttpHeaders(), HttpStatus.valueOf(ex.getCode()));
  }


  /**
   * Handles bad value types.
   *
   * @param ex
   * @param headers
   * @param status
   * @param request
   * @return
   */
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    OBErrorResponse errorResponse = buildOBErrorResponseFromStatus(status);

    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
      errorResponse.addErrorsItem(OBError.builder().errorCode("FIELD_ERROR").message(error.getField() + ": " + error.getDefaultMessage()).build());
    }
    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
      errorResponse.addErrorsItem(OBError.builder().errorCode("OBJ_ERROR").message(error.getObjectName() + ": " + error.getDefaultMessage()).build());
    }

    return handleExceptionInternal(
        ex, errorResponse, headers, status, request);
  }

  @ExceptionHandler( {MethodConstraintViolationException.class})
  public ResponseEntity<Object> handleConstraintViolation(MethodConstraintViolationException ex) {
    List<String> errors = new ArrayList<>();
    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
      errors.add(violation.getRootBeanClass().getName()
          + " "
          + violation.getPropertyPath() + ": " + violation.getMessage());
    }

    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
    return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
  }

  @ExceptionHandler( {MethodArgumentTypeMismatchException.class})
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException ex, WebRequest request) {

    OBErrorResponse errorResponse = new OBErrorResponse()
        .code(Integer.toString(HttpStatus.BAD_REQUEST.value()))
        .id(Integer.toString(HttpStatus.BAD_REQUEST.value()))
        .message(ex.getName() + " should be of type " + ex.getRequiredType().getName());

    return new ResponseEntity<>(
        errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
  }

  /**
   * Handles missing headers.
   *
   * @param ex
   * @param headers
   * @param status
   * @param request
   * @return
   */
  @Override
  protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    OBErrorResponse errorResponse = buildOBErrorResponseFromStatus(status);
    errorResponse.setMessage(ex.getLocalizedMessage());
    return new ResponseEntity<>(errorResponse, new HttpHeaders(), status);

  }

  /**
   * Handles incorrect http method.
   *
   * @param ex
   * @param headers
   * @param status
   * @param request
   * @return
   */
  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
      HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    return new ResponseEntity<>(buildOBErrorResponseFromStatus(status), new HttpHeaders(), status);
  }

  /**
   * Handles incorrect media type.
   *
   * @param ex
   * @param headers
   * @param status
   * @param request
   * @return
   */
  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
                                                                   HttpHeaders headers,
                                                                   HttpStatus status,
                                                                   WebRequest request) {

    return new ResponseEntity<>(buildOBErrorResponseFromStatus(status), new HttpHeaders(), status);

  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

    OBErrorResponse errorResponse = buildOBErrorResponseFromStatus(status);
    Optional.ofNullable(ex)
        .map(Throwable::getCause) // This is intentional
        .map(Throwable::getCause) // This really is intentional
        .map(Throwable::getLocalizedMessage)
        .ifPresent(errorResponse::setMessage);

    return new ResponseEntity<>(errorResponse, new HttpHeaders(), status);

  }

  /*
   * Convenience method to build a standard error response
   */
  private OBErrorResponse buildOBErrorResponseFromStatus(HttpStatus status) {
    return new OBErrorResponse()
        .code(Integer.toString(status.value()))
        .id(Integer.toString(status.value()))
        .message(status.getReasonPhrase());
  }
}
