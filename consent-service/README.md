# CBPII API


## Overview

### Stack
This service is implemented using Spring Framework v4, JPA v2.0 and Servlet Spec v3.0.1

To run locally without a jndi datasource, specifiy the 'local' profile by adding..

```-Dspring.profiles.active=local```

Running the app without a profile defaults to locating the datasource through jndi. 
The jndi lookup name is specified in ```resources/datasource.properties```

### Build & Package

`mvn package`

outputs..

`R010584C0010_CoopDigitalCBPII-Database.zip`

### API Specification

The API specification can be found in ```/api/consent-service.yaml```.

A baseline of the openbanking specification for comparison can be found at ```original-confirmation-of_funds_api_specification-v3.1.2-RC1-swagger.yaml```

Account type is configurable and a default of ``01`` has been specified in ```resources/applications.properties```
An account type is required to check balance funds.

The follow templates are required by the main frame translators, in order to send the right content message format to the main frame queues: 

```resources/accounts-host-templates.xml```

```resources/customer-host-templates.xml```

```resources/enterpriseintegration-host-templates.xml```

### Logging

Logging is output to stdout.
Logging configuration is contained in ```resources/logback.xml```

### Queue Config

Queue configuration is contained in ```resources/applications.properties```

#####Jndi Queue Names

JNDI Connection Source:   `queue/mts-queue-connection-source`

JNDI Account Enq. Queue:  `jms/queues/accountEnquiry`

JNDI Customer Enq. Queue: `jms/queues/customerEnquiry`

JNDI Reply Queue: `jms/queues/replyQueue`

### DB Config

#### Users

Requires: 
* `cbpiidig&&env` identified by `&&cpbii_pass`
* `cbpiidig&&env._app` identified by `&&cpbii_pass_app`

Base schema script is `/consent-service/database-schema/V0.0.1_Base_Schema.sql`

#####Jndi Connections

Datasource configuration is contained in: `resources/datasource.properties`

CBPII schema: `jdbc/digital-consent-db`

MTS Reference Data Schema: `jdbc/mts-reference-data-maps-db`

